from setuptools import setup

setup(
    name='cbds',
    version='0.1.3',
    packages=['cbds', 'cbds.utils', 'cbds.deeplearning', 'cbds.deeplearning.metrics', 'cbds.deeplearning.models',
              'cbds.deeplearning.settings'],
    url='',
    license='MIT/BSD',
    author='Center for Big Data Statistics/Statistics Netherlands',
    author_email='cbds_common@thinkpractice.nl',
    description='A collection of useful python functions and classes for data science from the '
                'Center of Big Data Statistics, Statistics Netherlands',
    install_requires=['keras>=2.1', 'matplotlib>=3.0', 'numpy', 'sklearn', 'graphviz', 'pydot']
)
