from sklearn.model_selection import train_test_split
import argparse
import psycopg2
import psycopg2.extras
import csv

def query_first_result(cursor, query):
    cursor.execute(query)
    return cursor.fetchone()

def query_results(cursor, query, row_filter):
    cursor.execute(query)
    return [row_filter(row) for row in cursor.fetchall()]

def create_project(cursor, project_name):
    query_result = query_first_result(cursor, "insert into project (project_name) values ('{}') returning project_id".format(project_name))
    return query_result.project_id

def project_id_for(cursor, project_name):
    query_result = query_first_result(cursor, "select project_id from project where project_name = '{}'".format(project_name))
    return query_result.project_id

def dataset_id_for(cursor, dataset_name):
    query_result = query_first_result(cursor, "select dataset_id from dataset where dataset_name = '{}'".format(dataset_name))
    return query_result.dataset_id

def create_dataset(cursor, dataset_name):
    query_result = query_first_result(cursor, "insert into dataset (dataset_name) values ('{}') returning dataset_id".format(dataset_name))
    return query_result.dataset_id

def add_dataset_to_project(cursor, project_name, dataset_name):
    dataset_id = dataset_id_for(cursor, dataset_name)
    project_id = project_id_for(cursor, project_name)
    cursor.execute("insert into project_dataset(project_id, dataset_id) values ({}, {})".format(project_id, dataset_id))

def create_dataset_fold(cursor, dataset_name, fold_name, training_percentage=0, test_percentage=0, validation_percentage=0):
    dataset_id = dataset_id_for(cursor, dataset_name)
    query_result = query_first_result(cursor, "insert into dataset_fold (fold_name, dataset_id, training_set_percentage, test_set_percentage, validation_set_percentage)\
            values ('{}', {}, {}, {}, {}) returning fold_id".format(fold_name, dataset_id, training_percentage, test_percentage, validation_percentage))
    return query_result.fold_id

# TODO stratified samples?
def create_fold_from_data(cursor, dataset_name, fold_name, test_percentage, validation_percentage, only_annotated_data=False):
    annotated_data_subquery = "inner join files_annotations as fa on fa.dataset_id = df.dataset_id and fa.file_id = f.file_id;" if only_annotated_data else ";"
    all_ids = query_results(cursor, """
        select df.file_id from dataset_files as df
        inner join dataset as d on d.dataset_id = df.dataset_id and dataset_name = '{}'
        inner join files as f on f.file_id = df.file_id
        {}
        """.format(dataset_name, annotated_data_subquery), lambda row: row.file_id)

    fold_id = create_dataset_fold(cursor, dataset_name, fold_name, 1.0-test_percentage, test_percentage, validation_percentage)
    training_val_set_ids, test_set_ids = train_test_split(all_ids, test_size=test_percentage)
    training_set_ids, validation_set_ids = train_test_split(training_val_set_ids, test_size=validation_percentage)
    add_files_ids_to_dataset_fold(cursor, fold_id, training_set_ids, "training_set")
    add_files_ids_to_dataset_fold(cursor, fold_id, test_set_ids, "test_set")
    add_files_ids_to_dataset_fold(cursor, fold_id, validation_set_ids, "validation_set")

def dataset_fold_id_for(cursor, fold_name):
    query_result = query_first_result(cursor, "select fold_id from dataset_fold where fold_name = '{}'".format(fold_name))
    return query_result.fold_id

def dataset_fold_type_id_for(cursor, fold_type):
    query_result = query_first_result(cursor, "select fold_type_id from dataset_fold_types where fold_type_name = '{}'".format(fold_type))
    return query_result.fold_type_id

def add_files_to_dataset_fold(cursor, fold_name, file_uuids, fold_type):
    fold_id = dataset_fold_id_for(cursor, fold_name)
    fold_type_id = dataset_fold_type_id_for(cursor, fold_type)
    cursor.execute("""insert into dataset_fold_files(fold_id, file_id, fold_type_id)
           select  df.fold_id, dfi.file_id, {fold_type_id} as fold_type_id from dataset_files as dfi
           inner join dataset_fold as df on df.dataset_id = dfi.dataset_id and df.fold_id = {fold_id}
           inner join files as f on f.file_id = dfi.file_id
           inner join (
           select * from tiles where uuid in ({uuids})
           ) ti on ti.tile_id = f.tile_id;
    """.format(fold_id=fold_id, fold_type_id=fold_type_id, uuids=",".join("'{}'".format(file_uuid) for file_uuid in file_uuids)))

def add_files_ids_to_dataset_fold(cursor, fold_id, file_ids, fold_type):
    fold_type_id = dataset_fold_type_id_for(cursor, fold_type)
    cursor.execute("""insert into dataset_fold_files(fold_id, file_id, fold_type_id)
           select  df.fold_id, dfi.file_id, {fold_type_id} as fold_type_id from dataset_files as dfi
           inner join dataset_fold as df on df.dataset_id = dfi.dataset_id and df.fold_id = {fold_id}
           where dfi.file_id in ({ids})
    """.format(fold_id=fold_id, fold_type_id=fold_type_id, ids=",".join("'{}'".format(file_id) for file_id in file_ids)))

def create_model(cursor, model_name, model_filename):
    query_result = query_first_result(cursor, "insert into models(model_name, model_filename) values ('{}', '{}') returning model_id".format(model_name, model_filename))
    return query_result.model_id

def model_id_for(cursor, model_name):
    query_result = query_first_result(cursor, "select model_id from models where model_name = '{}'".format(model_name))
    return query_result.model_id

def new_import_id_for(cursor):
    cursor.callproc("get_new_import_id")
    return cursor.fetchone().get_new_import_id

def copy_model_predictions(cursor, import_id):
    cursor.callproc("copy_model_predictions", (import_id,))

def add_predictions_to_model(cursor, model_name, fold_name, predictions):
    model_id = model_id_for(cursor, model_name)
    fold_id = dataset_fold_id_for(cursor, fold_name)
    import_id = new_import_id_for(cursor)

    cursor.execute("insert into model_predictions_import (import_id, uuid_string, prediction, model_id, fold_id) values {}"
            .format(",".join(["({},'{}', {}, {}, {})".format(import_id, prediction["uuid"], prediction["prediction"], model_id, fold_id)
                for prediction in predictions])))

    copy_model_predictions(cursor, import_id)

def new_import_id_for_datafiles(cursor):
    cursor.callproc("get_new_data_files_import_id")
    return cursor.fetchone().get_new_data_files_import_id

def copy_dataset_files(cursor, import_id):
    cursor.callproc("copy_dataset_files", (import_id,))

def add_datafiles_to_dataset(cursor, dataset_name, layer_name, file_uuids):
    import_id = new_import_id_for_datafiles(cursor)
    cursor.execute("""insert into dataset_files_import (import_id, dataset_name, layername, uuid_string) values {}
            """.format(",".join(["({}, '{}', '{}', '{}')".format(import_id, dataset_name, layer_name, file_uuid)
                for file_uuid in file_uuids])))
    copy_dataset_files(cursor, import_id)

def read_csv_data(filename, delimiter, row_filter):
    with open(filename) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=delimiter)
        return [row_filter(row) for row in csv_reader]

def add_predictions_from_csv(cursor, filename, delimiter, model_name, fold_name):
    predictions = read_csv_data(filename, delimiter, lambda row: row)
    add_predictions_to_model(cursor, model_name, fold_name, predictions)

def add_datafiles_from_csv(cursor, filename, delimiter, dataset_name, layer_name):
    file_uuids = read_csv_data(filename, delimiter, lambda row: row["uuid"])
    add_datafiles_to_dataset(cursor, dataset_name, layer_name, file_uuids)

def add_data_to_fold_from_csv(cursor, filename, delimiter, fold_name):
    fold_data = read_csv_data(filename, delimiter, lambda row: row["uuid"])
    add_files_to_dataset_fold(cursor, fold_name, fold_data, "training_set")

def save_fold_to_csv(cursor, filename, delimiter, dataset_name, fold_name, add_labels):
    add_labels_subquery = "left join files_annotations as fa on fa.dataset_id = df.dataset_id and fa.file_id = f.file_id" if add_labels else ""
    label_query = ", fa.label" if add_labels else ""
    fold_rows = query_results(cursor, """select d.dataset_name, df.fold_name, f.file_id, ti.uuid, f.filename, dfi.fold_type_id {label_query} 
            from dataset as d
            inner join dataset_fold as df on df.dataset_id = d.dataset_id
            inner join dataset_fold_files as dfi on dfi.fold_id = df.fold_id
            inner join files as f on f.file_id = dfi.file_id
            inner join tiles as ti on ti.tile_id = f.tile_id
            {subquery}
            where dataset_name = '{dataset_name}' and df.fold_name = '{fold_name}'
    """.format(label_query=label_query, dataset_name=dataset_name, fold_name=fold_name, subquery=add_labels_subquery), lambda row: row._asdict())
    with open(filename, "w") as csv_file:
        csv_writer = csv.DictWriter(csv_file, delimiter=delimiter, fieldnames=fold_rows[0].keys())
        csv_writer.writeheader()
        for row in fold_rows:
            csv_writer.writerow(row)

def create_hook(cursor, args):
    if args.create == "project":
        create_project(cursor, args.name)
    elif args.create == "dataset":
        create_dataset(cursor, args.name)

def create_model_hook(cursor, args):
    create_model(cursor, args.model_name, args.model_filename)

def add_hook(cursor, args):
    if args.add == "dataset":
        add_dataset_to_project(cursor, args.parent_name, args.child_name)
    elif args.add == "fold":
        create_dataset_fold(cursor, args.parent_name, args.child_name)

def add_data_files_hook(cursor, args):
    add_datafiles_from_csv(cursor, args.filename, args.delimiter, args.dataset_name, args.layer_name)

def add_predictions_hook(cursor, args):
    add_predictions_from_csv(cursor, args.filename, args.delimiter, args.model_name, args.fold_name)

def add_data_to_fold_hook(cursor, args):
    add_data_to_fold_from_csv(cursor, args.filename, args.delimiter, args.fold_name)

def create_fold_hook(cursor, args):
    create_fold_from_data(cursor, args.dataset_name, args.fold_name, args.test_percentage, args.validation_percentage, args.only_annotated_data)

def save_fold_hook(cursor, args):
    save_fold_to_csv(cursor, args.filename, args.delimiter, args.dataset_name, args.fold_name, args.add_labels)

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--server-address", default="localhost", help="The ip address of the postgres database")
parser.add_argument("-n", "--db-name", default="postgres", help="The database name in the postgres database")
parser.add_argument("-u", "--db-user", default="postgres", help="The username for the postgres database")
parser.add_argument("-p", "--password", required=True, help="The password for the postgres database")
subparsers = parser.add_subparsers(help='Sub-command help')
create_command = subparsers.add_parser("create", help="Create a new project, dataset")
create_command.set_defaults(func=create_hook)
create_command.add_argument('create', nargs='?', default='project', choices=['project', 'dataset', 'fold'])
create_command.add_argument('name')
create_model_command = subparsers.add_parser('create_model', help="Create a model")
create_model_command.set_defaults(func=create_model_hook)
create_model_command.add_argument("model_name")
create_model_command.add_argument("model_filename")
add_model_command = subparsers.add_parser('add', help="Add dataset to project or fold to dataset")
add_model_command.set_defaults(func=add_hook)
add_model_command.add_argument('add', nargs='?', default='dataset', choices=['dataset', 'fold'])
add_model_command.add_argument("parent_name")
add_model_command.add_argument("child_name")
add_data_files_command = subparsers.add_parser('add_data_files', help="Add data files to dataset")
add_data_files_command.set_defaults(func=add_data_files_hook)
add_data_files_command.add_argument("filename")
add_data_files_command.add_argument("delimiter", default=";")
add_data_files_command.add_argument("dataset_name")
add_data_files_command.add_argument("layer_name")
add_data_files_command = subparsers.add_parser('add_data_to_fold', help="Add data already in database to a fold")
add_data_files_command.set_defaults(func=add_data_to_fold_hook)
add_data_files_command.add_argument("filename")
add_data_files_command.add_argument("delimiter", default=";")
add_data_files_command.add_argument("fold_name")
create_fold_command = subparsers.add_parser('create_fold', help="Create a fold for a dataset")
create_fold_command.set_defaults(func=create_fold_hook)
create_fold_command.add_argument("dataset_name")
create_fold_command.add_argument("fold_name")
create_fold_command.add_argument("test_percentage", type=float)
create_fold_command.add_argument("validation_percentage", type=float, default=0.1)
create_fold_command.add_argument("only_annotated_data", type=bool, default=False)
save_fold_command = subparsers.add_parser('save_fold', help="Save a fold to disk")
save_fold_command.set_defaults(func=save_fold_hook)
save_fold_command.add_argument("filename")
save_fold_command.add_argument("delimiter", default=";")
save_fold_command.add_argument("dataset_name")
save_fold_command.add_argument("fold_name")
save_fold_command.add_argument("add_labels")
add_predictions_model_command = subparsers.add_parser('add_predictions', help="Add predictions to model")
add_predictions_model_command.set_defaults(func=add_predictions_hook)
add_predictions_model_command.add_argument("filename")
add_predictions_model_command.add_argument("delimiter", default=";")
add_predictions_model_command.add_argument("model_name")
add_predictions_model_command.add_argument("fold_name")
args = parser.parse_args()

connection_parameters = {
   'host': args.server_address,
   'database': args.db_name,
   'user': args.db_user,
   'password': args.password
}

with psycopg2.connect(**connection_parameters) as db_connection:
    with db_connection.cursor(cursor_factory = psycopg2.extras.NamedTupleCursor) as cursor:
        if args.func:
            args.func(cursor, args)

