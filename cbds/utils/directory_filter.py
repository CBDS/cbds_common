from .file_filter import FileFilter
import os


class DirectoryFilter(object):
    def __init__(self, root_path):
        self.__root_path = root_path

    @property
    def root_path(self):
        return self.__root_path

    @property
    def all(self):
        return FileFilter(self.root_path, os.listdir(self.root_path), "")

    def __getattr__(self, filter_name):
        return FileFilter(self.root_path, os.listdir(self.root_path), filter_name)

    def dir(self, filter_name):
        sub_path = os.path.join(self.root_path, filter_name)
        if not os.path.exists(sub_path):
            return self
        return DirectoryFilter(sub_path)
