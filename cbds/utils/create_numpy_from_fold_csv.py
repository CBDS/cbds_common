from collections import defaultdict
import progressbar
import numpy as np
import argparse
import csv
import cv2
import os

def fold_name_for(fold_type): 
    if fold_type == "1":
        return "training"
    elif fold_type == "2":
        return "testing"
    return "validation"

def write_fold_type(filename, fold_type, files_and_labels): 
    root_filename = "{}_{}".format(filename, fold_name_for(fold_type))
    data_filename = "{}.npy".format(root_filename)
    labels_filename = "{}_labels.npy".format(root_filename)

    images = []
    labels = []
    widgets=[
        ' [', progressbar.Timer(), '] ',
        progressbar.Bar(),
        ' (', progressbar.ETA(), ') ',
    ]
    pbar = progressbar.ProgressBar(len(files_and_labels), widgets)
    pbar.start()
    for i,(filename, label) in enumerate(files_and_labels):
        image = cv2.imread(filename)
        images.append(image[:,:,::-1])
        labels.append(label)
        pbar.update(i)
    pbar.finish()
    
    print("Writing to {}".format(data_filename))
    print(len(images))
    np.save(data_filename, images)
    print("Writing to {}".format(labels_filename))
    np.save(labels_filename, np.array(labels))

def write_fold_types(filename, files_per_fold_type):
    for fold_type, files_and_labels in files_per_fold_type.items():
        print("Writing: {}".format(fold_name_for(fold_type)))
        write_fold_type(filename, fold_type, files_and_labels)

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-file", required=True, help="The csv with the fold information")
parser.add_argument("-c", "--delimiter", default=";", help="The delimiter for the csv file")
parser.add_argument("-o", "--output-dir", required=True, help="The output directory to write the numpy files to")
parser.add_argument("-d", "--dataset-dir", required=True, help="The directory containing the images files (top level)")
args = parser.parse_args()

with open(args.input_file) as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=args.delimiter)
    files_per_fold_type = defaultdict(list)
    for row in csv_reader:        
        fold_filename = row["fold_name"]
        fold_type = row["fold_type_id"]
        filename = os.path.join(args.dataset_dir, row["filename"].replace("${DATASET_DIR}/", ""))
        label = row["label"]
        files_per_fold_type[fold_type].append((filename, label))
    write_fold_types(os.path.join(args.output_dir, fold_filename), files_per_fold_type)