import os


class Utils:
    @staticmethod
    def makedirs_if_not_exist(path):
        if not os.path.exists(path):
            os.makedirs(path)