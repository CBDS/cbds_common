from keras.preprocessing.image import ImageDataGenerator
from .dataset import Dataset


class ImageGenerator:
    def __init__(self, dataset, shuffle_data=True, seed=None, featurewise_center=False, samplewise_center=False,
                 featurewise_std_normalization=False, samplewise_std_normalization=False, zca_whitening=False,
                 zca_epsilon=1e-06, rotation_range=0, width_shift_range=0.0, height_shift_range=0.0,
                 brightness_range=None, shear_range=0.0, zoom_range=0.0, channel_shift_range=0.0, fill_mode='nearest',
                 cval=0.0, horizontal_flip=False, vertical_flip=False, rescale=None, preprocessing_function=None,
                 data_format=None, validation_split=0.0, dtype=None):
        self.__dataset = dataset
        self.__shuffle_data = shuffle_data
        self.__seed = seed
        self.__feature_wise_center = featurewise_center
        self.__sample_wise_center = samplewise_center
        self.__feature_wise_std_normalization = featurewise_std_normalization
        self.__sample_wise_std_normalization = samplewise_std_normalization
        self.__zca_whitening = zca_whitening
        self.__zca_epsilon = zca_epsilon
        self.__rotation_range = rotation_range
        self.__width_shift_range = width_shift_range
        self.__height_shift_range = height_shift_range
        self.__brightness_range = brightness_range
        self.__shear_range = shear_range
        self.__zoom_range = zoom_range
        self.__channel_shift_range = channel_shift_range
        self.__fill_mode = fill_mode
        self.__cval = cval
        self.__horizontal_flip = horizontal_flip
        self.__vertical_flip = vertical_flip
        self.__rescale = rescale
        self.__preprocessing_function = preprocessing_function
        self.__data_format = data_format
        self.__validation_split = validation_split
        self.__dtype = dtype

    @property
    def dataset(self):
        return self.__dataset

    @property
    def name(self):
        return self.dataset.name

    @property
    def data(self):
        return self.dataset.data

    @property
    def labels(self):
        return self.dataset.labels

    @property
    def inner_generator(self):
        return ImageDataGenerator(featurewise_center=self.feature_wise_center,
                                  samplewise_center=self.sample_wise_center,
                                  featurewise_std_normalization=self.feature_wise_std_normalization,
                                  samplewise_std_normalization=self.sample_wise_std_normalization,
                                  zca_whitening=self.zca_whitening,
                                  zca_epsilon=self.zca_epsilon,
                                  rotation_range=self.rotation_range,
                                  width_shift_range=self.width_shift_range,
                                  height_shift_range=self.height_shift_range,
                                  brightness_range=self.brightness_range,
                                  shear_range=self.shear_range,
                                  zoom_range=self.zoom_range,
                                  channel_shift_range=self.channel_shift_range,
                                  fill_mode=self.fill_mode,
                                  cval=self.cval,
                                  horizontal_flip=self.horizontal_flip,
                                  vertical_flip=self.vertical_flip,
                                  rescale=self.rescale,
                                  preprocessing_function=self.preprocessing_function,
                                  data_format=self.data_format,
                                  validation_split=self.validation_split,
                                  dtype=self.dtype)

    @property
    def shuffle_data(self):
        return self.__shuffle_data

    @shuffle_data.setter
    def shuffle_data(self, value):
        self.__shuffle_data = value

    @property
    def seed(self):
        return self.__seed

    @seed.setter
    def seed(self, value):
        self.__seed = value

    @property
    def feature_wise_center(self):
        return self.__feature_wise_center

    @feature_wise_center.setter
    def feature_wise_center(self, value):
        self.__feature_wise_center = value

    @property
    def sample_wise_center(self):
        return self.__sample_wise_center

    @sample_wise_center.setter
    def sample_wise_center(self, value):
        self.__sample_wise_center = value

    @property
    def feature_wise_std_normalization(self):
        return self.__feature_wise_std_normalization

    @feature_wise_std_normalization.setter
    def feature_wise_std_normalization(self, value):
        self.__feature_wise_std_normalization = value

    @property
    def sample_wise_std_normalization(self):
        return self.__sample_wise_std_normalization

    @sample_wise_std_normalization.setter
    def sample_wise_std_normalization(self, value):
        self.__sample_wise_std_normalization = value

    @property
    def zca_whitening(self):
        return self.__zca_whitening

    @zca_whitening.setter
    def zca_whitening(self, value):
        self.__zca_whitening = value

    @property
    def zca_epsilon(self):
        return self.__zca_epsilon

    @zca_epsilon.setter
    def zca_epsilon(self, value):
        self.__zca_epsilon = value

    @property
    def rotation_range(self):
        return self.__rotation_range

    @rotation_range.setter
    def rotation_range(self, value):
        self.__rotation_range = value

    @property
    def width_shift_range(self):
        return self.__width_shift_range

    @width_shift_range.setter
    def width_shift_range(self, value):
        self.__width_shift_range = value

    @property
    def height_shift_range(self):
        return self.__height_shift_range

    @height_shift_range.setter
    def height_shift_range(self, value):
        self.__height_shift_range = value

    @property
    def brightness_range(self):
        return self.__brightness_range

    @brightness_range.setter
    def brightness_range(self, value):
        self.__brightness_range = value

    @property
    def shear_range(self):
        return self.__shear_range

    @shear_range.setter
    def shear_range(self, value):
        self.__shear_range = value

    @property
    def zoom_range(self):
        return self.__zoom_range

    @zoom_range.setter
    def zoom_range(self, value):
        self.__zoom_range = value

    @property
    def channel_shift_range(self):
        return self.__channel_shift_range

    @channel_shift_range.setter
    def channel_shift_range(self, value):
        self.__channel_shift_range = value

    @property
    def fill_mode(self):
        return self.__fill_mode

    @fill_mode.setter
    def fill_mode(self, value):
        self.__fill_mode = value

    @property
    def cval(self):
        return self.__cval

    @cval.setter
    def cval(self, value):
        self.__cval = value

    @property
    def horizontal_flip(self):
        return self.__horizontal_flip

    @horizontal_flip.setter
    def horizontal_flip(self, value):
        self.__horizontal_flip = value

    @property
    def vertical_flip(self):
        return self.__vertical_flip

    @vertical_flip.setter
    def vertical_flip(self, value):
        self.__vertical_flip = value

    @property
    def rescale(self):
        return self.__rescale

    @rescale.setter
    def rescale(self, value):
        self.__rescale = value

    @property
    def preprocessing_function(self):
        return self.__preprocessing_function

    @preprocessing_function.setter
    def preprocessing_function(self, value):
        self.__preprocessing_function = value

    @property
    def data_format(self):
        return self.__data_format

    @data_format.setter
    def data_format(self, value):
        self.__data_format = value

    @property
    def dtype(self):
        return self.__dtype

    @dtype.setter
    def dtype(self, value):
        self.__dtype = value

    @property
    def validation_split(self):
        return self.__validation_split

    @validation_split.setter
    def validation_split(self, value):
        self.__validation_split = value

    def __len__(self):
        return len(self.labels)

    def with_shuffle_data(self, shuffle_data):
        self.shuffle_data = shuffle_data
        return self

    def with_seed(self, seed):
        self.seed = seed
        return self

    def with_feature_wise_center(self, feature_wise_center):
        self.feature_wise_center = feature_wise_center
        return self

    def with_sample_wise_center(self, samplewise_center):
        self.sample_wise_center = samplewise_center
        return self

    def with_feature_wise_std_normalization(self, featurewise_std_normalization):
        self.feature_wise_std_normalization = featurewise_std_normalization
        return self

    def with_sample_wise_std_normalization(self, samplewise_std_normalization):
        self.sample_wise_std_normalization = samplewise_std_normalization
        return self

    def with_zca_whitening(self, zca_whitening):
        self.zca_whitening = zca_whitening
        return self

    def with_zca_epsilon(self, zca_episilon):
        self.zca_epsilon = zca_episilon
        return self

    def with_rotation_range(self, rotation_range):
        self.rotation_range = rotation_range
        return self

    def with_width_shift_range(self, width_shift_range):
        self.width_shift_range = width_shift_range
        return self

    def with_height_shift_range(self, height_shift_range):
        self.height_shift_range = height_shift_range
        return self

    def with_brightness_range(self, brightness_range):
        self.brightness_range = brightness_range
        return self

    def with_shear_range(self, shear_range):
        self.shear_range = shear_range
        return self

    def with_zoom_range(self, zoom_range):
        self.zoom_range = zoom_range
        return self

    def with_channel_shift_range(self, channel_shift_range):
        self.channel_shift_range = channel_shift_range
        return self

    def with_fill_mode(self, fill_mode):
        self.fill_mode = fill_mode
        return self

    def with_cval(self, cval):
        self.cval = cval
        return self

    def with_horizontal_flip(self, horizontal_flip):
        self.horizontal_flip = horizontal_flip
        return self

    def with_vertical_flip(self, vertical_flip):
        self.vertical_flip = vertical_flip
        return self

    def with_rescale(self, rescale):
        self.rescale = rescale
        return self

    def with_preprocessing_function(self, preprocessing_function):
        self.preprocessing_function = preprocessing_function
        return self

    def with_data_format(self, data_format):
        self.data_format = data_format
        return self

    def with_validation_split(self, validation_split):
        self.validation_split = validation_split
        return self

    def with_dtype(self, dtype):
        self.dtype = dtype
        return self

    def flow(self, batch_size=32):
        return self.inner_generator.flow(self.dataset.data, self.dataset.labels, batch_size=batch_size, seed=self.seed,
                                         shuffle=self.shuffle_data)

    # TODO is there a a way to save the preprocessing method to json?
    def to_dict(self):
        return {
            "dataset": self.dataset.to_dict(),
            "shuffle_data": self.shuffle_data,
            "seed": self.seed,
            "feature_wise_center": self.feature_wise_center,
            "sample_wise_center": self.sample_wise_center,
            "feature_wise_std_normalization": self.feature_wise_std_normalization,
            "sample_wise_std_normalization": self.sample_wise_std_normalization,
            "zca_whitening": self.zca_whitening,
            "zca_epsilon": self.zca_epsilon,
            "rotation_range": self.rotation_range,
            "width_shift_range": self.width_shift_range,
            "height_shift_range": self.height_shift_range,
            "brightness_range": self.brightness_range,
            "shear_range": self.shear_range,
            "zoom_range": self.zoom_range,
            "channel_shift_range": self.channel_shift_range,
            "fill_mode": self.fill_mode,
            "cval": self.cval,
            "horizontal_flip": self.horizontal_flip,
            "vertical_flip": self.vertical_flip,
            "rescale": self.rescale,
            "data_format": self.data_format,
            "validation_split": self.validation_split,
            "dtype": self.dtype
        }

    @staticmethod
    def from_dict(json_dict):
        dataset = Dataset.instance_from_dict(json_dict["dataset"])
        image_generator = ImageGenerator(dataset=dataset)
        image_generator.shuffle_data = json_dict.get("shuffle_data", image_generator.shuffle_data)
        image_generator.seed = json_dict.get("seed", image_generator.seed)
        image_generator.feature_wise_center = json_dict.get("feature_wise_center", image_generator.feature_wise_center)
        image_generator.sample_wise_center = json_dict.get("sample_wise_center", image_generator.sample_wise_center)
        image_generator.feature_wise_std_normalization = json_dict.get("feature_wise_std_normalization",
                                                                       image_generator.feature_wise_std_normalization)
        image_generator.sample_wise_std_normalization = json_dict.get("sample_wise_std_normalization",
                                                                      image_generator.sample_wise_std_normalization)
        image_generator.zca_whitening = json_dict.get("zca_whitening", image_generator.zca_whitening)
        image_generator.zca_epsilon = json_dict.get("zca_epsilon", image_generator.zca_epsilon)
        image_generator.rotation_range = json_dict.get("rotation_range", image_generator.rotation_range)
        image_generator.width_shift_range = json_dict.get("width_shift_range", image_generator.width_shift_range)
        image_generator.height_shift_range = json_dict.get("height_shift_range", image_generator.height_shift_range)
        image_generator.brightness_range = json_dict.get("brightness_range", image_generator.brightness_range)
        image_generator.shear_range = json_dict.get("shear_range", image_generator.shear_range)
        image_generator.zoom_range = json_dict.get("zoom_range", image_generator.zoom_range)
        image_generator.channel_shift_range = json_dict.get("channel_shift_range", image_generator.channel_shift_range)
        image_generator.fill_mode = json_dict.get("fill_mode", image_generator.fill_mode)
        image_generator.cval = json_dict.get("cval", image_generator.cval)
        image_generator.horizontal_flip = json_dict.get("horizontal_flip", image_generator.horizontal_flip)
        image_generator.vertical_flip = json_dict.get("vertical_flip", image_generator.vertical_flip)
        image_generator.rescale = json_dict.get("rescale", image_generator.rescale)
        image_generator.data_format = json_dict.get("data_format", image_generator.data_format)
        image_generator.validation_split = json_dict.get("validation_split", image_generator.validation_split)
        image_generator.dtype = json_dict.get("dtype", image_generator.dtype)

        return image_generator
