from keras.models import load_model
from keras.callbacks import ModelCheckpoint, CSVLogger
from .metrics import PlotHistoryCallback
from .settings import OptimizerSettings
from .settings import SGDSettings
from .settings import SettingsProvider
from .dataset import Dataset
from keras.backend.tensorflow_backend import set_session
from keras.backend.tensorflow_backend import clear_session
from keras.backend.tensorflow_backend import get_session
import tensorflow
import os
import gc


class Run(SettingsProvider):
    def __init__(self, model_filename, datetime, run_dir, epochs=0, batch_size=32, loss_function="", metrics=["accuracy"],
                 optimizer_settings=None, callbacks=None, metric_callbacks=None, train_dataset=None, test_dataset=None,
                 evaluation_datasets=None, class_weights=None):
        super().__init__(settings_dir=run_dir, settings_filename="run.json")
        self.__model_filename = model_filename
        self.__model = None
        self.__datetime = datetime
        self.__run_dir = run_dir
        self.__epochs = epochs
        self.__batch_size = batch_size
        self.__class_weights = class_weights
        self.__loss = loss_function
        self.__metrics = metrics
        self.__optimizer = optimizer_settings
        self.__callbacks = [] if not callbacks else callbacks
        self.__metric_callbacks = [] if not metric_callbacks else metric_callbacks
        self.__train_dataset = train_dataset
        self.__test_dataset = test_dataset
        self.__evaluation_datasets = evaluation_datasets
        self.__model_checkpoint = ModelCheckpoint(self.model_checkpoint_path, verbose=1)
        self.__csv_logger = CSVLogger(self.csv_log_path)
        self.__history_plot = PlotHistoryCallback(self.history_plot_path)
        self.load_settings_automatically()

    @property
    def model_filename(self):
        return self.__model_filename

    @property
    def model(self):
        if not self.__model:
            self.__model = load_model(self.model_filename)
        return self.__model

    @model.setter
    def model(self, value):
        self.__model = value

    @property
    def datetime(self):
        return self.__datetime
    
    @property
    def run_dir(self):
        return self.__run_dir

    @property
    def model_checkpoint_path(self):
        return os.path.join(self.run_dir, "model.hdf5")

    @property
    def history_plot_path(self):
        return os.path.join(self.run_dir, "history_plot.png")

    @property
    def csv_log_path(self):
        return os.path.join(self.run_dir, "training_log.csv")

    @property
    def optimizer(self):
        return self.__optimizer

    @optimizer.setter
    def optimizer(self, value):
        self.__optimizer = value

    @property
    def loss(self):
        return self.__loss

    @loss.setter
    def loss(self, value):
        self.__loss = value

    @property
    def metrics(self):
        return self.__metrics

    @metrics.setter
    def metrics(self, value):
        self.__metrics = value

    @property
    def callbacks(self):
        return self.__callbacks

    @callbacks.setter
    def callbacks(self, value):
        self.__callbacks = value

    @property
    def default_callbacks(self):
        return [self.model_checkpoint, self.csv_logger, self.history_plot]

    @property
    def metric_callbacks(self):
        return self.__metric_callbacks

    @metric_callbacks.setter
    def metric_callbacks(self, value):
        self.__metric_callbacks = value

    @property
    def epochs(self):
        return self.__epochs

    @epochs.setter
    def epochs(self, value):
        self.__epochs = value
        
    @property
    def batch_size(self):
        return self.__batch_size
    
    @batch_size.setter
    def batch_size(self, value):
        self.__batch_size = value

    @property
    def class_weights(self):
        return self.__class_weights

    @class_weights.setter
    def class_weights(self, value):
        self.__class_weights = value

    @property
    def train_dataset(self):
        return self.__train_dataset

    @train_dataset.setter
    def train_dataset(self, value):
        self.__train_dataset = value

    @property
    def test_dataset(self):
        return self.__test_dataset

    @test_dataset.setter
    def test_dataset(self, value):
        self.__test_dataset = value

    @property
    def evaluation_datasets(self):
        return self.__evaluation_datasets

    @evaluation_datasets.setter
    def evaluation_datasets(self, value):
        self.__evaluation_datasets = value

    @property
    def model_checkpoint(self):
        return self.__model_checkpoint

    @model_checkpoint.setter
    def model_checkpoint(self, value):
        self.__model_checkpoint = value

    @property
    def csv_logger(self):
        return self.__csv_logger

    @csv_logger.setter
    def csv_logger(self, value):
        self.__csv_logger = value

    @property
    def history_plot(self):
        return self.__history_plot

    def with_optimizer(self, optimizer_settings):
        self.optimizer = optimizer_settings
        return self

    def with_loss_function(self, loss_function):
        self.loss = loss_function
        return self

    def with_metrics(self, metrics):
        self.metrics = metrics
        return self

    def with_callbacks(self, callbacks):
        self.callbacks = callbacks
        return self

    def with_metric_callbacks(self, metric_callbacks):
        self.metric_callbacks = metric_callbacks
        return self

    def with_epochs(self, epochs):
        self.epochs = epochs
        return self

    def with_batch_size(self, batch_size):
        self.batch_size = batch_size
        return self

    def with_class_weights(self, class_weights):
        self.class_weights = class_weights
        return self

    def with_train_dataset(self, train_dataset):
        self.train_dataset = train_dataset
        return self

    def with_test_dataset(self, test_dataset):
        self.test_dataset = test_dataset
        return self

    def with_evaluation_datasets(self, evaluation_datasets):
        self.evaluation_datasets = evaluation_datasets
        return self

    def with_evaluation_dataset(self, evaluation_dataset):
        return self.with_evaluation_datasets([evaluation_dataset])

    def with_model_checkpoint(self, monitor="val_loss", verbose=1, save_best_only=False, save_weights_only=False,
                              mode="auto", period=1):
        self.model_checkpoint = ModelCheckpoint(filepath=os.path.join(self.run_dir, "model.hdf5"), monitor=monitor,
                                                verbose=verbose, save_best_only=save_best_only,
                                                save_weights_only=save_weights_only, mode=mode, period=period)
        return self

    def with_csv_logger(self, separator=",", append=False):
        self.csv_logger = CSVLogger(filename=self.csv_log_path, separator=separator, append=append)
        return self

    def with_sgd_optimizer(self, lr=0.1, momentum=0.0, decay=0.0, nesterov=False):
        return self.with_optimizer(SGDSettings(lr=lr, momentum=momentum, decay=decay, nesterov=nesterov))

    def compile_model(self):
        self.model.compile(optimizer=self.optimizer.optimizer, loss=self.loss, metrics=self.metrics)

    def train(self):
        self.compile_model()
        self.model.fit_generator(self.train_dataset.flow(batch_size=self.batch_size),
                                 steps_per_epoch=len(self.train_dataset) // self.batch_size,
                                 epochs=self.epochs,
                                 callbacks=self.default_callbacks + self.callbacks,
                                 validation_data=self.test_dataset.flow(batch_size=self.batch_size),
                                 validation_steps=len(self.test_dataset) // self.batch_size,
                                 class_weight=self.class_weights)
        self.__model_filename = self.model_checkpoint_path

    def evaluate(self):
        # TODO change to predict, having a generator here (with shuffle) can create problems with comparison to labels
        # which are not shuffled.
        for evaluation_dataset in self.evaluation_datasets:
            predictions = self.model.predict_generator(evaluation_dataset.flow(batch_size=self.batch_size), verbose=1,
                                                       steps=len(evaluation_dataset) // self.batch_size)
            for metric_callback in self.metric_callbacks:
                metric_callback.evaluate(self.run_dir, evaluation_dataset, predictions)

    def clear_session(self):
        sess = get_session()
        clear_session()
        sess.close()
        sess = get_session()

        try:
            del self.__model
        except:
            pass

        print(gc.collect())

        # use the same config as you used to create the session
        config = tensorflow.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 1
        config.gpu_options.visible_device_list = "0"
        set_session(tensorflow.Session(config=config))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.save_settings()
        self.clear_session()

    def to_dict(self):
        return {
            "model_filename": self.model_filename,
            "datetime": self.datetime,
            "run_dir": self.run_dir,
            "epochs": self.epochs,
            "batch_size": self.batch_size,
            "loss_function": self.loss,
            "class_weights": self.class_weights,
            "optimizer": {
                "type": type(self.optimizer).__name__,
                "settings": self.optimizer.to_dict()
            },
            "train_dataset": self.train_dataset.to_dict(),
            "test_dataset": self.test_dataset.to_dict(),
            "evaluation_datasets": [evaluation_dataset.to_dict()
                                    for evaluation_dataset in self.evaluation_datasets],
            "SettingsProvider": super().to_dict()
        }

    def from_dict(self, settings_dict):
        super().from_dict(settings_dict["SettingsProvider"])
        self.__model_filename = settings_dict.get("model_filename", self.model_filename)
        self.__datetime = settings_dict.get("datetime", self.datetime)
        self.__run_dir = settings_dict.get("run_dir", self.run_dir)
        self.epochs = settings_dict.get("epochs", self.epochs)
        self.batch_size = settings_dict.get("batch_size", self.batch_size)
        self.loss = settings_dict.get("loss_function", self.loss)
        self.class_weights = settings_dict.get("class_weights", self.class_weights)
        self.class_weights = {int(key): value for key, value in self.class_weights.items()}
        self.optimizer = self.optimizer_for_settings(settings_dict)
        self.train_dataset = Dataset.instance_from_dict(settings_dict["train_dataset"])
        self.test_dataset = Dataset.instance_from_dict(settings_dict["test_dataset"])
        self.evaluation_datasets = [Dataset.instance_from_dict(evaluation_dataset_dict)
                                    for evaluation_dataset_dict in settings_dict["evaluation_datasets"]]
        self.model_checkpoint = ModelCheckpoint(self.model_checkpoint_path, verbose=1)
        self.csv_logger = CSVLogger(self.csv_log_path)

    @staticmethod
    def optimizer_for_settings(settings_dict):
        optimizer_dict = settings_dict["optimizer"]
        optimizer_type = optimizer_dict["type"]
        optimizer_settings = optimizer_dict["settings"]
        optimizer = OptimizerSettings().optimizer_for(optimizer_type)
        return optimizer.from_dict(optimizer_settings)

    @staticmethod
    def instance_from_dict(run_dict):
        run = Run(model_filename=None, datetime=None, run_dir=run_dict["run_dir"])
        run.from_dict(run_dict)
        return run
