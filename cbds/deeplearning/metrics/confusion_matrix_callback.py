from .metric_callback import MetricCallback
from sklearn.metrics import confusion_matrix
import numpy as np
import os


class ConfusionMatrixCallback(MetricCallback):
    def __init__(self):
        super()

    def evaluate(self, report_path, dataset, predictions, cut_off=0.5):
        confusion_matrix_filename = os.path.join(report_path, "confusion_matrix_{}.txt".format(dataset.name))
        y_true, y_pred = self.classification_labels_and_predictions_for(dataset.labels, predictions, cut_off)
        np.savetxt(confusion_matrix_filename, confusion_matrix(y_true, y_pred))
