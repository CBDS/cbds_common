from keras.callbacks import Callback
from cbds.deeplearning.history_plot import HistoryPlot
from collections import defaultdict


class PlotHistoryCallback(Callback):
    def __init__(self, report_path):
        super()
        self.__report_path = report_path
        self.__metrics = defaultdict(list)

    @property
    def report_path(self):
        return self.__report_path

    @property
    def metrics(self):
        return self.__metrics

    def on_epoch_end(self, epoch, logs):
        for key, value in logs.items():
            self.metrics[key].append(value)
        HistoryPlot.plot_and_save_history(self.metrics, self.report_path)
