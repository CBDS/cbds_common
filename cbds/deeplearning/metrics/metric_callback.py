import numpy as np

class MetricCallback:
    @staticmethod
    def predictions_to_labels(predictions, cut_off):
        predictions = MetricCallback.to_binary(predictions)
        return predictions >= cut_off

    @staticmethod
    def to_labels(one_hot_or_labels):
        if len(one_hot_or_labels.shape) == 2:
            return one_hot_or_labels.argmax(axis=1)
        return one_hot_or_labels

    @staticmethod
    def is_one_hot_encoded(predictions: np.array) -> bool: 
        return predictions.shape[-1] > 1

    @staticmethod
    def is_binary_one_hot(predictions: np.array) -> bool:
        return predictions.shape[-1] == 2

    @staticmethod
    def to_binary(labels):
        if MetricCallback.is_one_hot_encoded(labels) and MetricCallback.is_binary_one_hot(labels):
            return labels[1]
        return labels

    def classification_labels_and_predictions_for(self, labels, predictions, cut_off):
        y_true = MetricCallback.to_binary(labels[:len(predictions)])
        y_pred = self.predictions_to_labels(predictions, cut_off=cut_off)
        return y_true, y_pred

    def evaluate(self, report_path, dataset, predictions, cut_off):
        pass


