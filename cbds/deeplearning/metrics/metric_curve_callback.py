from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score, balanced_accuracy_score, jaccard_similarity_score, matthews_corrcoef, log_loss, classification_report
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from .metric_callback import MetricCallback
import os


class MetricCurveCallback(MetricCallback):
    def __init__(self, number_of_cut_offs=100):
        super()
        self.__number_of_cut_offs = number_of_cut_offs

    @property
    def number_of_cut_offs(self): 
        return self.__number_of_cut_offs

    @property
    def metrics(self):
        return {
            "precision": precision_score, 
            "recall": recall_score, 
            "f1": f1_score, 
            "accuracy": accuracy_score,
            "balanced_accuracy": balanced_accuracy_score,
            "jaccard_similarity": jaccard_similarity_score,
            "MCC": matthews_corrcoef,
            "log_loss": log_loss
            }

    def evaluate(self, report_path, dataset, predictions, cut_off=0.5):
        metric_curve_filename = os.path.join(report_path, "metric_curves_{}.png".format(dataset.name))
        best_cut_off = self.write_metric_curves(metric_curve_filename, self.metrics, dataset, predictions)

        classification_report_filename = os.path.join(report_path, "classification_report_best_cut_off_{}.txt".format(best_cut_off))
        with open(classification_report_filename, "w") as report_file:
            y_true, y_pred = self.classification_labels_and_predictions_for(dataset.labels, predictions, best_cut_off)
            report_file.write(classification_report(y_true, y_pred >= best_cut_off ))

    def write_metric_curves(self, filename, metrics, y_true, y_pred):
        cut_offs = np.linspace(0, 1, num=self.number_of_cut_offs)

        figure, ax = plt.subplots(2, 4)
        for index, (metric_name, metric) in enumerate(metrics.items()):
            r = int(index <= 1)
            c = int(index % 2 == 0)
            cut_off = self.plot_metric_curve(ax[r, c], metric_name, metric, y_true, y_pred, cut_offs, "training set", "b")
            if metric_name == "f1":
                best_cut_off = cut_off
        plt.legend()
        
        matplotlib.use("Agg")
        figure.savefig(filename)
        plt.close(figure)
        return best_cut_off

    def metric_curve(self, true_labels, predictions, cut_offs, metric):
        return np.array([metric(true_labels, self.predictions_to_labels(predictions, cut_off)) for cut_off in cut_offs])

    def plot_metric_curve(self, axes, metric_name, metric, true_labels, predictions, cut_offs, label, plot_color, cut_off_color="g"):
        metric_values = self.metric_curve(true_labels, predictions, cut_offs, metric)
        axes.set_title(metric_name)
        axes.set_ylim([0, 1])
        axes.set_xlim([0, 1])
        axes.plot(cut_offs, metric_values, color=plot_color, label=label)

        max_value_index = metric_values.argmax()
        best_cut_off = cut_offs[max_value_index]
        axes.axvline(best_cut_off, ymax=metric_values[max_value_index], color=cut_off_color, label="{}_cut_off".format(label))
        return best_cut_off
