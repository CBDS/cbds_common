from .metric_callback import MetricCallback
import numpy as np
import os


class SavePredictionsToDisk(MetricCallback):
    def __init__(self):
        super()

    def evaluate(self, report_path, dataset, predictions, cut_off):
        number_of_items = len(dataset.labels)
        cut_offs = np.ones(number_of_items) * cut_off
        prediction_data = np.stack((dataset.labels, predictions, cut_offs), axis=1)\
            .reshape((number_of_items, 3))
        np.save(os.path.join(report_path, "predictions_{}.npy".format(dataset.name)), prediction_data)