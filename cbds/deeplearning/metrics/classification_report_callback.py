from .metric_callback import MetricCallback
from sklearn.metrics import classification_report
import os


class ClassificationReportCallback(MetricCallback):
    def __init__(self):
        super()

    def evaluate(self, report_path, dataset, predictions, cut_off=0.5):
        classification_report_filename = os.path.join(report_path, "classification_report_{}.txt".format(dataset.name))
        with open(classification_report_filename, "w") as report_file:
            y_true, y_pred = self.classification_labels_and_predictions_for(dataset.labels, predictions, cut_off)
            report_file.write(classification_report(y_true, y_pred))
