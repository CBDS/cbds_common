from .metric_callback import MetricCallback
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import matplotlib
import os


class PlotRocCallback(MetricCallback):
    def __init__(self):
        super()

    def evaluate(self, report_path, dataset, predictions, cut_off=0.5):
        y_true = dataset.labels[:len(predictions)]
        false_positive_rate, true_positive_rate, _ = roc_curve(y_true, predictions)
        roc_auc = auc(false_positive_rate, true_positive_rate)
        roc_plot_filename = os.path.join(report_path, "roc_plot_{}.png".format(dataset.name))
        self.plot_roc_and_save(roc_plot_filename, false_positive_rate, true_positive_rate, roc_auc, cut_off)

    def plot_roc(self, false_positive_rate, true_positive_rate, roc_auc, cut_off):
        figure = plt.figure()
        lw = 2
        plt.plot(false_positive_rate, true_positive_rate, color='darkorange',
                 lw=lw, label='ROC curve' % roc_auc)
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.axvline(x=cut_off, linestyle="--")
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic')
        plt.legend(loc="lower right")
        return figure

    def plot_roc_and_save(self, filename, false_positive_rate, true_positive_rate, roc_auc, cut_off):
        matplotlib.use("Agg")
        figure = self.plot_roc(false_positive_rate, true_positive_rate, roc_auc, cut_off)
        figure.savefig(filename)
        plt.close(figure)
