from keras.models import Model
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, Flatten
from keras.utils.data_utils import get_file
from .model_utils import add_batch_normalization


WEIGHTS_PATH = ('https://github.com/fchollet/deep-learning-models/'
                'releases/download/v0.1/'
                'vgg16_weights_tf_dim_ordering_tf_kernels.h5')
WEIGHTS_PATH_NO_TOP = ('https://github.com/fchollet/deep-learning-models/'
                       'releases/download/v0.1/'
                        'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5')


def load_weights(model, include_top, weights):
    """Loads pre-trained weights for this vgg16 model.
    Copied from Keras code

    :param model: the vgg16 model for which the weights should be loaded
    :param include_top: whether this vgg16 includes fully-connected layers or not.
    :param weights: the weights that should be loaded
    """
    if weights == 'imagenet':
        if include_top:
            weights_path = get_file(
                'vgg16_weights_tf_dim_ordering_tf_kernels.h5',
                WEIGHTS_PATH,
                cache_subdir='models',
                file_hash='64373286793e3c8b2b4e3219cbf3544b')
        else:
            weights_path = get_file(
                'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                WEIGHTS_PATH_NO_TOP,
                cache_subdir='models',
                file_hash='6d6bbae143d832006294945121d1f1fc')
        model.load_weights(weights_path, by_name=True)
    elif weights is not None:
        model.load_weights(weights, by_name=True)


def vgg16_block_conv2(x, layer_depth, block_name, batch_normalization):
    x = Conv2D(layer_depth, (3, 3), activation="relu", padding="same", name=("%s_conv1" % block_name))(x)
    x = add_batch_normalization(x, name=("%s_batch1" % block_name), batch_normalization=batch_normalization)
    x = Conv2D(layer_depth, (3, 3), activation="relu", padding="same", name=("%s_conv2" % block_name))(x)
    x = add_batch_normalization(x, name=("%s_batch2" % block_name), batch_normalization=batch_normalization)
    x = MaxPooling2D((2, 2), strides=(2, 2), name=("%s_pool" % block_name))(x)
    return x


def vgg16_block_conv3(x, layer_depth, block_name, batch_normalization):
    x = Conv2D(layer_depth, (3, 3), activation="relu", padding="same", name=("%s_conv1" % block_name))(x)
    x = add_batch_normalization(x, name=("%s_batch1" % block_name), batch_normalization=batch_normalization)
    x = Conv2D(layer_depth, (3, 3), activation="relu", padding="same", name=("%s_conv2" % block_name))(x)
    x = add_batch_normalization(x, name=("%s_batch2" % block_name), batch_normalization=batch_normalization)
    x = Conv2D(layer_depth, (3, 3), activation="relu", padding="same", name=("%s_conv3" % block_name))(x)
    x = add_batch_normalization(x, name=("%s_batch3" % block_name), batch_normalization=batch_normalization)
    x = MaxPooling2D((2, 2), strides=(2, 2), name=("%s_pool" % block_name))(x)
    return x


def vgg16(input_shape, include_top=True, weights="imagenet", batch_normalization=True, classes=1000):
    # Block 1
    input_layer = Input(shape=input_shape)
    x = vgg16_block_conv2(input_layer, 64, "block1", batch_normalization)

    # Block 2
    x = vgg16_block_conv2(x, 128, "block2", batch_normalization)

    # Block 3
    x = vgg16_block_conv3(x, 256, "block3", batch_normalization)

    # Block 4
    x = vgg16_block_conv3(x, 512, "block4", batch_normalization)

    # Block 5
    x = vgg16_block_conv3(x, 512, "block5", batch_normalization)

    if include_top:
        x = Flatten(name="flatten")(x)
        x = Dense(4096, activation="relu", name="fc1")(x)
        x = Dense(4096, activation="relu", name="fc2")(x)
        x = Dense(classes, activation="softmax", name="predictions")(x)

    model = Model(input_layer, x, name="vgg16")
    load_weights(model, include_top, weights)

    return model


def vgg16_like(input_shape, blocks):
    input_layer = Input(shape=input_shape)
    x = input_layer
    for block in blocks:
        block_name = block["name"]
        layer_depth = block["layer_depth"]
        batch_normalization = block["batch_normalization"]
        if block["type"] == "conv2":
            x = vgg16_block_conv2(x, layer_depth, block_name, batch_normalization)
        else:
            x = vgg16_block_conv3(x, layer_depth, block_name, batch_normalization)
    return Model(input_layer, x)


def vgg16_like_from_list(input_shape, block_size_list):
    return vgg16_like(input_shape, [{
        "name": "block_{}".format(i),
        "layer_depth": layer_depth,
        "type": type,
        "batch_normalization": True
    } for i, (type, layer_depth) in enumerate(block_size_list)])


