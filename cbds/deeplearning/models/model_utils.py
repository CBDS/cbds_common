from keras.layers import BatchNormalization


def add_batch_normalization(x, axis=-1, name=None, batch_normalization=True):
    if not batch_normalization:
        return x
    return BatchNormalization(axis=axis, name=name)(x)
