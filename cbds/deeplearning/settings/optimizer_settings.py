from .sgd_settings import SGDSettings
from .rms_prop_settings import RMSPropSettings


class OptimizerSettings:
    def __init__(self):
        self.__optimizers = []

    @property
    def optimizers(self):
        if not self.__optimizers:
            self.__optimizers = [
                SGDSettings(),
                RMSPropSettings()
            ]
        return self.__optimizers

    @property
    def optimizer_dict(self):
        return {type(optimizer).__name__: optimizer for optimizer in self.optimizers}

    def __getitem__(self, optimizer_type_name):
        return self.optimizer_dict[optimizer_type_name]

    def optimizer_for(self, optimizer_type_name):
        return self[optimizer_type_name]
