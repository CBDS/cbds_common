from keras.optimizers import RMSprop


class RMSPropSettings:
    def __init__(self, lr=0.001, rho=0.9, epsilon=None, decay=0.0):
        self.__lr = lr
        self.__rho = rho
        self.__epsilon = epsilon
        self.__decay = decay

    @property
    def lr(self):
        return self.__lr

    @lr.setter
    def lr(self, value):
        self.__lr = value

    @property
    def rho(self):
        return self.__rho

    @rho.setter
    def rho(self, value):
        self.__rho = value

    @property
    def epsilon(self):
        return self.__epsilon

    @epsilon.setter
    def epsilon(self, value):
        self.__epsilon = value

    @property
    def decay(self):
        return self.__decay

    @decay.setter
    def decay(self, value):
        self.__decay = value

    @property
    def optimizer(self):
        return RMSprop(lr=self.lr, rho=self.rho, epsilon=self.epsilon, decay=self.decay)

    def with_lr(self, lr):
        self.lr = lr
        return self

    def with_rho(self, rho):
        self.rho = rho
        return self

    def with_epsilon(self, epsilon):
        self.epsilon = epsilon
        return self

    def with_decay(self, decay):
        self.decay = decay
        return self

    def to_dict(self):
        return {
            "lr": self.lr,
            "rho": self.rho,
            "epsilon": self.epsilon,
            "decay": self.decay
        }

    @staticmethod
    def from_dict(settings_dict):
        rms_prop_settings = RMSPropSettings()
        rms_prop_settings.lr = settings_dict.get("lr", rms_prop_settings.lr)
        rms_prop_settings.rho = settings_dict.get("rho", rms_prop_settings.rho)
        rms_prop_settings.epsilon = settings_dict.get("epsilon", rms_prop_settings.epsilon)
        rms_prop_settings.decay = settings_dict.get("decay", rms_prop_settings.decay)
        return rms_prop_settings
