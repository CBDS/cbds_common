from datetime import datetime
import os
import json
import re


class SettingsProvider:
    def __init__(self, settings_dir, settings_filename):
        self.__settings_dir = settings_dir
        self.__settings_filename = settings_filename

    @property
    def settings_dir(self):
        return self.__settings_dir

    @property
    def settings_filename(self):
        return self.__settings_filename

    @property
    def combined_settings_path(self):
        return os.path.join(self.settings_dir, self.settings_filename)

    def to_dict(self):
        return {
            "settings_dir": self.settings_dir,
            "settings_filename": self.settings_filename
        }

    def from_dict(self, json_settings):
        self.__settings_dir = json_settings.get("settings_dir", self.settings_dir)
        self.__settings_filename = json_settings.get("settings_filename", self.settings_filename)

    @staticmethod
    def instance_from_dict(json_settings):
        pass

    def load_settings_automatically(self):
        if not os.path.exists(self.combined_settings_path):
            return
        try:
            with open(self.combined_settings_path) as json_file:
                settings_dict = json.load(json_file, object_hook=datetime_parser)
                self.from_dict(settings_dict)
        except:
            pass

    def save_settings(self):
        self.save(self.combined_settings_path)

    @classmethod
    def load(cls, filename):
        with open(filename) as json_file:
            settings_dict = json.load(json_file, object_hook=datetime_parser)
            return cls.instance_from_dict(settings_dict)

    def save(self, filename):
        with open(filename, "w") as json_file:
            json.dump(self.to_dict(), json_file, cls=DateTimeAwareEncoder, indent=4, sort_keys=True)


class DateTimeAwareEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.strftime("%Y-%m-%dT%H:%M:%S")

        return json.JSONEncoder.default(self, o)


def datetime_parser(dictionary):
    # regex from: https://stackoverflow.com/questions/3143070/javascript-regex-iso-datetime
    for key, value in dictionary.items():
        if isinstance(value, str) and re.search(r"(\d{4}-[01]\d-[0-3]\dT)", value):
            try:
                dictionary[key] = datetime.strptime(value, "%Y-%m-%dT%H:%M:%S")
            except:
                pass
    return dictionary
