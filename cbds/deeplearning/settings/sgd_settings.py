from keras.optimizers import SGD


class SGDSettings(object):
    def __init__(self, lr=0.01, momentum=0.0, decay=0.0, nesterov=False):
        self.__lr = lr
        self.__momentum = momentum
        self.__decay = decay
        self.__nesterov = nesterov

    @property
    def lr(self):
        return self.__lr

    @lr.setter
    def lr(self, learning_rate):
        self.__lr = learning_rate

    @property
    def momentum(self):
        return self.__momentum

    @momentum.setter
    def momentum(self, momentum):
        self.__momentum = momentum

    @property
    def decay(self):
        return self.__decay

    @decay.setter
    def decay(self, decay):
        self.__decay = decay

    @property
    def nesterov(self):
        return self.__nesterov

    @nesterov.setter
    def nesterov(self, nesterov):
        self.__nesterov = nesterov

    @property
    def optimizer(self):
        return SGD(lr=self.lr, momentum=self.momentum, decay=self.decay, nesterov=self.nesterov)

    def with_lr(self, learning_rate):
        self.__lr = learning_rate
        return self

    def with_momentum(self, momentum):
        self.__momentum = momentum
        return self

    def with_decay(self, decay):
        self.__decay = decay
        return self

    def with_nesterov(self, nesterov):
        self.__nesterov = nesterov
        return self

    def to_dict(self):
        return {
            "lr": self.lr,
            "momentum": self.momentum,
            "decay": self.decay,
            "nesterov": self.nesterov
        }

    @staticmethod
    def from_dict(settings_dict):
        sgd_settings = SGDSettings()
        sgd_settings.lr = settings_dict.get("lr", sgd_settings.lr)
        sgd_settings.momentum = settings_dict.get("momentum", sgd_settings.momentum)
        sgd_settings.decay = settings_dict.get("decay", sgd_settings.decay)
        sgd_settings.nesterov = settings_dict.get("nesterov", sgd_settings.nesterov)
        return sgd_settings
