from sklearn.model_selection import train_test_split
from sklearn.utils import class_weight
from keras.preprocessing.image import ImageDataGenerator
from .settings import SettingsProvider
from cbds.utils import Utils
import numpy as np
import os
import shutil


class Dataset(SettingsProvider):
    def __init__(self, name=None, dataset_dir=""):
        super().__init__(dataset_dir, "dataset.json")
        self.__name = name
        self.__dataset_dir = dataset_dir
        self.__dataset_filename = os.path.join(self.dataset_dir, "{}.npy".format(self.name))
        self.__labels_filename = os.path.join(self.dataset_dir, "{}_labels.npy".format(self.name))
        self.__labels = []
        self.__data = []
        self.__image_data_generator = ImageDataGenerator()
        self.load_settings_automatically()

    @property
    def name(self):
        return self.__name

    @property
    def dataset_dir(self):
        return self.__dataset_dir

    @property
    def dataset_filename(self):
        return self.__dataset_filename

    @property
    def labels_filename(self):
        return self.__labels_filename

    @property
    def data(self):
        if len(self.__data) == 0:
            self.__data = np.load(self.dataset_filename)
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def labels(self):
        if len(self.__labels) == 0:
            self.__labels = np.load(self.labels_filename)
        return self.__labels

    @labels.setter
    def labels(self, value):
        self.__labels = value

    @property
    def image_data_generator(self):
        return self.__image_data_generator

    @image_data_generator.setter
    def image_data_generator(self, value):
        self.__image_data_generator = value

    @property
    def class_weights(self):
        return {class_index: weight
                for class_index, weight in enumerate(class_weight.compute_class_weight("balanced",
                                                                                       np.unique(self.labels),
                                                                                       self.labels))}

    def __len__(self):
        return len(self.labels)

    def import_numpy_dataset(self, source_dataset_path, source_dataset_labels_path):
        self.__dataset_filename = os.path.join(self.dataset_dir, os.path.basename(source_dataset_path))
        if os.path.exists(source_dataset_path):
            shutil.copyfile(source_dataset_path, self.dataset_filename)

        self.__labels_filename = os.path.join(self.dataset_dir, os.path.basename(source_dataset_labels_path))
        if os.path.exists(source_dataset_labels_path):
            shutil.copyfile(source_dataset_labels_path, self.labels_filename)

        return self

    def save_dataset(self):
        np.save(self.dataset_filename, self.data)
        np.save(self.labels_filename, self.labels)

    def sample(self, sample_size, random_state=None):
        sample_dataset = Dataset(name=self.name, dataset_dir=self.dataset_dir)

        np.random.seed(random_state)

        sample_data = None
        sample_labels = None
        for label in np.unique(self.labels):
            label_indices = np.where(self.labels == label)[0]
            data_for_label = self.data[label_indices, :]
            labels_for_label = self.labels[label_indices]
            sample_indices = np.random.randint(low=0, high=len(labels_for_label),
                                               size=int(sample_size*len(labels_for_label)))

            selected_data = np.take(data_for_label, sample_indices, axis=0)
            selected_labels = np.take(labels_for_label, sample_indices, axis=0)
            if sample_data is None and sample_labels is None:
                sample_data = selected_data
                sample_labels = selected_labels
                continue

            sample_data = np.concatenate((sample_data, selected_data))
            sample_labels = np.concatenate((sample_labels, selected_labels))

        sample_dataset.data = sample_data
        sample_dataset.labels = sample_labels
        return sample_dataset

    def split(self, test_size, random_state=None, first_name=None, second_name=None):
        (trainX, testX, trainY, testY) = train_test_split(self.data, self.labels, test_size=test_size,
                                                          random_state=random_state, stratify=self.labels)
        
        train_name = self.name if not first_name else first_name
        datasets_dir, _ = os.path.split(self.dataset_dir)
        train_dir = os.path.join(datasets_dir, train_name)
        Utils.makedirs_if_not_exist(train_dir)
        train_set = Dataset(name=train_name, dataset_dir=train_dir)
        train_set.__data = trainX
        train_set.__labels = trainY

        test_name = self.name if not second_name else second_name
        test_dir = os.path.join(datasets_dir, test_name)
        Utils.makedirs_if_not_exist(test_dir)
        test_set = Dataset(name=test_name, dataset_dir=test_dir)
        test_set.__data = testX
        test_set.__labels = testY

        return train_set, test_set

    def flow(self, batch_size=32):
        return self.image_data_generator.flow(self.data, self.labels, batch_size, shuffle=False)

    def from_dict(self, dataset_dict):
        self.__name = dataset_dict.get("name", "")
        self.__dataset_dir = dataset_dict.get("dataset_dir", "")
        self.__dataset_filename = dataset_dict.get("dataset_filename", self.dataset_filename)
        self.__labels_filename = dataset_dict.get("labels_filename", self.labels_filename)
        super().from_dict(dataset_dict["SettingsProvider"])

    def to_dict(self):
        return {
            "name": self.name,
            "dataset_dir": self.dataset_dir,
            "dataset_filename": self.dataset_filename,
            "labels_filename": self.labels_filename,
            "SettingsProvider": super().to_dict()
        }

    @staticmethod
    def instance_from_dict(dataset_dict):
        dataset = Dataset(dataset_dir=dataset_dict.get("dataset_dir", None))
        dataset.from_dict(dataset_dict)
        return dataset
