import numpy as np
import matplotlib.pyplot as plt
import matplotlib


class HistoryPlot:
    @classmethod
    def plot_history(cls, history):
        training_keys = sorted([key for key in history.keys() if not key.startswith("val")])
        validation_keys = sorted([key for key in history.keys() if key.startswith("val")])

        fig, ax = plt.subplots(len(training_keys), 1)
        epochs_ran = len(history["loss"])
        for index, (training_key, validation_key) in enumerate(zip(training_keys, validation_keys)):
            axes = ax[index]
            cls.plot_training_validation_history(axes, epochs_ran, history[training_key],
                                                 history[validation_key], training_key, validation_key)
        return fig

    @classmethod
    def plot_training_validation_history(cls, axes, epochs_ran, training_history, validation_history, training_key,
                                         validation_key):
        plt.title("History for {}/{}".format(training_key, validation_key))
        plt.ylabel("{}/{}".format(training_key, validation_key))
        plt.xlabel("Epoch")
        axes.set_xlim([0, epochs_ran])
        axes.plot(np.arange(0, epochs_ran), training_history, label=training_key)
        axes.plot(np.arange(0, epochs_ran), validation_history, label=validation_key, color="r")
        axes.legend()

    @classmethod
    def plot_and_save_history(cls, history, filename):
        matplotlib.use("Agg")
        figure = cls.plot_history(history)
        figure.savefig(filename)
        plt.close(figure)
