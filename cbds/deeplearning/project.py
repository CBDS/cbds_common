from .settings import SettingsProvider
from .model import Model
from .dataset import Dataset
from .image_generator import ImageGenerator
from cbds.utils import Utils
import os


class Project(SettingsProvider):
    def __init__(self, project_path=""):
        super().__init__(project_path, "project.json")
        self.__project_path = project_path
        self.__opened_models = dict()
        self.__opened_datasets = dict()
        self.load_settings_automatically()

    @property
    def project_path(self):
        return self.__project_path

    @property
    def models_path(self):
        return os.path.join(self.project_path, "models")

    @property
    def datasets_path(self):
        return os.path.join(self.project_path, "datasets")

    @property
    def models(self):
        return [model_path for model_path in sorted(os.listdir(self.models_path))
                if os.path.isdir(os.path.join(self.models_path, model_path))]

    @property
    def datasets(self):
        return [dataset_path for dataset_path in sorted(os.listdir(self.datasets_path))
                if os.path.isdir(os.path.join(self.datasets_path, dataset_path))]

    @property
    def opened_models(self):
        return self.__opened_models

    @opened_models.setter
    def opened_models(self, value):
        self.__opened_models = value

    @property
    def opened_datasets(self):
        return self.__opened_datasets

    @opened_datasets.setter
    def opened_datasets(self, value):
        self.__opened_datasets = value

    def create_dirs(self):
        Utils.makedirs_if_not_exist(self.models_path)
        Utils.makedirs_if_not_exist(self.datasets_path)

    def model(self, model_name):
        if model_name in self.opened_models:
            return self.opened_models[model_name]

        model = Model(name=model_name, model_dir=os.path.join(self.models_path, model_name))
        Utils.makedirs_if_not_exist(model.model_dir)
        self.opened_models[model.name] = model
        return model

    def dataset(self, dataset_name):
        if dataset_name in self.opened_datasets:
            return self.opened_datasets[dataset_name]

        dataset = Dataset(name=dataset_name, dataset_dir=os.path.join(self.datasets_path, dataset_name))
        Utils.makedirs_if_not_exist(dataset.dataset_dir)
        self.opened_datasets[dataset.name] = dataset
        return dataset

    def image_generator_for_dataset(self, dataset_name):
        return ImageGenerator(self.dataset(dataset_name))

    def save(self, filename):
        super().save(filename)
        for opened_model in self.opened_models.values():
            opened_model.save_settings()
        for opened_dataset in self.opened_datasets.values():
            opened_dataset.save_settings()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.save_settings()

    def from_dict(self, json_settings):
        settings = json_settings["Project"]
        super().from_dict(settings["SettingsProvider"])
        self.__project_path = settings.get("project_path", self.project_path)

    def to_dict(self):
        return {"Project": {
            "project_path": self.project_path,
            "SettingsProvider": super().to_dict()
        }}

    @staticmethod
    def instance_from_dict(json_settings):
        project = Project(project_path=json_settings.get("Project", json_settings)\
            .get("project_path", ""))
        project.from_dict(json_settings)
        return project
