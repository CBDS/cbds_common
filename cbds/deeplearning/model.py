from .settings import SettingsProvider
from keras.models import load_model
from keras.utils import plot_model
from sklearn.model_selection import ParameterSampler
from .metrics import ClassificationReportCallback, ConfusionMatrixCallback, PlotRocCallback, PlotHistoryCallback
from shutil import copy
from datetime import datetime
from .run import Run
from cbds.utils import Utils
import os


class Model(SettingsProvider):
    def __init__(self, name=None, model_dir=""):
        super().__init__(model_dir, "model.json")
        self.__name = name
        self.__model = None
        self.__model_dir = model_dir
        self.__model_filename = os.path.join(self.model_dir, "{}.hdf5".format(self.name))
        self.__opened_runs = dict()
        self.__sklearn_model = None
        self.load_settings_automatically()

    @property
    def name(self):
        return self.__name

    @property
    def model_dir(self):
        return self.__model_dir

    @property
    def model_filename(self):
        return self.__model_filename

    @property
    def model(self):
        if not self.__model and os.path.exists(self.model_filename):
            self.__model = load_model(self.model_filename)
        return self.__model

    @property
    def runs(self):
        run_dirs = ["{}-{}-{} {}".format(*run_dir[0].split("/")[-4:]) for run_dir in sorted(os.walk(os.path.join(self.model_dir, "runs")))
                    if os.path.isdir(run_dir[0]) and not run_dir[1] and "runs" in run_dir[0]]
        return [datetime.strptime(run_dir, "%Y-%m-%d %H:%M:%S") for run_dir in run_dirs]

    @property
    def opened_runs(self):
        return self.__opened_runs

    @opened_runs.setter
    def opened_runs(self, value):
        self.__opened_runs = value

    @property
    def current_run(self):
        return list(self.opened_runs.values())[-1]

    def create_model(self, model):
        model.save(self.model_filename)

    def import_model(self, model_path):
        copy(model_path, self.model_dir)
        self.__model_filename = os.path.join(self.model_dir, os.path.basename(model_path))

    def run(self, date=None):
        if not date:
            date = datetime.now()

        if date in self.opened_runs:
            return self.opened_runs[date]
        subdirs = "runs/{year}/{month}/{day}/{time}".format(year=date.year, month=date.month, day=date.day,
                                                            time="{:02d}:{:02d}:{:02d}"
                                                            .format(date.hour, date.minute, date.second))
        run_dir = os.path.join(self.model_dir, subdirs)
        Utils.makedirs_if_not_exist(run_dir)
        run = Run(model_filename=self.model_filename, datetime=date, run_dir=run_dir)
        self.opened_runs[run.datetime] = run
        return run

    def random_search(self, train_dataset, test_dataset, param_distributions, n_iter=10, random_state=None):
        for run_settings in ParameterSampler(param_distributions, n_iter, random_state):
            print("New run with settings: {}".format(run_settings))
            with self.run().with_epochs(run_settings["epochs"])\
                .with_batch_size(run_settings["batch_size"])\
                .with_loss_function(run_settings["loss_function"])\
                .with_sgd_optimizer(lr=run_settings["lr"], momentum=run_settings["momentum"],
                                    decay=self.decay_settings_for(run_settings), nesterov=run_settings["nesterov"])\
                .with_callbacks(run_settings.get("callbacks", []))\
                .with_metric_callbacks([ClassificationReportCallback(), ConfusionMatrixCallback(), PlotRocCallback()])\
                .with_train_dataset(train_dataset)\
                .with_test_dataset(test_dataset)\
                .with_evaluation_dataset(test_dataset) as run:
                run.train()
                run.evaluate()

    def decay_settings_for(self, run_settings):
        if run_settings.get("calculate_decay", False):
            return run_settings["lr"] / run_settings["epochs"]
        return run_settings.get("decay", 0.0)

    def save(self, filename):
        super().save(filename)
        for opened_run in self.opened_runs.values():
            opened_run.save_settings()

    def plot(self):
        plot_model(self.model, to_file=os.path.join(self.model_dir, "{}.png".format(self.name)), show_shapes=True)

    def from_dict(self, model_dict):
        super().from_dict(model_dict["SettingsProvider"])
        self.__name = model_dict["name"]
        self.__model_dir = model_dict["model_dir"]
        self.__model_filename = model_dict["model_filename"]

    def to_dict(self):
        return {
            "name": self.name,
            "model_dir": self.model_dir,
            "model_filename": self.model_filename,
            "SettingsProvider": super().to_dict()
        }

    @staticmethod
    def instance_from_dict(model_dict):
        model = Model(model_dir=model_dict["model_dir"])
        model.from_dict(model_dict)
        return model
