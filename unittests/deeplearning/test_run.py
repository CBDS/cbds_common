import unittest
from unittest.mock import MagicMock, PropertyMock, call, patch
from datetime import datetime
from cbds.deeplearning import Run
from cbds.deeplearning.settings import SGDSettings, RMSPropSettings
from cbds.deeplearning import Dataset
from cbds.deeplearning.metrics import ClassificationReportCallback, ConfusionMatrixCallback, PlotRocCallback
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from unittests.testutils import TestUtils
import os


class RunTests(unittest.TestCase):
    def setUp(self):
        self.model_filename = r"test_data/project/models/model1/model1.hdf5"
        os.makedirs(r"test_data/project/datasets/training_set")
        os.makedirs(r"test_data/project/datasets/test_set")
        os.makedirs(r"test_data/project/models/model1/runs/2019/4/23/16:58:00")
        os.makedirs(r"test_data/project/models/model1/runs/2019/5/8/11:30:10")

    def tearDown(self):
        TestUtils.rm_dir_if_exists(r"test_data/project/datasets/training_set")
        TestUtils.rm_dir_if_exists(r"test_data/project/datasets/test_set")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model1/runs/2019/4/23/16:58:00")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model1/runs/2019/5/8/11:30:10")

    def test_run_sets_model_filename(self):
        self.assertEqual(self.model_filename, Run(self.model_filename, datetime=datetime.now(), run_dir="")
                         .model_filename)

    def test_run_sets_date(self):
        self.assertEqual(datetime(2015, 1, 1, 11, 1, 10),
                         Run(model_filename=self.model_filename, datetime=datetime(2015, 1, 1, 11, 1, 10), run_dir="").datetime)
        self.assertEqual(datetime(2016, 3, 2, 12, 5, 1),
                         Run(model_filename=self.model_filename, datetime=datetime(2016, 3, 2, 12, 5, 1), run_dir="").datetime)
        self.assertEqual(datetime(2017, 8, 15, 17, 10, 5),
                         Run(model_filename=self.model_filename, datetime=datetime(2017, 8, 15, 17, 10, 5), run_dir="").datetime)
        self.assertEqual(datetime(2018, 10, 21, 0, 23, 30),
                         Run(model_filename=self.model_filename, datetime=datetime(2018, 10, 21, 0, 23, 30), run_dir="").datetime)
        self.assertEqual(datetime(2019, 12, 30, 1, 55, 56),
                         Run(model_filename=self.model_filename, datetime=datetime(2019, 12, 30, 1, 55, 56), run_dir="").datetime)

    def test_run_sets_run_dir(self):
        self.assertEqual(r"runs/2015/1/1/11:01:10",
                         Run(model_filename=self.model_filename, datetime=datetime(2015, 1, 1, 11, 1, 10),
                             run_dir=r"runs/2015/1/1/11:01:10").run_dir)
        self.assertEqual(r"runs/2016/3/2/12:05:01",
                         Run(model_filename=self.model_filename, datetime=datetime(2016, 3, 2, 12, 5, 1),
                             run_dir=r"runs/2016/3/2/12:05:01").run_dir)
        self.assertEqual(r"runs/2017/8/15/17:10:05",
                         Run(model_filename=self.model_filename, datetime=datetime(2017, 8, 15, 17, 10, 5),
                             run_dir=r"runs/2017/8/15/17:10:05").run_dir)
        self.assertEqual(r"runs/2018/10/21/00:23:30",
                         Run(model_filename=self.model_filename, datetime=datetime(2018, 10, 21, 0, 23, 30),
                             run_dir=r"runs/2018/10/21/00:23:30").run_dir)
        self.assertEqual(r"runs/2019/12/30/01:55:56",
                         Run(model_filename=self.model_filename, datetime=datetime(2019, 12, 30, 1, 55, 56),
                             run_dir=r"runs/2019/12/30/01:55:56").run_dir)

    def test_history_plot_path_returns_correct_path(self):
        self.assertEqual(r"runs/2015/1/1/11:01:10/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime(2015, 1, 1, 11, 1, 10),
                             run_dir=r"runs/2015/1/1/11:01:10").history_plot_path)
        self.assertEqual(r"runs/2016/3/2/12:05:01/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime(2016, 3, 2, 12, 5, 1),
                             run_dir=r"runs/2016/3/2/12:05:01").history_plot_path)
        self.assertEqual(r"runs/2017/8/15/17:10:05/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime(2017, 8, 15, 17, 10, 5),
                             run_dir=r"runs/2017/8/15/17:10:05").history_plot_path)
        self.assertEqual(r"runs/2018/10/21/00:23:30/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime(2018, 10, 21, 0, 23, 30),
                             run_dir=r"runs/2018/10/21/00:23:30").history_plot_path)
        self.assertEqual(r"runs/2019/12/30/01:55:56/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime(2019, 12, 30, 1, 55, 56),
                             run_dir=r"runs/2019/12/30/01:55:56").history_plot_path)

    def test_csv_log_path_returns_correct_path(self):
        self.assertEqual(r"runs/2015/1/1/11:01:10/training_log.csv",
                         Run(model_filename=self.model_filename, datetime=datetime(2015, 1, 1, 11, 1, 10),
                             run_dir=r"runs/2015/1/1/11:01:10").csv_log_path)
        self.assertEqual(r"runs/2016/3/2/12:05:01/training_log.csv",
                         Run(model_filename=self.model_filename, datetime=datetime(2016, 3, 2, 12, 5, 1),
                             run_dir=r"runs/2016/3/2/12:05:01").csv_log_path)
        self.assertEqual(r"runs/2017/8/15/17:10:05/training_log.csv",
                         Run(model_filename=self.model_filename, datetime=datetime(2017, 8, 15, 17, 10, 5),
                             run_dir=r"runs/2017/8/15/17:10:05").csv_log_path)
        self.assertEqual(r"runs/2018/10/21/00:23:30/training_log.csv",
                         Run(model_filename=self.model_filename, datetime=datetime(2018, 10, 21, 0, 23, 30),
                             run_dir=r"runs/2018/10/21/00:23:30").csv_log_path)
        self.assertEqual(r"runs/2019/12/30/01:55:56/training_log.csv",
                         Run(model_filename=self.model_filename, datetime=datetime(2019, 12, 30, 1, 55, 56),
                             run_dir=r"runs/2019/12/30/01:55:56").csv_log_path)

    def test_run_epochs_set_correctly(self):
        self.assertEqual(10, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", epochs=10).epochs)
        self.assertEqual(20, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", epochs=20).epochs)
        self.assertEqual(30, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", epochs=30).epochs)
        self.assertEqual(40, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", epochs=40).epochs)
        self.assertEqual(50, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", epochs=50).epochs)

    def test_with_epochs_sets_epochs_correctly(self):
        self.assertEqual(10, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").with_epochs(10).epochs)
        self.assertEqual(20, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").with_epochs(20).epochs)
        self.assertEqual(30, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").with_epochs(30).epochs)
        self.assertEqual(40, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").with_epochs(40).epochs)
        self.assertEqual(50, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").with_epochs(50).epochs)

    def test_run_batch_size_set_correctly(self):
        self.assertEqual(16, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", batch_size=16).batch_size)
        self.assertEqual(32, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", batch_size=32).batch_size)
        self.assertEqual(64, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", batch_size=64).batch_size)
        self.assertEqual(128, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", batch_size=128).batch_size)
        self.assertEqual(256, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="", batch_size=256).batch_size)

    def test_with_batch_size_sets_batch_size_correctly(self):
        self.assertEqual(16, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_batch_size(16).batch_size)
        self.assertEqual(32, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_batch_size(32).batch_size)
        self.assertEqual(64, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_batch_size(64).batch_size)
        self.assertEqual(128, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_batch_size(128).batch_size)
        self.assertEqual(256, Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_batch_size(256).batch_size)

    def test_run_loss_function_set_correctly(self):
        self.assertEqual("categorical_crossentropy",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             loss_function="categorical_crossentropy").loss)

        self.assertEqual("binary_crossentropy",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             loss_function="binary_crossentropy").loss)

        self.assertEqual("hinge",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             loss_function="hinge").loss)

        self.assertEqual("categorical_hinge",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             loss_function="categorical_hinge").loss)

        self.assertEqual("mean_squared_error",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             loss_function="mean_squared_error").loss)

    def test_with_loss_function_sets_loss_function_correctly(self):
        self.assertEqual("categorical_crossentropy",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_loss_function("categorical_crossentropy").loss)

        self.assertEqual("binary_crossentropy",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_loss_function("binary_crossentropy").loss)

        self.assertEqual("hinge",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_loss_function("hinge").loss)

        self.assertEqual("categorical_hinge",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_loss_function("categorical_hinge").loss)

        self.assertEqual("mean_squared_error",
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_loss_function("mean_squared_error").loss)

    def test_metrics_sets_metrics_correctly(self):
        self.assertEqual(["accuracy"], Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                           metrics=["accuracy"]).metrics)
        self.assertEqual(["accuracy", "mean_squared_error"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             metrics=["accuracy", "mean_squared_error"]).metrics)
        self.assertEqual(["accuracy", "mean_squared_error", "categorical_accuracy"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             metrics=["accuracy", "mean_squared_error", "categorical_accuracy"]).metrics)

        self.assertEqual(["accuracy", "mean_squared_error", "binary_accuracy"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             metrics=["accuracy", "mean_squared_error", "binary_accuracy"]).metrics)

        self.assertEqual(["accuracy", "mean_squared_error", "top_k_categorical_accuracy"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             metrics=["accuracy", "mean_squared_error", "top_k_categorical_accuracy"]).metrics)

    def test_with_metrics_sets_metrics_correctly(self):
        self.assertEqual(["accuracy"], Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_metrics(["accuracy"]).metrics)
        self.assertEqual(["accuracy", "mean_squared_error"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_metrics(["accuracy", "mean_squared_error"]).metrics)
        self.assertEqual(["accuracy", "mean_squared_error", "categorical_accuracy"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_metrics(["accuracy", "mean_squared_error", "categorical_accuracy"]).metrics)

        self.assertEqual(["accuracy", "mean_squared_error", "binary_accuracy"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_metrics(["accuracy", "mean_squared_error", "binary_accuracy"]).metrics)

        self.assertEqual(["accuracy", "mean_squared_error", "top_k_categorical_accuracy"],
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_metrics(["accuracy", "mean_squared_error", "top_k_categorical_accuracy"]).metrics)

    # TODO replace SGDSettings by SGD
    def test_optimizer_set_correctly(self):
        self.assertEqual(0.1,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             optimizer_settings=SGDSettings(lr=0.1))
                         .optimizer.lr)
        self.assertEqual(0.01,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             optimizer_settings=SGDSettings(lr=0.01))
                         .optimizer.lr)
        self.assertEqual(0.001,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             optimizer_settings=SGDSettings(lr=0.001))
                         .optimizer.lr)
        self.assertEqual(0.0001,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             optimizer_settings=SGDSettings(lr=0.0001))
                         .optimizer.lr)
        self.assertEqual(0.00001,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                             optimizer_settings=SGDSettings(lr=0.00001))
                         .optimizer.lr)

    # TODO replace SGDSettings by SGD
    def test_with_optimizer_sets_optimizer_settings(self):
        self.assertEqual(0.1,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_optimizer(SGDSettings(lr=0.1)).optimizer.lr)
        self.assertEqual(0.01,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_optimizer(SGDSettings(lr=0.01)).optimizer.lr)
        self.assertEqual(0.001,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_optimizer(SGDSettings(lr=0.001)).optimizer.lr)
        self.assertEqual(0.0001,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_optimizer(SGDSettings(lr=0.0001)).optimizer.lr)
        self.assertEqual(0.00001,
                         Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_optimizer(SGDSettings(lr=0.00001)).optimizer.lr)

    def test_callbacks_set_correctly(self):
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  callbacks=[EarlyStopping()]).callbacks[0],
                              EarlyStopping)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  callbacks=[EarlyStopping(), ModelCheckpoint("test_data")]).callbacks[0],
                              EarlyStopping)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  callbacks=[EarlyStopping(), ModelCheckpoint("test_data")]).callbacks[1],
                              ModelCheckpoint)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  callbacks=[EarlyStopping(), ModelCheckpoint("test_data"),
                                             TensorBoard()]).callbacks[0],
                              EarlyStopping)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  callbacks=[EarlyStopping(), ModelCheckpoint("test_data"),
                                             TensorBoard()]).callbacks[1],
                              ModelCheckpoint)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  callbacks=[EarlyStopping(), ModelCheckpoint("test_data"),
                                             TensorBoard()]).callbacks[2],
                              TensorBoard)

    def test_with_callbacks_sets_callbacks_correctly(self):
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                              .with_callbacks([EarlyStopping()]).callbacks[0],
                              EarlyStopping)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_callbacks([EarlyStopping(), ModelCheckpoint("test_data")]).callbacks[0],
                              EarlyStopping)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_callbacks([EarlyStopping(), ModelCheckpoint("test_data")]).callbacks[1],
                              ModelCheckpoint)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_callbacks([EarlyStopping(), ModelCheckpoint("test_data"),
                                              TensorBoard()]).callbacks[0],
                              EarlyStopping)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_callbacks([EarlyStopping(), ModelCheckpoint("test_data"),
                                              TensorBoard()]).callbacks[1],
                              ModelCheckpoint)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_callbacks([EarlyStopping(), ModelCheckpoint("test_data"),
                                              TensorBoard()]).callbacks[2],
                              TensorBoard)

    def test_metric_callbacks_set_correctly(self):
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  metric_callbacks=[ClassificationReportCallback()]).metric_callbacks[0],
                              ClassificationReportCallback)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  metric_callbacks=[ClassificationReportCallback(),
                                                    ConfusionMatrixCallback()]).metric_callbacks[0],
                              ClassificationReportCallback)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  metric_callbacks=[ClassificationReportCallback(),
                                                    ConfusionMatrixCallback()]).metric_callbacks[1],
                              ConfusionMatrixCallback)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  metric_callbacks=[ClassificationReportCallback(), ConfusionMatrixCallback(),
                                                    PlotRocCallback()]).metric_callbacks[0],
                              ClassificationReportCallback)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  metric_callbacks=[ClassificationReportCallback(), ConfusionMatrixCallback(),
                                                    PlotRocCallback()]).metric_callbacks[1],
                              ConfusionMatrixCallback)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                  metric_callbacks=[ClassificationReportCallback(), ConfusionMatrixCallback(),
                                                    PlotRocCallback()]).metric_callbacks[2],
                              PlotRocCallback)

    def test_with_metric_callbacks_sets_metric_callbacks_correctly(self):
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                              .with_metric_callbacks([ClassificationReportCallback()]).metric_callbacks[0],
                              ClassificationReportCallback)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_metric_callbacks(
            [ClassificationReportCallback(), ConfusionMatrixCallback()]).metric_callbacks[0],
                              ClassificationReportCallback)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_metric_callbacks(
            [ClassificationReportCallback(), ConfusionMatrixCallback()]).metric_callbacks[1],
                              ConfusionMatrixCallback)

        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_metric_callbacks([ClassificationReportCallback(), ConfusionMatrixCallback(),
                                                     PlotRocCallback()]).metric_callbacks[0],
                              ClassificationReportCallback)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_metric_callbacks([ClassificationReportCallback(), ConfusionMatrixCallback(),
                                                     PlotRocCallback()]).metric_callbacks[1],
                              ConfusionMatrixCallback)
        self.assertIsInstance(Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="").
                              with_metric_callbacks([ClassificationReportCallback(), ConfusionMatrixCallback(),
                                                     PlotRocCallback()]).metric_callbacks[2],
                              PlotRocCallback)

    def test_train_dataset_set_correctly(self):
        self.assertEqual("dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                         train_dataset=Dataset(name="dataset1", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                         train_dataset=Dataset(name="dataset2", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset3", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                         train_dataset=Dataset(name="dataset3", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset4", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                         train_dataset=Dataset(name="dataset4", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset5", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                         train_dataset=Dataset(name="dataset5", dataset_dir=""))
                         .train_dataset.name)

    def test_with_train_dataset_sets_train_dataset(self):
        self.assertEqual("dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_train_dataset(Dataset(name="dataset1", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_train_dataset(Dataset(name="dataset2", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset3", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_train_dataset(Dataset(name="dataset3", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset4", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_train_dataset(Dataset(name="dataset4", dataset_dir=""))
                         .train_dataset.name)
        self.assertEqual("dataset5", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_train_dataset(Dataset(name="dataset5", dataset_dir=""))
                         .train_dataset.name)

    def test_test_dataset_set_correctly(self):
        self.assertEqual("test_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                              test_dataset=Dataset(name="test_dataset1", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                              test_dataset=Dataset(name="test_dataset2", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset3", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                              test_dataset=Dataset(name="test_dataset3", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset4", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                              test_dataset=Dataset(name="test_dataset4", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset5", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                              test_dataset=Dataset(name="test_dataset5", dataset_dir=""))
                         .test_dataset.name)

    def test_with_test_dataset_sets_test_dataset(self):
        self.assertEqual("test_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_test_dataset(Dataset(name="test_dataset1", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_test_dataset(Dataset(name="test_dataset2", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset3", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_test_dataset(Dataset(name="test_dataset3", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset4", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_test_dataset(Dataset(name="test_dataset4", dataset_dir=""))
                         .test_dataset.name)
        self.assertEqual("test_dataset5", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_test_dataset(Dataset(name="test_dataset5", dataset_dir=""))
                         .test_dataset.name)

    def test_evaluation_datasets_set_correctly(self):
        evaluation_datasets = [Dataset(name="evaluation_dataset1", dataset_dir="")]
        self.assertEqual("evaluation_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                                    evaluation_datasets=evaluation_datasets).evaluation_datasets[
            0].name)

        evaluation_datasets.append(Dataset(name="evaluation_dataset2", dataset_dir=""))
        self.assertEqual("evaluation_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                                    evaluation_datasets=evaluation_datasets).evaluation_datasets[
            0].name)
        self.assertEqual("evaluation_dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                                    evaluation_datasets=evaluation_datasets).evaluation_datasets[
            1].name)

        evaluation_datasets.append(Dataset(name="evaluation_dataset3", dataset_dir=""))
        self.assertEqual("evaluation_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                                    evaluation_datasets=evaluation_datasets).evaluation_datasets[
            0].name)
        self.assertEqual("evaluation_dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                                    evaluation_datasets=evaluation_datasets).evaluation_datasets[
            1].name)
        self.assertEqual("evaluation_dataset3", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="",
                                                    evaluation_datasets=evaluation_datasets).evaluation_datasets[
            2].name)

    def test_with_evaluation_datasets_sets_evaluation_datasets(self):
        evaluation_datasets = [Dataset(name="evaluation_dataset1", dataset_dir="")]
        self.assertEqual("evaluation_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_datasets(evaluation_datasets).evaluation_datasets[
            0].name)

        evaluation_datasets.append(Dataset(name="evaluation_dataset2", dataset_dir=""))
        self.assertEqual("evaluation_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_datasets(evaluation_datasets)
                         .evaluation_datasets[0].name)
        self.assertEqual("evaluation_dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_datasets(evaluation_datasets)
                         .evaluation_datasets[1].name)

        evaluation_datasets.append(Dataset(name="evaluation_dataset3", dataset_dir=""))
        self.assertEqual("evaluation_dataset1", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_datasets(evaluation_datasets)
                         .evaluation_datasets[0].name)
        self.assertEqual("evaluation_dataset2", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_datasets(evaluation_datasets)
                         .evaluation_datasets[1].name)
        self.assertEqual("evaluation_dataset3", Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_datasets(evaluation_datasets)
                         .evaluation_datasets[2].name)

    def test_with_evaluation_dataset_sets_evaluation_datasets(self):
        dataset1 = Dataset(name="dataset1", dataset_dir="")
        self.assertEqual([dataset1], Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_dataset(dataset1).evaluation_datasets)

        dataset2 = Dataset(name="dataset2", dataset_dir="")
        self.assertEqual([dataset2], Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_dataset(dataset2).evaluation_datasets)

        dataset3 = Dataset(name="dataset3", dataset_dir="")
        self.assertEqual([dataset3], Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir="")
                         .with_evaluation_dataset(dataset3).evaluation_datasets)

    def test_with_model_checkpoint_sets_model_checkpoint_to_save_one_file_only(self):
        run1 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2015/1/1/11:01:10/")
        run1.with_model_checkpoint()
        self.assertEqual("runs/2015/1/1/11:01:10/model.hdf5", run1.model_checkpoint.filepath)
        self.assertEqual("val_loss", run1.model_checkpoint.monitor)
        self.assertEqual(1, run1.model_checkpoint.verbose)
        self.assertFalse(run1.model_checkpoint.save_best_only)
        self.assertFalse(run1.model_checkpoint.save_weights_only)
        self.assertEqual("<ufunc 'less'>", str(run1.model_checkpoint.monitor_op))
        self.assertEqual(1, run1.model_checkpoint.period)

        run2 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2016/2/5/12:01:10/")
        run2.with_model_checkpoint(monitor="val_acc", verbose=0, save_best_only=True, save_weights_only=False,
                                   mode="min", period=2)
        self.assertEqual("runs/2016/2/5/12:01:10/model.hdf5", run2.model_checkpoint.filepath)
        self.assertEqual("val_acc", run2.model_checkpoint.monitor)
        self.assertEqual(0, run2.model_checkpoint.verbose)
        self.assertTrue(run2.model_checkpoint.save_best_only)
        self.assertFalse(run2.model_checkpoint.save_weights_only)
        self.assertEqual("<ufunc 'less'>", str(run2.model_checkpoint.monitor_op))
        self.assertEqual(2, run2.model_checkpoint.period)

        run3 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2017/10/31/1:01:10/")
        run3.with_model_checkpoint(save_weights_only=True, mode="max", period=3)
        self.assertEqual("runs/2017/10/31/1:01:10/model.hdf5", run3.model_checkpoint.filepath)
        self.assertEqual("val_loss", run3.model_checkpoint.monitor)
        self.assertEqual(1, run3.model_checkpoint.verbose)
        self.assertFalse(run3.model_checkpoint.save_best_only)
        self.assertTrue(run3.model_checkpoint.save_weights_only)
        self.assertEqual("<ufunc 'greater'>", str(run3.model_checkpoint.monitor_op))
        self.assertEqual(3, run3.model_checkpoint.period)

    def test_with_csv_logger_set_csv_logger(self):
        run1 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2015/1/1/11:01:10/")
        run1.with_csv_logger()
        self.assertEqual(run1.csv_log_path, run1.csv_logger.filename)
        # self.assertEqual(",", run1.csv_logger.separator)
        self.assertFalse(run1.csv_logger.append)

        run2 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2016/2/5/12:01:10/")
        run2.with_csv_logger(separator=";")
        self.assertEqual(run2.csv_log_path, run2.csv_logger.filename)
        # self.assertEqual(";", run2.csv_logger.separator)
        self.assertFalse(run2.csv_logger.append)

        run3 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2017/10/31/1:01:10/")
        run3.with_csv_logger(separator="-", append=True)
        self.assertEqual(run3.csv_log_path, run3.csv_logger.filename)
        # self.assertEqual("-", run3.csv_logger.separator)
        self.assertTrue(run3.csv_logger.append)

    def test_constructor_sets_class_weights(self):
        self.assertDictEqual({1: 0.5, 2: 1}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                                                 run_dir=r"runs/2015/1/1/11:01:10/", class_weights={1: 0.5, 2: 1})
                             .class_weights)
        self.assertDictEqual({0: 2.5, 1: 1}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                                                 run_dir=r"runs/2015/1/1/11:01:10/",  class_weights={0: 2.5, 1: 1})
                             .class_weights)
        self.assertDictEqual({1: 0.5, 2: 1, 3: 0.5}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                                                         run_dir=r"runs/2015/1/1/11:01:10/",
                                                         class_weights={1: 0.5, 2: 1, 3: 0.5}).class_weights)

        self.assertDictEqual({0: 50, 1: 1, 2: 10, 3: 25}, Run(model_filename=self.model_filename,
                                                              datetime=datetime.now(),
                                                              run_dir=r"runs/2015/1/1/11:01:10/",
                                                              class_weights={0: 50, 1: 1, 2: 10, 3: 25}).class_weights)
        self.assertDictEqual({0: 10, 1: 0.5, 2: 1}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                                                        run_dir=r"runs/2015/1/1/11:01:10/",
                                                        class_weights={0: 10, 1: 0.5, 2: 1}).class_weights)

    def test_with_class_weights_sets_class_weights(self):
        self.assertDictEqual({1: 0.5, 2: 1}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                             run_dir=r"runs/2015/1/1/11:01:10/").with_class_weights({1: 0.5, 2: 1}).class_weights)
        self.assertDictEqual({0: 2.5, 1: 1}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                             run_dir=r"runs/2015/1/1/11:01:10/").with_class_weights({0: 2.5, 1: 1}).class_weights)
        self.assertDictEqual({1: 0.5, 2: 1, 3: 0.5}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                             run_dir=r"runs/2015/1/1/11:01:10/")
                             .with_class_weights({1: 0.5, 2: 1, 3: 0.5}).class_weights)
        self.assertDictEqual({0: 50, 1: 1, 2: 10, 3: 25}, Run(model_filename=self.model_filename,
                                                              datetime=datetime.now(),
                             run_dir=r"runs/2015/1/1/11:01:10/")
                             .with_class_weights({0: 50, 1: 1, 2: 10, 3: 25}).class_weights)
        self.assertDictEqual({0: 10, 1: 0.5, 2: 1}, Run(model_filename=self.model_filename, datetime=datetime.now(),
                             run_dir=r"runs/2015/1/1/11:01:10/").with_class_weights({0: 10, 1: 0.5, 2: 1}).class_weights)

    def test_returns_plot_history_callbacks_with_correct_report_path(self):
        self.assertEqual(r"runs/2015/1/1/11:01:10/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime.now(),
                             run_dir=r"runs/2015/1/1/11:01:10/")
                         .history_plot.report_path)
        self.assertEqual(r"runs/2016/2/5/12:01:10/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime.now(),
                             run_dir=r"runs/2016/2/5/12:01:10/")
                         .history_plot.report_path)
        self.assertEqual(r"runs/2017/10/31/1:01:10/history_plot.png",
                         Run(model_filename=self.model_filename, datetime=datetime.now(),
                             run_dir=r"runs/2017/10/31/1:01:10/")
                         .history_plot.report_path)

    def test_default_callbacks_returns_default_callbacks(self):
        run1 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2015/1/1/11:01:10/")
        self.assertEqual(run1.model_checkpoint, run1.default_callbacks[0])
        self.assertEqual(run1.csv_logger, run1.default_callbacks[1])
        self.assertEqual(run1.history_plot, run1.default_callbacks[2])

    def test_with_sgd_optimizer_sets_optimizer_for_run(self):
        run1 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2015/1/1/11:01:10")
        run1.with_sgd_optimizer(lr=0.001, momentum=0.8, decay=0.00001, nesterov=False)
        self.assertIsInstance(run1.optimizer, SGDSettings)
        self.assertEqual(0.001, run1.optimizer.lr)
        self.assertEqual(0.8, run1.optimizer.momentum)
        self.assertEqual(0.00001, run1.optimizer.decay)
        self.assertFalse(run1.optimizer.nesterov)

        run2 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2015/1/1/11:01:10")
        run2.with_sgd_optimizer(lr=0.0001, momentum=0.7, decay=0.0001, nesterov=True)
        self.assertIsInstance(run2.optimizer, SGDSettings)
        self.assertEqual(0.0001, run2.optimizer.lr)
        self.assertEqual(0.7, run2.optimizer.momentum)
        self.assertEqual(0.0001, run2.optimizer.decay)
        self.assertTrue(run2.optimizer.nesterov)

        run3 = Run(model_filename=self.model_filename, datetime=datetime.now(), run_dir=r"runs/2015/1/1/11:01:10")
        run3.with_sgd_optimizer(lr=0.01, momentum=0.6, decay=0.001, nesterov=True)
        self.assertIsInstance(run3.optimizer, SGDSettings)
        self.assertEqual(0.01, run3.optimizer.lr)
        self.assertEqual(0.6, run3.optimizer.momentum)
        self.assertEqual(0.001, run3.optimizer.decay)
        self.assertTrue(run3.optimizer.nesterov)

    def test_to_dict_returns_dict_with_run_settings(self):
        self.maxDiff = None
        train_dataset = Dataset(name="train_dataset")
        test_dataset = Dataset(name="test_dataset")
        evalation_datasets = [Dataset(name="evaluation_dataset1"), Dataset(name="evaluation_dataset2")]
        self.assertDictEqual({
            "model_filename": self.model_filename,
            "datetime": datetime(2019, 4, 23, 11, 36, 9),
            "run_dir": r"runs/2019/4/23/11:36:09",
            "epochs": 10,
            "batch_size": 64,
            "loss_function": "binary_crossentropy",
            "class_weights": {0: 0.5, 1: 1},
            "optimizer": {
                "type": "SGDSettings",
                "settings": {
                    "lr": 0.01,
                    "momentum": 0.9,
                    "decay": 0.0005,
                    "nesterov": True
                }
            },
            "train_dataset": {
                "name": "train_dataset",
                "dataset_dir": "",
                "dataset_filename": "train_dataset.npy",
                "labels_filename": "train_dataset_labels.npy",
                "SettingsProvider": {
                    "settings_dir": "",
                    "settings_filename": "dataset.json"
                }
            },
            "test_dataset": {
                "name": "test_dataset",
                "dataset_dir": "",
                "dataset_filename": "test_dataset.npy",
                "labels_filename": "test_dataset_labels.npy",
                "SettingsProvider": {
                    "settings_dir": "",
                    "settings_filename": "dataset.json"
                }
            },
            "evaluation_datasets": [
                {
                    "name": "evaluation_dataset1",
                    "dataset_dir": "",
                    "dataset_filename": "evaluation_dataset1.npy",
                    "labels_filename": "evaluation_dataset1_labels.npy",
                    "SettingsProvider": {
                        "settings_dir": "",
                        "settings_filename": "dataset.json"
                    }
                },
                {
                    "name": "evaluation_dataset2",
                    "dataset_dir": "",
                    "dataset_filename": "evaluation_dataset2.npy",
                    "labels_filename": "evaluation_dataset2_labels.npy",
                    "SettingsProvider": {
                        "settings_dir": "",
                        "settings_filename": "dataset.json"
                    }
                }
            ],
            "SettingsProvider": {
                "settings_dir": r"runs/2019/4/23/11:36:09",
                "settings_filename": "run.json"
            }
        }, Run(model_filename=self.model_filename, datetime=datetime(2019, 4, 23, 11, 36, 9), run_dir=r"runs/2019/4/23/11:36:09",
               epochs=10, batch_size=64, loss_function="binary_crossentropy",
               optimizer_settings=SGDSettings(lr=0.01, momentum=0.9, decay=0.0005, nesterov=True),
               train_dataset=train_dataset, test_dataset=test_dataset, evaluation_datasets=evalation_datasets,
               class_weights={0: 0.5, 1: 1}
               ).to_dict())

        self.assertDictEqual({
            "model_filename": self.model_filename,
            "datetime": datetime(2019, 4, 23, 11, 36, 9),
            "run_dir": r"runs/2019/4/23/11:36:09",
            "epochs": 10,
            "batch_size": 64,
            "loss_function": "binary_crossentropy",
            "class_weights": {0: 0.5, 1: 1},
            "optimizer": {
                "type": "RMSPropSettings",
                "settings": {
                    "lr": 0.01,
                    "rho": 0.8,
                    "epsilon": 1e-6,
                    "decay": 1e-7,
                },
            },
            "train_dataset": {
                "name": "train_dataset",
                "dataset_dir": "",
                "dataset_filename": "train_dataset.npy",
                "labels_filename": "train_dataset_labels.npy",
                "SettingsProvider": {
                    "settings_dir": "",
                    "settings_filename": "dataset.json"
                }
            },
            "test_dataset": {
                "name": "test_dataset",
                "dataset_dir": "",
                "dataset_filename": "test_dataset.npy",
                "labels_filename": "test_dataset_labels.npy",
                "SettingsProvider": {
                    "settings_dir": "",
                    "settings_filename": "dataset.json"
                }
            },
            "evaluation_datasets": [
                {
                    "name": "evaluation_dataset1",
                    "dataset_dir": "",
                    "dataset_filename": "evaluation_dataset1.npy",
                    "labels_filename": "evaluation_dataset1_labels.npy",
                    "SettingsProvider": {
                        "settings_dir": "",
                        "settings_filename": "dataset.json"
                    }
                },
                {
                    "name": "evaluation_dataset2",
                    "dataset_dir": "",
                    "dataset_filename": "evaluation_dataset2.npy",
                    "labels_filename": "evaluation_dataset2_labels.npy",
                    "SettingsProvider": {
                        "settings_dir": "",
                        "settings_filename": "dataset.json"
                    }
                }
            ],
            "SettingsProvider": {
                "settings_dir": r"runs/2019/4/23/11:36:09",
                "settings_filename": "run.json"
            }
        }, Run(model_filename=self.model_filename, datetime=datetime(2019, 4, 23, 11, 36, 9),
               run_dir=r"runs/2019/4/23/11:36:09",
               epochs=10, batch_size=64, loss_function="binary_crossentropy",
               optimizer_settings=RMSPropSettings(lr=0.01, rho=0.8, epsilon=1e-6, decay=1e-7),
               train_dataset=train_dataset, test_dataset=test_dataset, evaluation_datasets=evalation_datasets,
               class_weights={0: 0.5, 1: 1}
               ).to_dict())

    def test_from_dict_returns_object_with_settings_from_dict(self):
        run = Run(model_filename=self.model_filename, datetime=None, run_dir="")
        run.from_dict(
            {
                "model_filename": r"test_data/project/models/model1/runs/2019/4/23/11:36:09",
                "datetime": datetime(2019, 4, 23, 11, 36, 9),
                "run_dir": r"runs/2019/4/23/11:36:09",
                "epochs": 10,
                "batch_size": 64,
                "loss_function": "binary_crossentropy",
                "class_weights": {0: 0.5, 1: 1},
                "optimizer": {
                    "type": "SGDSettings",
                    "settings": {
                        "lr": 0.01,
                        "momentum": 0.9,
                        "decay": 0.0005,
                        "nesterov": True
                     },
                },
                "train_dataset": {
                    "name": "train_dataset",
                    "dataset_dir": r"test_data/project/datasets/train_dataset",
                    "dataset_filename": r"test_data/project/datasets/train_dataset/train_dataset.npy",
                    "labels_filename": r"test_data/project/datasets/train_dataset/train_dataset_labels.npy",
                    "SettingsProvider": {
                        "settings_dir": "test_data/project/datasets/train_dataset",
                        "settings_filename": "dataset.json"
                    }
                },
                "test_dataset": {
                    "name": "test_dataset",
                    "dataset_dir": r"test_data/project/datasets/test_dataset",
                    "dataset_filename": r"test_data/project/datasets/test_dataset/test_dataset.npy",
                    "labels_filename": r"test_data/project/datasets/test_dataset/test_dataset_labels.npy",
                    "SettingsProvider": {
                        "settings_dir": r"test_data/project/datasets/test_dataset",
                        "settings_filename": "dataset.json"
                    }
                },
                "evaluation_datasets": [
                    {
                        "name": "evaluation_dataset1",
                        "dataset_dir": r"test_data/project/datasets/evaluation_dataset1",
                        "dataset_filename": r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1.npy",
                        "labels_filename": r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1_labels.npy",
                        "SettingsProvider": {
                            "settings_dir": r"test_data/project/datasets/evaluation_dataset1",
                            "settings_filename": "dataset.json"
                        }
                    },
                    {
                        "name": "evaluation_dataset2",
                        "dataset_dir": r"test_data/project/datasets/evaluation_dataset2",
                        "dataset_filename": r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2.npy",
                        "labels_filename":
                            r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2_labels.npy",
                        "SettingsProvider": {
                            "settings_dir": "test_data/project/datasets/evaluation_dataset2",
                            "settings_filename": "dataset.json"
                        }
                    }
                ],
                "SettingsProvider": {
                    "settings_dir": r"runs/2019/4/23/11:36:09",
                    "settings_filename": "run.json"
                }
            }
        )
        self.assertEqual(r"test_data/project/models/model1/runs/2019/4/23/11:36:09", run.model_filename)
        self.assertEqual(datetime(2019, 4, 23, 11, 36, 9), run.datetime)
        self.assertEqual(r"runs/2019/4/23/11:36:09", run.run_dir)
        self.assertEqual(10, run.epochs)
        self.assertEqual(64, run.batch_size)
        self.assertDictEqual({0: 0.5, 1: 1}, run.class_weights)
        self.assertEqual("binary_crossentropy", run.loss)
        self.assertEqual(0.01, run.optimizer.lr)
        self.assertEqual(0.9, run.optimizer.momentum)
        self.assertEqual(0.0005, run.optimizer.decay)
        self.assertEqual(True, run.optimizer.nesterov)

        self.assertEqual("train_dataset", run.train_dataset.name)
        self.assertEqual(r"test_data/project/datasets/train_dataset", run.train_dataset.dataset_dir)
        self.assertEqual(r"test_data/project/datasets/train_dataset", run.train_dataset.settings_dir)
        self.assertEqual("dataset.json", run.train_dataset.settings_filename)
        self.assertEqual(r"test_data/project/datasets/train_dataset/train_dataset.npy", run.train_dataset
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/train_dataset/train_dataset_labels.npy", run.train_dataset
                         .labels_filename)

        self.assertEqual("test_dataset", run.test_dataset.name)
        self.assertEqual(r"test_data/project/datasets/test_dataset", run.test_dataset.dataset_dir)
        self.assertEqual(r"test_data/project/datasets/test_dataset", run.test_dataset.settings_dir)
        self.assertEqual("dataset.json", run.test_dataset.settings_filename)
        self.assertEqual(r"test_data/project/datasets/test_dataset/test_dataset.npy", run.test_dataset
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/test_dataset/test_dataset_labels.npy", run.test_dataset
                         .labels_filename)

        self.assertEqual("evaluation_dataset1", run.evaluation_datasets[0].name)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1",
                         run.evaluation_datasets[0].dataset_dir)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1",
                         run.evaluation_datasets[0].settings_dir)
        self.assertEqual("dataset.json", run.evaluation_datasets[0].settings_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1.npy",
                         run.evaluation_datasets[0]
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1_labels.npy",
                         run.evaluation_datasets[0]
                         .labels_filename)

        self.assertEqual("evaluation_dataset2", run.evaluation_datasets[1].name)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2",
                         run.evaluation_datasets[1].dataset_dir)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2",
                         run.evaluation_datasets[1].settings_dir)
        self.assertEqual("dataset.json", run.evaluation_datasets[1].settings_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2.npy",
                         run.evaluation_datasets[1]
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2_labels.npy",
                         run.evaluation_datasets[1]
                         .labels_filename)

        self.assertEqual(r"runs/2019/4/23/11:36:09", run.settings_dir)
        self.assertEqual("run.json", run.settings_filename)
        self.assertEqual(r"runs/2019/4/23/11:36:09/model.hdf5", run.model_checkpoint.filepath)
        self.assertEqual(r"runs/2019/4/23/11:36:09/training_log.csv", run.csv_logger.filename)

    def test_load_loads_run_settings_from_disk(self):
        run = Run.load(r"test_data/run.json")
        self.assertEqual(r"test_data/project/models/model1/runs/2018/4/23/11:36:09", run.model_filename)
        self.assertEqual(datetime(2018, 4, 23, 11, 36, 9), run.datetime)
        self.assertEqual(r"runs/2018/4/23/11:36:09", run.run_dir)
        self.assertEqual(20, run.epochs)
        self.assertEqual(128, run.batch_size)
        self.assertDictEqual({0: 10, 1: 20}, run.class_weights)
        self.assertEqual("categorical_crossentropy", run.loss)
        self.assertEqual(0.001, run.optimizer.lr)
        self.assertEqual(0.8, run.optimizer.momentum)
        self.assertEqual(0.005, run.optimizer.decay)
        self.assertEqual(False, run.optimizer.nesterov)

        self.assertEqual("train_dataset", run.train_dataset.name)
        self.assertEqual(r"test_data/project/datasets/train_dataset", run.train_dataset.dataset_dir)
        self.assertEqual(r"test_data/project/datasets/train_dataset", run.train_dataset.settings_dir)
        self.assertEqual("dataset.json", run.train_dataset.settings_filename)
        self.assertEqual(r"test_data/project/datasets/train_dataset/train_dataset.npy", run.train_dataset
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/train_dataset/train_dataset_labels.npy", run.train_dataset
                         .labels_filename)

        self.assertEqual("test_dataset", run.test_dataset.name)
        self.assertEqual(r"test_data/project/datasets/test_dataset", run.test_dataset.dataset_dir)
        self.assertEqual(r"test_data/project/datasets/test_dataset", run.test_dataset.settings_dir)
        self.assertEqual("dataset.json", run.test_dataset.settings_filename)
        self.assertEqual(r"test_data/project/datasets/test_dataset/test_dataset.npy", run.test_dataset
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/test_dataset/test_dataset_labels.npy", run.test_dataset
                         .labels_filename)

        self.assertEqual("evaluation_dataset1", run.evaluation_datasets[0].name)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1",
                         run.evaluation_datasets[0].dataset_dir)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1",
                         run.evaluation_datasets[0].settings_dir)
        self.assertEqual("dataset.json", run.evaluation_datasets[0].settings_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1.npy",
                         run.evaluation_datasets[0]
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1_labels.npy",
                         run.evaluation_datasets[0]
                         .labels_filename)

        self.assertEqual("evaluation_dataset2", run.evaluation_datasets[1].name)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2",
                         run.evaluation_datasets[1].dataset_dir)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2",
                         run.evaluation_datasets[1].settings_dir)
        self.assertEqual("dataset.json", run.evaluation_datasets[1].settings_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2.npy",
                         run.evaluation_datasets[1]
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2_labels.npy",
                         run.evaluation_datasets[1]
                         .labels_filename)

        self.assertEqual(r"runs/2018/4/23/11:36:09", run.settings_dir)
        self.assertEqual("run.json", run.settings_filename)
        self.assertEqual(r"runs/2018/4/23/11:36:09/model.hdf5", run.model_checkpoint.filepath)
        self.assertEqual(r"runs/2018/4/23/11:36:09/training_log.csv", run.csv_logger.filename)

    def test_loads_run_settings_from_disk_automatically(self):
        run = Run(model_filename=self.model_filename, datetime=None, run_dir=r"test_data")
        self.assertEqual(r"test_data/project/models/model1/runs/2018/4/23/11:36:09", run.model_filename)
        self.assertEqual(datetime(2018, 4, 23, 11, 36, 9), run.datetime)
        self.assertEqual(r"runs/2018/4/23/11:36:09", run.run_dir)
        self.assertEqual(20, run.epochs)
        self.assertEqual(128, run.batch_size)
        self.assertDictEqual({0: 10, 1: 20}, run.class_weights)
        self.assertEqual("categorical_crossentropy", run.loss)
        self.assertEqual(0.001, run.optimizer.lr)
        self.assertEqual(0.8, run.optimizer.momentum)
        self.assertEqual(0.005, run.optimizer.decay)
        self.assertEqual(False, run.optimizer.nesterov)

        self.assertEqual("train_dataset", run.train_dataset.name)
        self.assertEqual(r"test_data/project/datasets/train_dataset", run.train_dataset.dataset_dir)
        self.assertEqual(r"test_data/project/datasets/train_dataset", run.train_dataset.settings_dir)
        self.assertEqual("dataset.json", run.train_dataset.settings_filename)
        self.assertEqual(r"test_data/project/datasets/train_dataset/train_dataset.npy", run.train_dataset
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/train_dataset/train_dataset_labels.npy", run.train_dataset
                         .labels_filename)

        self.assertEqual("test_dataset", run.test_dataset.name)
        self.assertEqual(r"test_data/project/datasets/test_dataset", run.test_dataset.dataset_dir)
        self.assertEqual(r"test_data/project/datasets/test_dataset", run.test_dataset.settings_dir)
        self.assertEqual("dataset.json", run.test_dataset.settings_filename)
        self.assertEqual(r"test_data/project/datasets/test_dataset/test_dataset.npy", run.test_dataset
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/test_dataset/test_dataset_labels.npy", run.test_dataset
                         .labels_filename)

        self.assertEqual("evaluation_dataset1", run.evaluation_datasets[0].name)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1",
                         run.evaluation_datasets[0].dataset_dir)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1",
                         run.evaluation_datasets[0].settings_dir)
        self.assertEqual("dataset.json", run.evaluation_datasets[0].settings_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1.npy",
                         run.evaluation_datasets[0]
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset1/evaluation_dataset1_labels.npy",
                         run.evaluation_datasets[0]
                         .labels_filename)

        self.assertEqual("evaluation_dataset2", run.evaluation_datasets[1].name)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2",
                         run.evaluation_datasets[1].dataset_dir)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2",
                         run.evaluation_datasets[1].settings_dir)
        self.assertEqual("dataset.json", run.evaluation_datasets[1].settings_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2.npy",
                         run.evaluation_datasets[1]
                         .dataset_filename)
        self.assertEqual(r"test_data/project/datasets/evaluation_dataset2/evaluation_dataset2_labels.npy",
                         run.evaluation_datasets[1]
                         .labels_filename)

        self.assertEqual(r"runs/2018/4/23/11:36:09", run.settings_dir)
        self.assertEqual("run.json", run.settings_filename)

    def test_save_saves_settings_to_disk(self):
        run = Run(model_filename=self.model_filename, datetime=datetime(2019, 4, 23, 16, 46, 00), run_dir=r"test_data/",
                  optimizer_settings=SGDSettings(),
                  train_dataset=Dataset(name="train_dataset"), test_dataset=Dataset(name="test_dataset"),
                  evaluation_datasets=[Dataset(name="evaluation_dataset1"), Dataset(name="evaluation_dataset2")])
        run.save(r"test_data/project/models/model1/runs/2019/4/23/16:58:00/run1.json")
        saved_run = Run.load(r"test_data/project/models/model1/runs/2019/4/23/16:58:00/run1.json")

        self.assertEqual(run.model_filename, saved_run.model_filename)
        self.assertEqual(run.datetime, saved_run.datetime)
        self.assertEqual(run.run_dir, saved_run.run_dir)
        self.assertEqual(run.settings_dir, saved_run.settings_dir)
        self.assertEqual(run.settings_filename, saved_run.settings_filename)
        self.assertEqual(run.optimizer.lr, saved_run.optimizer.lr)
        self.assertEqual(run.optimizer.momentum, saved_run.optimizer.momentum)
        self.assertEqual(run.optimizer.decay, saved_run.optimizer.decay)
        self.assertEqual(run.optimizer.nesterov, saved_run.optimizer.nesterov)
        self.assertEqual(run.loss, saved_run.loss)
        self.assertEqual(run.metrics, saved_run.metrics)
        self.assertEqual(run.callbacks, saved_run.callbacks)
        self.assertEqual(run.metric_callbacks, saved_run.metric_callbacks)
        self.assertEqual(run.epochs, saved_run.epochs)
        self.assertEqual(run.batch_size, saved_run.batch_size)
        self.assertDictEqual(run.class_weights, saved_run.class_weights)

        self.assertEqual("train_dataset", run.train_dataset.name)
        self.assertEqual("test_dataset", run.test_dataset.name)
        self.assertEqual("evaluation_dataset1", run.evaluation_datasets[0].name)
        self.assertEqual("evaluation_dataset2", run.evaluation_datasets[1].name)

    def test_settings_filename_set_correctly(self):
        self.assertEqual("run.json", Run(model_filename=self.model_filename, datetime=datetime(2015, 8, 9, 9),
                                         run_dir=r"runs/2015/8/9/9:00:00").settings_filename)
        self.assertEqual("run.json", Run(model_filename=self.model_filename, datetime=datetime(2016, 9, 10, 10),
                                         run_dir=r"runs/2016/9/10/10:00:00").settings_filename)
        self.assertEqual("run.json", Run(model_filename=self.model_filename, datetime=datetime(2017, 10, 11, 11),
                                         run_dir=r"runs/2017/10/11/11:00:00").settings_filename)
        self.assertEqual("run.json", Run(model_filename=self.model_filename, datetime=datetime(2018, 11, 12, 12),
                                         run_dir=r"runs/2018/11/12/12:00:00").settings_filename)
        self.assertEqual("run.json", Run(model_filename=self.model_filename, datetime=datetime(2019, 12, 13, 13),
                                         run_dir=r"runs/2019/12/13/13:00:00").settings_filename)

    def test_settings_dir_set_correctly(self):
        self.assertEqual(r"runs/2015/8/9/9:00:00", Run(model_filename=self.model_filename, datetime=datetime(2015, 8, 9, 9),
                                                       run_dir=r"runs/2015/8/9/9:00:00").settings_dir)
        self.assertEqual(r"runs/2016/9/10/10:00:00", Run(model_filename=self.model_filename, datetime=datetime(2016, 9, 10, 10),
                                                         run_dir=r"runs/2016/9/10/10:00:00").settings_dir)
        self.assertEqual(r"runs/2017/10/11/11:00:00", Run(model_filename=self.model_filename, datetime=datetime(2017, 10, 11, 11),
                                                          run_dir=r"runs/2017/10/11/11:00:00").settings_dir)
        self.assertEqual(r"runs/2018/11/12/12:00:00", Run(model_filename=self.model_filename, datetime=datetime(2018, 11, 12, 12),
                                                          run_dir=r"runs/2018/11/12/12:00:00").settings_dir)
        self.assertEqual(r"runs/2019/12/13/13:00:00", Run(model_filename=self.model_filename, datetime=datetime(2019, 12, 13, 13),
                                                          run_dir=r"runs/2019/12/13/13:00:00").settings_dir)


class RunTrainTestEvaluateTests(unittest.TestCase):
    def setUp(self):
        self.model = TestUtils.create_example_model()
        TestUtils.rm_file_if_exists(r"test_data/project/datasets/training_data/dataset1.py")
        TestUtils.rm_file_if_exists(r"test_data/project/datasets/test_data/dataset2.py")
        os.makedirs(r"test_data/project/datasets/training_set")
        os.makedirs(r"test_data/project/datasets/test_set")
        self.model_mock = MagicMock()

        self.train_dataset = TestUtils.example_training_set(r"test_data/project/datasets/training_set")
        self.train_flow_mock = MagicMock()
        self.train_dataset.flow = MagicMock(return_value=self.train_flow_mock)

        self.test_dataset = TestUtils.example_test_set(r"test_data/project/datasets/test_set")
        self.test_flow_mock = MagicMock()
        self.test_dataset.flow = MagicMock(return_value=self.test_flow_mock)

    def tearDown(self):
        TestUtils.rm_dir_if_exists(r"test_data/project/datasets/training_set")
        TestUtils.rm_dir_if_exists(r"test_data/project/datasets/test_set")

    def test_train_compiles_model(self):
        optimizer_mock = PropertyMock()
        sgd_settings = MagicMock()
        sgd_settings.optimizer = optimizer_mock

        run = Run(model_filename="", datetime=datetime.now(), run_dir="", epochs=100, optimizer_settings=sgd_settings,
                  loss_function="categorical_crossentropy", metrics=["accuracy"], train_dataset=self.train_dataset,
                  test_dataset=self.test_dataset)
        run.model = self.model_mock
        run.train()
        self.model_mock.compile.assert_called_with(optimizer=optimizer_mock, loss="categorical_crossentropy",
                                                   metrics=["accuracy"])

    def test_train_calls_fit_generator_with_train_test_generators(self):
        sgd_settings = SGDSettings()

        run = Run(model_filename="", datetime=datetime.now(), run_dir="", epochs=100,
                  optimizer_settings=sgd_settings,
                  loss_function="categorical_crossentropy", metrics=["accuracy"], train_dataset=self.train_dataset,
                  test_dataset=self.test_dataset, class_weights={0: 1, 1: 20})
        run.model = self.model_mock
        run.train()
        self.model_mock.fit_generator.assert_called_with(self.train_flow_mock,
                                                         steps_per_epoch=len(self.train_dataset) // run.batch_size,
                                                         epochs=run.epochs,
                                                         callbacks=run.default_callbacks + run.callbacks,
                                                         validation_data=self.test_flow_mock,
                                                         validation_steps=len(self.test_dataset) // run.batch_size,
                                                         class_weight=run.class_weights)

    def test_train_set_model_filename_to_model_checkpoint_path(self):
        sgd_settings = SGDSettings()

        run = Run(model_filename="model1.hdf", datetime=datetime.now(), run_dir="runs/2019/4/24/13:54:00", epochs=100,
                  optimizer_settings=sgd_settings,
                  loss_function="categorical_crossentropy", metrics=["accuracy"], train_dataset=self.train_dataset,
                  test_dataset=self.test_dataset)
        run.model = self.model_mock
        self.assertEqual("model1.hdf", run.model_filename)
        run.train()
        self.assertEqual(r"runs/2019/4/24/13:54:00/model.hdf5", run.model_filename)

    def test_evaluate_calls_predict_generator_for_each_evaluation_set(self):
        sgd_settings = SGDSettings()

        evaluation_dataset1 = TestUtils.example_training_set(r"test_data/project/datasets/training_set")
        evaluation1_flow_mock = MagicMock()
        evaluation_dataset1.flow = MagicMock(return_value=evaluation1_flow_mock)

        evaluation_dataset2 = TestUtils.example_training_set(r"test_data/project/datasets/training_set")
        evaluation2_flow_mock = MagicMock()
        evaluation_dataset2.flow = MagicMock(return_value=evaluation2_flow_mock)

        evaluation_dataset3 = TestUtils.example_training_set(r"test_data/project/datasets/training_set")
        evaluation3_flow_mock = MagicMock()
        evaluation_dataset3.flow = MagicMock(return_value=evaluation3_flow_mock)

        evaluation_datasets = [evaluation_dataset1, evaluation_dataset2, evaluation_dataset3]

        run = Run(model_filename="", datetime=datetime.now(), run_dir="", epochs=100,
                  optimizer_settings=sgd_settings,
                  loss_function="categorical_crossentropy", metrics=["accuracy"], train_dataset=self.train_dataset,
                  test_dataset=self.test_dataset, evaluation_datasets=evaluation_datasets)
        run.model = self.model_mock
        run.evaluate()

        self.model_mock.predict_generator.assert_has_calls([call(evaluation1_flow_mock, verbose=1,
                                                                 steps=len(evaluation_dataset1) // run.batch_size),
                                                            call(evaluation2_flow_mock, verbose=1,
                                                                 steps=len(evaluation_dataset2) // run.batch_size),
                                                            call(evaluation3_flow_mock, verbose=1,
                                                                 steps=len(evaluation_dataset3) // run.batch_size)])

    def test_evaluate_calls_evaluate_on_metric_callbacks(self):
        metric_callback1 = MagicMock()
        metric_callback2 = MagicMock()
        metric_callback3 = MagicMock()
        sgd_settings = SGDSettings()

        self.model_mock.predict_generator = MagicMock(side_effect=[[1, 1, 1], [0, 1, 0], [1, 0, 0]] * 3)

        evaluation_dataset1 = TestUtils.example_data_set([1, 2, 3], [1, 0, 1], "evaluation_dataset1",
                                                         r"test_data/project/datasets/training_set")
        evaluation1_flow_mock = MagicMock()
        evaluation_dataset1.flow = MagicMock(return_value=evaluation1_flow_mock)

        evaluation_dataset2 = TestUtils.example_data_set([4, 5, 6], [0, 1, 0], "evaluation_dataset2",
                                                         r"test_data/project/datasets/training_set")
        evaluation2_flow_mock = MagicMock()
        evaluation_dataset2.flow = MagicMock(return_value=evaluation2_flow_mock)

        evaluation_dataset3 = TestUtils.example_data_set([7, 8, 9], [0, 1, 1], "evaluation_dataset3",
                                                         r"test_data/project/datasets/training_set")
        evaluation3_flow_mock = MagicMock()
        evaluation_dataset3.flow = MagicMock(return_value=evaluation3_flow_mock)

        evaluation_datasets = [evaluation_dataset1, evaluation_dataset2, evaluation_dataset3]

        run = Run(model_filename="", datetime=datetime.now(),
                  run_dir=r"test_data/project/model1/runs/2019/4/17/14:39:00", epochs=100,
                  metric_callbacks=[metric_callback1, metric_callback2, metric_callback3],
                  optimizer_settings=sgd_settings,
                  loss_function="categorical_crossentropy", metrics=["accuracy"], train_dataset=self.train_dataset,
                  test_dataset=self.test_dataset, evaluation_datasets=evaluation_datasets)
        run.model = self.model_mock
        run.evaluate()

        metric_callback1.evaluate.assert_has_calls(
            [call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset1, [1, 1, 1]),
             call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset2, [0, 1, 0]),
             call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset3, [1, 0, 0])])

        metric_callback2.evaluate.assert_has_calls(
            [call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset1, [1, 1, 1]),
             call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset2, [0, 1, 0]),
             call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset3, [1, 0, 0])])

        metric_callback3.evaluate.assert_has_calls(
            [call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset1, [1, 1, 1]),
             call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset2, [0, 1, 0]),
             call(r"test_data/project/model1/runs/2019/4/17/14:39:00", evaluation_dataset3, [1, 0, 0])])

    def test_context_manager_calls_save_settings_and_clear_session(self):
        sgd_settings = SGDSettings()
        evaluation_dataset1 = TestUtils.example_data_set([1, 2, 3], [1, 0, 1], "evaluation_dataset1",
                                                         r"test_data/project/datasets/training_set")
        with patch.object(Run, "save_settings", return_value=None) as mock_method1:
            with patch.object(Run, "clear_session", return_value=None) as mock_method2:
                with Run(model_filename="", datetime=datetime.now(),
                         run_dir=r"test_data/project/models/model1/runs/2019/5/8/11:30:10", optimizer_settings=sgd_settings,
                         train_dataset=self.train_dataset, test_dataset=self.test_dataset,
                         evaluation_datasets=[evaluation_dataset1]):
                    pass
        self.assertTrue(mock_method1.called)
        self.assertTrue(mock_method2.called)


if __name__ == '__main__':
    unittest.main()
