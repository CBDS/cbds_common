import unittest
from cbds.deeplearning.settings import SGDSettings
import keras.backend as K


class SGDSettingsTests(unittest.TestCase):
    def setUp(self):
        self.sgd_settings = SGDSettings()

    def test_constructor_sets_defaults(self):
        self.assertEqual(0.01, self.sgd_settings.lr)
        self.assertEqual(0.0, self.sgd_settings.momentum)
        self.assertEqual(0.0, self.sgd_settings.decay)
        self.assertFalse(self.sgd_settings.nesterov)

    def test_with_learning_rate_sets_learning_rate(self):
        self.assertEqual(0.1, self.sgd_settings.with_lr(0.1).lr)
        self.assertEqual(0.01, self.sgd_settings.with_lr(0.01).lr)
        self.assertEqual(0.001, self.sgd_settings.with_lr(0.001).lr)
        self.assertEqual(0.0001, self.sgd_settings.with_lr(0.0001).lr)
        self.assertEqual(0.00001, self.sgd_settings.with_lr(0.00001).lr)

    def test_with_momentum_sets_momentum(self):
        self.assertEqual(0.1, self.sgd_settings.with_momentum(0.1).momentum)
        self.assertEqual(0.2, self.sgd_settings.with_momentum(0.2).momentum)
        self.assertEqual(0.3, self.sgd_settings.with_momentum(0.3).momentum)
        self.assertEqual(0.4, self.sgd_settings.with_momentum(0.4).momentum)
        self.assertEqual(0.5, self.sgd_settings.with_momentum(0.5).momentum)

    def test_with_decay_sets_decay(self):
        self.assertEqual(0.01, self.sgd_settings.with_decay(0.01).decay)
        self.assertEqual(0.02, self.sgd_settings.with_decay(0.02).decay)
        self.assertEqual(0.03, self.sgd_settings.with_decay(0.03).decay)
        self.assertEqual(0.04, self.sgd_settings.with_decay(0.04).decay)
        self.assertEqual(0.05, self.sgd_settings.with_decay(0.05).decay)

    def test_with_nesterov_sets_nesterov(self):
        self.assertTrue(self.sgd_settings.with_nesterov(True).nesterov)
        self.assertFalse(self.sgd_settings.with_nesterov(False).nesterov)

    def test_optimizer_returns_sgd_optimizer_with_settings(self):
        sgd_settings1 = SGDSettings(lr=0.01, momentum=0.7, decay=0, nesterov=False)
        self.assertAlmostEqual(0.01, K.eval(sgd_settings1.optimizer.lr))
        self.assertAlmostEqual(0.7, K.eval(sgd_settings1.optimizer.momentum))
        self.assertAlmostEqual(0, K.eval(sgd_settings1.optimizer.decay))
        self.assertEqual(False, sgd_settings1.optimizer.nesterov)

        sgd_settings1 = SGDSettings(lr=0.001, momentum=0.8, decay=1e-5, nesterov=False)
        self.assertAlmostEqual(0.001, K.eval(sgd_settings1.optimizer.lr))
        self.assertAlmostEqual(0.8, K.eval(sgd_settings1.optimizer.momentum))
        self.assertAlmostEqual(1e-5, K.eval(sgd_settings1.optimizer.decay))
        self.assertEqual(False, sgd_settings1.optimizer.nesterov)

        sgd_settings1 = SGDSettings(lr=0.0001, momentum=0.9, decay=1e-6, nesterov=True)
        self.assertAlmostEqual(0.0001, K.eval(sgd_settings1.optimizer.lr))
        self.assertAlmostEqual(0.9, K.eval(sgd_settings1.optimizer.momentum))
        self.assertAlmostEqual(1e-6, K.eval(sgd_settings1.optimizer.decay))
        self.assertEqual(True, sgd_settings1.optimizer.nesterov)

    def test_to_dict_returns_dict_with_settings(self):
        self.assertEqual({
            "lr": 0.001,
            "momentum": 0.9,
            "decay": 0.00005,
            "nesterov": True
        }, SGDSettings(lr=0.001, momentum=0.9, decay=0.00005, nesterov=True).to_dict())

    def test_from_dict_returns_sgd_settings_with_values_from_dict(self):
        sgd_settings = SGDSettings.from_dict({
            "lr": 0.0001,
            "momentum": 0.8,
            "decay": 0.000005,
            "nesterov": False
        })
        self.assertEqual(0.0001, sgd_settings.lr)
        self.assertEqual(0.8, sgd_settings.momentum)
        self.assertEqual(0.000005, sgd_settings.decay)
        self.assertFalse(sgd_settings.nesterov)


if __name__ == '__main__':
    unittest.main()
