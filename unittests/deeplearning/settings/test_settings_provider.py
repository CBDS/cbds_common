import unittest
from unittest.mock import patch, call
from cbds.deeplearning.settings import SettingsProvider


class SettingsProviderTests(unittest.TestCase):
    def test_settings_filename_set_correctly(self):
        self.assertEqual("project.json", SettingsProvider("test_data", "project.json").settings_filename)
        self.assertEqual("model.json", SettingsProvider("test_data", "model.json").settings_filename)
        self.assertEqual("dataset.json", SettingsProvider("test_data", "dataset.json").settings_filename)
        self.assertEqual("run.json", SettingsProvider("test_data", "run.json").settings_filename)

    def test_settings_dir_set_correctly(self):
        self.assertEqual(r"test_data/project",
                         SettingsProvider(r"test_data/project", "project.json").settings_dir)
        self.assertEqual(r"test_data/project/models/model1",
                         SettingsProvider(r"test_data/project/models/model1","model.json").settings_dir)
        self.assertEqual(r"test_data/project/datasets/dataset1",
                         SettingsProvider(r"test_data/project/datasets/dataset1","dataset.json").settings_dir)
        self.assertEqual(r"test_data/project/models/model1/runs/2019/4/18/17:26:00/",
                         SettingsProvider(r"test_data/project/models/model1/runs/2019/4/18/17:26:00/",
                                          "run.json").settings_dir)

    def test_to_dict_returns_dict_with_settings(self):
        self.assertEqual({
                                  "settings_dir": r"test_data/project",
                                  "settings_filename": "project.json"
                         },
                         SettingsProvider(settings_dir=r"test_data/project",
                                          settings_filename="project.json").to_dict())

        self.assertEqual({
                                "settings_dir": r"test_data/project/models/model1",
                                "settings_filename": "model.json"
                         },
                         SettingsProvider(settings_dir=r"test_data/project/models/model1",
                                          settings_filename="model.json").to_dict())

    def test_from_dict_sets_settings_correctly(self):
        settings_provider = SettingsProvider("", "")
        settings_provider.from_dict({
                        "settings_dir": r"test_data/project/models/model2",
                        "settings_filename": "model.json"
                    })
        self.assertEqual(r"test_data/project/models/model2", settings_provider.settings_dir)
        self.assertEqual("model.json", settings_provider.settings_filename)

        settings_provider2 = SettingsProvider("", "")
        settings_provider2.from_dict({
                        "settings_dir": r"test_data/project/datasets/dataset1",
                        "settings_filename": "dataset.json"
                    })
        self.assertEqual(r"test_data/project/datasets/dataset1", settings_provider2.settings_dir)
        self.assertEqual("dataset.json", settings_provider2.settings_filename)


if __name__ == '__main__':
    unittest.main()
