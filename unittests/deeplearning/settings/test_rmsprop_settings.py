import unittest
from cbds.deeplearning.settings import RMSPropSettings
import keras.backend as K


class RMSPropSettingsTest(unittest.TestCase):
    def test_lr_initialized_by_contructor(self):
        self.assertEqual(0.1, RMSPropSettings(lr=0.1).lr)
        self.assertEqual(0.01, RMSPropSettings(lr=0.01).lr)
        self.assertEqual(0.001, RMSPropSettings(lr=0.001).lr)
        self.assertEqual(0.0001, RMSPropSettings(lr=0.0001).lr)
        self.assertEqual(0.00001, RMSPropSettings(lr=0.00001).lr)

    def test_with_lr_sets_lr_correctly(self):
        self.assertEqual(0.1, RMSPropSettings().with_lr(lr=0.1).lr)
        self.assertEqual(0.01, RMSPropSettings().with_lr(lr=0.01).lr)
        self.assertEqual(0.001, RMSPropSettings().with_lr(lr=0.001).lr)
        self.assertEqual(0.0001, RMSPropSettings().with_lr(lr=0.0001).lr)
        self.assertEqual(0.00001, RMSPropSettings().with_lr(lr=0.00001).lr)

    def test_rho_initialized_by_constructor(self):
        self.assertEqual(0.1, RMSPropSettings(rho=0.1).rho)
        self.assertEqual(0.2, RMSPropSettings(rho=0.2).rho)
        self.assertEqual(0.3, RMSPropSettings(rho=0.3).rho)
        self.assertEqual(0.4, RMSPropSettings(rho=0.4).rho)
        self.assertEqual(0.5, RMSPropSettings(rho=0.5).rho)

    def test_with_rho_sets_rho_correctly(self):
        self.assertEqual(0.1, RMSPropSettings().with_rho(rho=0.1).rho)
        self.assertEqual(0.2, RMSPropSettings().with_rho(rho=0.2).rho)
        self.assertEqual(0.3, RMSPropSettings().with_rho(rho=0.3).rho)
        self.assertEqual(0.4, RMSPropSettings().with_rho(rho=0.4).rho)
        self.assertEqual(0.5, RMSPropSettings().with_rho(rho=0.5).rho)

    def test_epsilon_initialized_by_constructor(self):
        self.assertEqual(1e-3, RMSPropSettings(epsilon=1e-3).epsilon)
        self.assertEqual(1e-4, RMSPropSettings(epsilon=1e-4).epsilon)
        self.assertEqual(1e-5, RMSPropSettings(epsilon=1e-5).epsilon)
        self.assertEqual(1e-6, RMSPropSettings(epsilon=1e-6).epsilon)
        self.assertEqual(1e-7, RMSPropSettings(epsilon=1e-7).epsilon)

    def test_with_epsilon_sets_epsilon(self):
        self.assertEqual(1e-3, RMSPropSettings().with_epsilon(epsilon=1e-3).epsilon)
        self.assertEqual(1e-4, RMSPropSettings().with_epsilon(epsilon=1e-4).epsilon)
        self.assertEqual(1e-5, RMSPropSettings().with_epsilon(epsilon=1e-5).epsilon)
        self.assertEqual(1e-6, RMSPropSettings().with_epsilon(epsilon=1e-6).epsilon)
        self.assertEqual(1e-7, RMSPropSettings().with_epsilon(epsilon=1e-7).epsilon)

    def test_decay_initialized_by_constructor(self):
        self.assertEqual(0.01, RMSPropSettings(decay=0.01).decay)
        self.assertEqual(0.02, RMSPropSettings(decay=0.02).decay)
        self.assertEqual(0.03, RMSPropSettings(decay=0.03).decay)
        self.assertEqual(0.04, RMSPropSettings(decay=0.04).decay)
        self.assertEqual(0.05, RMSPropSettings(decay=0.05).decay)

    def test_with_decay_sets_decay(self):
        self.assertEqual(0.01, RMSPropSettings().with_decay(decay=0.01).decay)
        self.assertEqual(0.02, RMSPropSettings().with_decay(decay=0.02).decay)
        self.assertEqual(0.03, RMSPropSettings().with_decay(decay=0.03).decay)
        self.assertEqual(0.04, RMSPropSettings().with_decay(decay=0.04).decay)
        self.assertEqual(0.05, RMSPropSettings().with_decay(decay=0.05).decay)

    def test_optimizer_returns_rms_optimizer_with_settings(self):
        rms_prop_settings1 = RMSPropSettings(lr=0.001, rho=0.9, epsilon=None, decay=0)
        self.assertAlmostEqual(0.001, K.eval(rms_prop_settings1.optimizer.lr))
        self.assertAlmostEqual(0.9, K.eval(rms_prop_settings1.optimizer.rho))
        # apparently None sets epsilon to default of 1e-7
        self.assertAlmostEqual(1e-7, rms_prop_settings1.optimizer.epsilon)
        self.assertAlmostEqual(0, K.eval(rms_prop_settings1.optimizer.decay))

        rms_prop_settings2 = RMSPropSettings(lr=0.01, rho=0.8, epsilon=1e-3, decay=1e-4)
        self.assertAlmostEqual(0.01, K.eval(rms_prop_settings2.optimizer.lr))
        self.assertAlmostEqual(0.8, K.eval(rms_prop_settings2.optimizer.rho))
        self.assertAlmostEqual(1e-3, rms_prop_settings2.optimizer.epsilon)
        self.assertAlmostEqual(1e-4, K.eval(rms_prop_settings2.optimizer.decay))

        rms_prop_settings3 = RMSPropSettings(lr=0.1, rho=0.7, epsilon=1e-2, decay=1e-5)
        self.assertAlmostEqual(0.1, K.eval(rms_prop_settings3.optimizer.lr))
        self.assertAlmostEqual(0.7, K.eval(rms_prop_settings3.optimizer.rho))
        self.assertAlmostEqual(1e-2, rms_prop_settings3.optimizer.epsilon)
        self.assertAlmostEqual(1e-5, K.eval(rms_prop_settings3.optimizer.decay))

    def test_to_dict_returns_dict_with_settings(self):
        self.assertDictEqual({
            "lr": 0.01,
            "rho": 0.8,
            "epsilon": 1e-3,
            "decay": 1e-4
        }, RMSPropSettings(lr=0.01, rho=0.8, epsilon=1e-3, decay=1e-4).to_dict())

    def test_from_dict_returns_rms_prop_settings_with_values_from_dict(self):
        rms_prop_settings = RMSPropSettings.from_dict({
            "lr": 0.01,
            "rho": 0.8,
            "epsilon": 1e-3,
            "decay": 1e-4
        })
        self.assertEqual(0.01, rms_prop_settings.lr)
        self.assertEqual(0.8, rms_prop_settings.rho)
        self.assertEqual(1e-3, rms_prop_settings.epsilon)
        self.assertEqual(1e-4, rms_prop_settings.decay)

        rms_prop_settings = RMSPropSettings.from_dict({
            "lr": 0.001,
            "rho": 0.7,
            "epsilon": None,
            "decay": 1e-5
        })
        self.assertEqual(0.001, rms_prop_settings.lr)
        self.assertEqual(0.7, rms_prop_settings.rho)
        self.assertIsNone(rms_prop_settings.epsilon)
        self.assertEqual(1e-5, rms_prop_settings.decay)


if __name__ == '__main__':
    unittest.main()
