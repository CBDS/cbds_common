import unittest
from cbds.deeplearning.settings import OptimizerSettings


class OptimizerSettingsTests(unittest.TestCase):
    def setUp(self):
        self.optimizer_settings = OptimizerSettings()

    def test_optimizer_dict_returned_correctly(self):
        self.maxDiff = None
        self.assertDictEqual(self.optimizer_settings.optimizer_dict,
                             {type(optimizer).__name__: optimizer for optimizer in self.optimizer_settings.optimizers})

    def test_optimizer_for_returns_optimizer_of_correct_type(self):
        optimizer_names = [type(optimizer).__name__ for optimizer in self.optimizer_settings.optimizers]
        for i, optimizer_name in enumerate(optimizer_names):
            self.assertIsInstance(self.optimizer_settings.optimizer_for(optimizer_name),
                                  type(self.optimizer_settings.optimizers[i]))


if __name__ == '__main__':
    unittest.main()
