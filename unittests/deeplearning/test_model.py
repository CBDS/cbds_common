import unittest
from unittest.mock import MagicMock
from unittests.testutils import TestUtils
from cbds.deeplearning import Model
from cbds.deeplearning import Run
from keras.models import load_model
from datetime import datetime, timedelta
import os
import keras
import shutil


class ModelTests(unittest.TestCase):
    def setUp(self):
        TestUtils.rm_file_if_exists(r"test_data/project/models/model1/model1.hdf5")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model1")
        os.makedirs(r"test_data/project/models/model1")

    def tearDown(self):
        TestUtils.rm_file_if_exists(r"test_data/project/models/model1/model1.hdf5")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model1")

    def test_model_name_set_correctly(self):
        self.assertEqual("model1",
                         Model(name="model1", model_dir=r"test_data/project/models/model1").name)
        self.assertEqual("model2",
                         Model(name="model2", model_dir=r"test_data/project/models/model2").name)
        self.assertEqual("model3",
                         Model(name="model3", model_dir=r"test_data/project/models/model3").name)
        self.assertEqual("model4",
                         Model(name="model4", model_dir=r"test_data/project/models/model4").name)
        self.assertEqual("model5",
                         Model(name="model5", model_dir=r"test_data/project/models/model5").name)

    def test_model_dir_set_correctly(self):
        self.assertEqual(r"test_data/project/models/model1",
                         Model(name="model1", model_dir=r"test_data/project/models/model1").model_dir)
        self.assertEqual(r"test_data/project/models/model2",
                         Model(name="model2", model_dir=r"test_data/project/models/model2").model_dir)
        self.assertEqual(r"test_data/project/models/model3",
                         Model(name="model3", model_dir=r"test_data/project/models/model3").model_dir)
        self.assertEqual(r"test_data/project/models/model4",
                         Model(name="model4", model_dir=r"test_data/project/models/model4").model_dir)
        self.assertEqual(r"test_data/project/models/model5",
                         Model(name="model5", model_dir=r"test_data/project/models/model5").model_dir)

    def test_import_model_copies_model_to_model_dir(self):
        self.assertFalse(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))
        Model(name="model1", model_dir=r"test_data/project/models/model1").import_model(r"test_data/model1.hdf5")
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))

    def test_import_model_sets_model_filename(self):
        self.assertFalse(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))
        model = Model(name="model1", model_dir=r"test_data/project/models/model1")
        model.import_model(r"test_data/model1.hdf5")
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))
        self.assertEqual(r"test_data/project/models/model1/model1.hdf5", model.model_filename)

    def test_model_loads_model_from_model_dir(self):
        model = Model(name="model1", model_dir=r"test_data/project/models/model1")
        model.import_model(r"test_data/model1.hdf5")

        right_model = load_model(r"test_data/project/models/model1/model1.hdf5")
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))
        self.assertIsInstance(model.model, keras.models.Sequential)
        TestUtils.compare_keras_models(model.model, right_model)

    def test_create_model_creates_model_file(self):
        self.assertFalse(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))
        self.assertFalse(os.path.exists(r"test_data/project/models/model1/model2.hdf5"))
        self.assertFalse(os.path.exists(r"test_data/project/models/model1/model3.hdf5"))

        Model(name="model1", model_dir=r"test_data/project/models/model1")\
            .create_model(TestUtils.create_example_model())
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))

        Model(name="model2", model_dir=r"test_data/project/models/model1")\
            .create_model(TestUtils.create_example_model())
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/model2.hdf5"))

        Model(name="model3", model_dir=r"test_data/project/models/model1")\
            .create_model(TestUtils.create_example_model())
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/model3.hdf5"))

    def test_to_dict_returns_dictionary_with_model_settings(self):
        self.assertFalse(os.path.exists(r"test_data/project/models/model1/model1.hdf5"))
        model1 = Model(name="model1", model_dir=r"test_data/project/models/model1")
        model1.create_model(TestUtils.create_example_model())

        self.assertDictEqual({
            "name": "model1",
            "model_dir": r"test_data/project/models/model1",
            "model_filename": r"test_data/project/models/model1/model1.hdf5",
            "SettingsProvider": {
                "settings_dir": r"test_data/project/models/model1",
                "settings_filename": "model.json"
            }
        }, model1.to_dict())

    def test_from_dict_returns_model_with_settings_from_dictionary(self):
        model = Model.instance_from_dict({
            "name": "model2",
            "model_dir": r"test_data/project/models/model2",
            "model_filename": r"test_data/project/models/model2/model2.hdf5",
            "SettingsProvider": {
                "settings_dir": r"test_data/project/models/model2",
                "settings_filename": "model.json"
            }
        })
        self.assertEqual("model2", model.name)
        self.assertEqual(r"test_data/project/models/model2", model.model_dir)
        self.assertEqual(r"test_data/project/models/model2/model2.hdf5", model.model_filename)
        self.assertEqual(r"test_data/project/models/model2", model.settings_dir)
        self.assertEqual("model.json", model.settings_filename)

    def test_settings_filename_set_correctly(self):
        self.assertEqual("model.json",
                         Model(name="model1", model_dir=r"test_data/project/models/model1").settings_filename)
        self.assertEqual("model.json",
                         Model(name="model2", model_dir=r"test_data/project/models/model2").settings_filename)
        self.assertEqual("model.json",
                         Model(name="model3", model_dir=r"test_data/project/models/model3").settings_filename)

    def test_settings_dir_set_correctly(self):
        self.assertEqual(r"test_data/project/models/model1",
                         Model(name="model1", model_dir=r"test_data/project/models/model1").settings_dir)
        self.assertEqual(r"test_data/project/models/model2",
                         Model(name="model2", model_dir=r"test_data/project/models/model2").settings_dir)
        self.assertEqual(r"test_data/project/models/model3",
                         Model(name="model3", model_dir=r"test_data/project/models/model3").settings_dir)

    def test_load_loads_model_settings_correctly(self):
        model = Model.load(r"test_data/model.json")
        self.assertEqual("model1", model.name)
        self.assertEqual(r"test_data/project/models/model1", model.model_dir)
        self.assertEqual(r"test_data/project/models/model1/model1.hdf5", model.model_filename)
        self.assertEqual(r"test_data/project/models/model1", model.settings_dir)
        self.assertEqual("model.json", model.settings_filename)

    def test_save_saves_model_settings(self):
        model = Model("model1", model_dir=r"test_data/project/models/model1")
        model.import_model(r"test_data/model1.hdf5")
        model.save(r"test_data/project/models/model1/model.json")
        saved_model = Model.load(r"test_data/project/models/model1/model.json")
        self.assertEqual(model.name, saved_model.name)
        self.assertEqual(model.model_dir, saved_model.model_dir)
        self.assertEqual(model.model_filename, saved_model.model_filename)
        self.assertEqual(model.settings_dir, saved_model.settings_dir)
        self.assertEqual(model.settings_filename, saved_model.settings_filename)

    def test_settings_loaded_automatically_if_available(self):
        model = Model(name="", model_dir=r"test_data/")
        self.assertEqual("model1", model.name)
        self.assertEqual("test_data/project/models/model1", model.model_dir)
        self.assertEqual("test_data/project/models/model1", model.settings_dir)
        self.assertEqual("test_data/project/models/model1/model1.hdf5", model.model_filename)

    def test_model_run_added_to_opened_runs(self):
        model = Model(name="", model_dir=r"test_data/project/models/model1")
        model.import_model(r"test_data/model1.hdf5")
        run1 = model.run(datetime(2017, 7, 1, 11, 00))
        self.assertEqual(run1.datetime, model.opened_runs[datetime(2017, 7, 1, 11, 00)].datetime)

        run2 = model.run(datetime(2018, 9, 5, 12, 10))
        self.assertEqual(run2.datetime, model.opened_runs[datetime(2018, 9, 5, 12, 10)].datetime)

        run3 = model.run(datetime(2019, 11, 12, 1, 15))
        self.assertEqual(run3.datetime, model.opened_runs[datetime(2019, 11, 12, 1, 15)].datetime)

    def test_model_run_returns_same_run_for_same_datetime(self):
        model = Model(name="", model_dir=r"test_data/project/models/model1")
        model.import_model(r"test_data/model1.hdf5")
        run1 = model.run(datetime(2017, 7, 1, 11, 00))
        run2 = model.run(datetime(2018, 9, 5, 12, 10))
        run3 = model.run(datetime(2019, 11, 12, 1, 15))
        self.assertIs(run1, model.run(datetime(2017, 7, 1, 11, 00)))
        self.assertIs(run2, model.run(datetime(2018, 9, 5, 12, 10)))
        self.assertIs(run3, model.run(datetime(2019, 11, 12, 1, 15)))

    def test_save_methods_saves_opened_runs_to_disk(self):
        model = Model(name="", model_dir=r"test_data/project/models/model1")
        model.import_model(r"test_data/model1.hdf5")

        run1 = MagicMock()
        run2 = MagicMock()
        run3 = MagicMock()
        model.opened_runs = {
            datetime(2017, 7, 1, 11, 00): run1,
            datetime(2018, 9, 5, 12, 10): run2,
            datetime(2019, 11, 12, 1, 15): run3
        }

        model.save(r"test_data/project/models/model1/model.json")

        self.assertTrue(run1.save_settings.called)
        self.assertTrue(run2.save_settings.called)
        self.assertTrue(run3.save_settings.called)

    def test_runs_returns_all_runs_for_model(self):
        model = Model(name="", model_dir=r"test_data/project/models/model1")
        model.import_model(r"test_data/model1.hdf5")

        model.run(datetime(2017, 10, 11, 12))
        model.run(datetime(2017, 10, 12, 12))
        model.run(datetime(2018, 1, 1, 13, 1))
        model.run(datetime(2018, 2, 1, 13, 1))
        model.run(datetime(2019, 5, 7, 1, 15))
        self.assertEqual([datetime(2017, 10, 11, 12), datetime(2017, 10, 12, 12), datetime(2018, 1, 1, 13, 1),
                          datetime(2018, 2, 1, 13, 1), datetime(2019, 5, 7, 1, 15)], model.runs)

    def test_decay_settings_for_returns_decay_settings_directly(self):
        self.assertEqual(1e-1, Model(name="", model_dir=r"").decay_settings_for({"decay": 1e-1}))
        self.assertEqual(1e-2, Model(name="", model_dir=r"").decay_settings_for({"decay": 1e-2}))
        self.assertEqual(1e-3, Model(name="", model_dir=r"").decay_settings_for({"decay": 1e-3}))
        self.assertEqual(1e-4, Model(name="", model_dir=r"").decay_settings_for({"decay": 1e-4}))
        self.assertEqual(1e-5, Model(name="", model_dir=r"").decay_settings_for({"decay": 1e-5}))

    def test_decay_settings_for_calculates_decay_settings(self):
        self.assertEqual(1e-1 / 10, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-1, "epochs": 10,
                                                                                      "calculate_decay": True}))
        self.assertEqual(1e-1 / 20, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-1, "epochs": 20,
                                                                                      "calculate_decay": True}))

        self.assertEqual(1e-2 / 30, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-2, "epochs": 30,
                                                                                      "calculate_decay": True}))

        self.assertEqual(1e-3 / 40, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-3, "epochs": 40,
                                                                                      "calculate_decay": True}))
        self.assertEqual(1e-4 / 50, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-4, "epochs": 50,
                                                                                      "calculate_decay": True}))

    def test_decay_settings_for_ignores_decay_settings_when_calculate_decay_true(self):
        self.assertEqual(1e-1 / 10, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-1, "epochs": 10,
                                                                                      "calculate_decay": True,
                                                                                      "decay": 1e-1}))
        self.assertEqual(1e-1 / 20, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-1, "epochs": 20,
                                                                                      "calculate_decay": True,
                                                                                      "decay": 1e-2}))

        self.assertEqual(1e-2 / 30, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-2, "epochs": 30,
                                                                                      "calculate_decay": True,
                                                                                      "decay": 1e-3}))

        self.assertEqual(1e-3 / 40, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-3, "epochs": 40,
                                                                                      "calculate_decay": True,
                                                                                      "decay": 1e-4}))
        self.assertEqual(1e-4 / 50, Model(name="", model_dir=r"").decay_settings_for({"lr": 1e-4, "epochs": 50,
                                                                                      "calculate_decay": True,
                                                                                      "decay": 1e-5}))



class ModelRunTests(unittest.TestCase):
    def setUp(self):
        # some boiler plate to make run tests work
        self.remove_files_and_dirs()
        os.makedirs(r"test_data/project/models/model1")
        shutil.copy(r"test_data/model1.hdf5", r"test_data/project/models/model1")
        os.makedirs(r"test_data/project/models/model2")
        shutil.copy(r"test_data/model2.hdf5", r"test_data/project/models/model2")
        os.makedirs(r"test_data/project/models/model3")
        shutil.copy(r"test_data/model3.hdf5", r"test_data/project/models/model3")
        os.makedirs(r"test_data/project/models/model4")
        shutil.copy(r"test_data/model4.hdf5", r"test_data/project/models/model4")
        os.makedirs(r"test_data/project/models/model5")
        shutil.copy(r"test_data/model5.hdf5", r"test_data/project/models/model5")

    @staticmethod
    def remove_files_and_dirs():
        TestUtils.rm_file_if_exists(r"test_data/project/models/model1/model1.hdf5")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model1")
        TestUtils.rm_file_if_exists(r"test_data/project/models/model1/model2.hdf5")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model2")
        TestUtils.rm_file_if_exists(r"test_data/project/models/model1/model3.hdf5")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model3")
        TestUtils.rm_file_if_exists(r"test_data/project/models/model1/model4.hdf5")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model4")
        TestUtils.rm_file_if_exists(r"test_data/project/models/model1/model5.hdf5")
        TestUtils.rm_dir_if_exists(r"test_data/project/models/model5")

    def test_run_creates_run_directory(self):
        self.assertFalse(os.path.exists(r"test_data/project/models/model1/runs/2019/4/4/14:10:10"))
        Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                   datetime(2019, 4, 4, 14, 10, 10))
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2019/4/4/14:10:10"))

        self.assertFalse(os.path.exists(r"test_data/project/models/model1/runs/2018/12/11/15:35:25"))
        Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                   datetime(2018, 12, 11, 15, 35, 25))
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2018/12/11/15:35:25"))

        self.assertFalse(os.path.exists(r"test_data/project/models/model1/runs/2017/11/4/00:00:01"))
        Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                   datetime(2017, 11, 4, 0, 0, 1))
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2017/11/4/00:00:01"))

    def test_run_does_not_create_run_directory_if_exists(self):
        os.makedirs(r"test_data/project/models/model1/runs/2019/4/4/14:10:10")
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2019/4/4/14:10:10"))
        Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                   datetime(2019, 4, 4, 14, 10, 10))
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2019/4/4/14:10:10"))

        os.makedirs(r"test_data/project/models/model1/runs/2018/12/11/15:35:25")
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2018/12/11/15:35:25"))
        Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                   datetime(2018, 12, 11, 15, 35, 25))
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2018/12/11/15:35:25"))

        os.makedirs(r"test_data/project/models/model1/runs/2017/11/4/00:00:01")
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2017/11/4/00:00:01"))
        Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                   datetime(2017, 11, 4, 0, 0, 1))
        self.assertTrue(os.path.exists(r"test_data/project/models/model1/runs/2017/11/4/00:00:01"))

    def test_run_returns_run_object(self):
        self.assertIsInstance(Model(name="model1", model_dir=r"test_data/project/models/model1").run(), Run)
        self.assertIsInstance(Model(name="model2", model_dir=r"test_data/project/models/model2").run(), Run)
        self.assertIsInstance(Model(name="model3", model_dir=r"test_data/project/models/model3").run(), Run)
        self.assertIsInstance(Model(name="model4", model_dir=r"test_data/project/models/model4").run(), Run)
        self.assertIsInstance(Model(name="model5", model_dir=r"test_data/project/models/model5").run(), Run)

    def test_run_sets_model_for_run(self):
        model1 = Model(name="model1", model_dir=r"test_data/project/models/model1")
        self.assertEqual(model1.model_filename, model1.run().model_filename)

        model2 = Model(name="model2", model_dir=r"test_data/project/models/model2")
        self.assertEqual(model2.model_filename, model2.run().model_filename)

        model3 = Model(name="model3", model_dir=r"test_data/project/models/model3")
        self.assertEqual(model3.model_filename, model3.run().model_filename)

    def test_run_sets_date_for_run_object(self):
        run1 = Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                  datetime(2019, 4, 4, 14, 10, 10))
        self.assertEqual(datetime(2019, 4, 4, 14, 10, 10), run1.datetime)

        run2 = Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                  datetime(2018, 12, 11, 15, 35, 25))
        self.assertEqual(datetime(2018, 12, 11, 15, 35, 25), run2.datetime)

        run3 = Model(name="model1", model_dir=r"test_data/project/models/model1").run(date=
                                                                                  datetime(2017, 11, 4, 0, 0, 1))
        self.assertEqual(datetime(2017, 11, 4, 0, 0, 1), run3.datetime)

    def test_run_without_parameters_sets_current_date(self):
        Model(name="model1", model_dir=r"test_data/project/models/model1").run()
        self.assertEqual(datetime.now().replace(microsecond=0),
                         Model(name="model1", model_dir=r"test_data/project/models/model1").run().datetime
                         .replace(microsecond=0))
        self.assertEqual(datetime.now().replace(microsecond=0),
                         Model(name="model1", model_dir=r"test_data/project/models/model1").run().datetime
                         .replace(microsecond=0))
        self.assertEqual(datetime.now().replace(microsecond=0),
                         Model(name="model1", model_dir=r"test_data/project/models/model1").run().datetime
                         .replace(microsecond=0))
        self.assertEqual(datetime.now().replace(microsecond=0),
                         Model(name="model1", model_dir=r"test_data/project/models/model1").run().datetime
                         .replace(microsecond=0))
        self.assertEqual(datetime.now().replace(microsecond=0),
                         Model(name="model1", model_dir=r"test_data/project/models/model1").run().datetime
                         .replace(microsecond=0))

    def test_new_run_sets_run_dir_for_run_object(self):
        run1 = Model(name="model1", model_dir=r"test_data/project/models/model1")\
            .run(date=datetime(2019, 4, 4, 14, 10, 10))
        self.assertEqual(r"test_data/project/models/model1/runs/2019/4/4/14:10:10", run1.run_dir)

        run2 = Model(name="model2", model_dir=r"test_data/project/models/model2")\
            .run(date=datetime(2018, 12, 11, 15, 35, 25))
        self.assertEqual(r"test_data/project/models/model2/runs/2018/12/11/15:35:25", run2.run_dir)

        run3 = Model(name="model3", model_dir=r"test_data/project/models/model3")\
            .run(date=datetime(2017, 11, 4, 0, 0, 1))
        self.assertEqual(r"test_data/project/models/model3/runs/2017/11/4/00:00:01", run3.run_dir)

    def test_current_run_returns_current_run_for_model(self):
        model = Model(name="model1", model_dir=r"test_data/project/models/model1")
        run1 = model.run(date=datetime(2019, 4, 4, 14, 10, 10))
        self.assertIs(run1, model.current_run)

        run2 = model.run(date=datetime(2018, 12, 11, 15, 35, 25))
        self.assertIs(run2, model.current_run)

        run3 = model.run(date=datetime(2017, 11, 4, 0, 0, 1))
        self.assertIs(run3, model.current_run)


if __name__ == '__main__':
    unittest.main()
