import unittest
from cbds.deeplearning.metrics.metric_callback import MetricCallback
import numpy as np


class MetricCallbackTests(unittest.TestCase):
    def test_predictions_to_labels_uses_cut_off_to_get_labels(self):
        self.assertTrue(np.array_equal(np.array([0, 1, 0]),
                                       MetricCallback()
                                       .predictions_to_labels(np.array([0.1, 0.7, 0.5]), 0.6)))

        self.assertTrue(np.array_equal(np.array([0, 1, 1, 1, 1]),
                                       MetricCallback()
                                       .predictions_to_labels(np.array([0.1, 0.7, 0.5, 0.2, 1.0]), 0.2)))

        self.assertTrue(np.array_equal(np.array([0, 0, 1, 1, 1]),
                                       MetricCallback()
                                       .predictions_to_labels(np.array([0.1, 0.25, 0.7, 0.5, 0.3]), 0.3)))

    def test_to_labels_returns_original_label_for_label_vector(self):
        self.assertTrue(np.array_equal(np.array([0, 1, 2]),
                                       MetricCallback().to_labels(np.array([0, 1, 2]))))
        self.assertTrue(np.array_equal(np.array([3, 4, 5]),
                                       MetricCallback().to_labels(np.array([3, 4, 5]))))
        self.assertTrue(np.array_equal(np.array([6, 7]),
                                       MetricCallback().to_labels(np.array([6, 7]))))
        self.assertTrue(np.array_equal(np.array([8, 9, 10, 11]),
                                       MetricCallback().to_labels(np.array([8, 9, 10, 11]))))
        self.assertTrue(np.array_equal(np.array([12, 13, 14, 15]),
                                       MetricCallback().to_labels(np.array([12, 13, 14, 15]))))

    def test_to_labels_returns_original_label_for_one_hot_vector(self):
        self.assertTrue(np.array_equal(np.array([0, 1, 2]),
                                       MetricCallback().to_labels(np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]))))
        self.assertTrue(np.array_equal(np.array([3, 4, 5]),
                                       MetricCallback().to_labels(np.array([[0, 0, 0, 1, 0, 0],
                                                                            [0, 0, 0, 0, 1, 0],
                                                                            [0, 0, 0, 0, 0, 1]]))))
        self.assertTrue(np.array_equal(np.array([6, 7]),
                                       MetricCallback().to_labels(np.array([[0] * 6 + [1] + [0], [0] * 7 + [1]]))))
        self.assertTrue(np.array_equal(np.array([8, 9, 10, 11]),
                                       MetricCallback().to_labels(np.array([[0] * 8 + [1] + 3 * [0],
                                                                            [0] * 9 + [1] + 2 * [0],
                                                                            [0] * 10 + [1] + 1 * [0],
                                                                            [0] * 11 + [1]]))))

    def test_classification_labels_and_predictions_for_returns_labels(self):
        self.assertTrue(np.array_equal(np.array([0, 1, 1]),
                                       MetricCallback()
                                       .classification_labels_and_predictions_for(np.array([0, 1, 1]),
                                                                                  np.array([0.1, 0.3, 0.2]), 0.2)[0]
                                       ))

        self.assertTrue(np.array_equal(np.array([0, 0, 1]),
                                       MetricCallback()
                                       .classification_labels_and_predictions_for(np.array([0, 0, 1, 1, 1]),
                                                                                  np.array([0.1, 0.3, 0.2]), 0.3)[0]
                                       ))

        self.assertTrue(np.array_equal(np.array([1, 1, 1]),
                                       MetricCallback()
                                       .classification_labels_and_predictions_for(np.array([1, 1, 1]),
                                                                                  np.array([0.1, 0.3, 0.2]), 0.2)[0]
                                       ))

if __name__ == '__main__':
    unittest.main()
