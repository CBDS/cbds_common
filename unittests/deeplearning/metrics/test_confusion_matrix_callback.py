import unittest
from unittests.testutils import TestUtils
from sklearn.metrics import confusion_matrix
from cbds.deeplearning.metrics import ConfusionMatrixCallback
import numpy as np
import os


class ConfusionMatrixCallbackTests(unittest.TestCase):
    def setUp(self):
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/confusion_matrix_dataset1.txt")
        os.makedirs(r"test_data/project/model1/runs/2019/4/12/15:34/")

    def tearDown(self):
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/confusion_matrix_dataset1.txt")
        TestUtils.rm_dir_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/")

    def test_evaluate_writes_confusion_matrix_to_disk(self):
        true_labels = np.random.randint(0, 1, 100)
        predictions = np.random.rand(100)
        predicted_labels = predictions > 0.7
        report = confusion_matrix(true_labels, predicted_labels)
        dataset = TestUtils.example_data_set(np.ones(100), true_labels, name="dataset1")
        ConfusionMatrixCallback().evaluate(r"test_data/project/model1/runs/2019/4/12/15:34/",
            dataset, predictions, cut_off=0.7)

        report_from_file = np.loadtxt(r"test_data/project/model1/runs/2019/4/12/15:34/confusion_matrix_dataset1.txt")
        self.assertTrue(np.array_equal(report, report_from_file))

    def test_evaluate_handles_uneven_datasets(self):
        true_labels = np.random.randint(0, 1, 100)
        predictions = np.random.rand(75)
        predicted_labels = predictions > 0.7
        report = confusion_matrix(true_labels[:75], predicted_labels)
        dataset = TestUtils.example_data_set(np.ones(100), true_labels, name="dataset1")
        ConfusionMatrixCallback().evaluate(r"test_data/project/model1/runs/2019/4/12/15:34/",
            dataset, predictions, cut_off=0.7)

        report_from_file = np.loadtxt(r"test_data/project/model1/runs/2019/4/12/15:34/confusion_matrix_dataset1.txt")
        self.assertTrue(np.array_equal(report, report_from_file))


if __name__ == '__main__':
    unittest.main()
