import unittest
from cbds.deeplearning.metrics import SavePredictionsToDisk
from unittests.testutils import TestUtils
import numpy as np
import os


class SavePredictionsToDiskTests(unittest.TestCase):
    def setUp(self):
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset1.npy")
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset2.npy")
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset3.npy")
        self.report_path = r"test_data/project/model1/runs/2019/4/12/15:34/"
        os.makedirs(self.report_path)

    def tearDown(self):
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset1.npy")
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset2.npy")
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset3.npy")
        TestUtils.rm_dir_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/")

    def test_evaluate_creates_prediction_files(self):
        predictions1 = np.random.random(size=50)
        dataset1 = TestUtils.example_data_set(name="dataset1", data=np.random.rand(50, 75, 75, 3),
                                              labels=np.random.randint(2, size=50),
                                              dataset_dir=r"test_data/project/model1/runs/2019/4/12/15:34/")

        predictions2 = np.random.random(size=75)
        dataset2 = TestUtils.example_data_set(name="dataset2", data=np.random.rand(75, 75, 75, 3),
                                              labels=np.random.randint(2, size=75),
                                              dataset_dir=r"test_data/project/model1/runs/2019/4/12/15:34/")

        predictions3 = np.random.random(size=100)
        dataset3 = TestUtils.example_data_set(name="dataset3", data=np.random.rand(100, 75, 75, 3),
                                              labels=np.random.randint(2, size=100),
                                              dataset_dir=r"test_data/project/model1/runs/2019/4/12/15:34/")

        save_predictions_to_disk = SavePredictionsToDisk()
        self.assertFalse(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset1.npy"))
        save_predictions_to_disk.evaluate(report_path=self.report_path, dataset=dataset1, predictions=predictions1,
                                          cut_off=0.5)
        self.assertTrue(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset1.npy"))

        self.assertFalse(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset2.npy"))
        save_predictions_to_disk.evaluate(report_path=self.report_path, dataset=dataset2, predictions=predictions2,
                                          cut_off=0.6)
        self.assertTrue(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset2.npy"))

        self.assertFalse(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset3.npy"))
        save_predictions_to_disk.evaluate(report_path=self.report_path, dataset=dataset3, predictions=predictions3,
                                          cut_off=0.6)
        self.assertTrue(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset3.npy"))

    def test_evaluate_writes_predictions_labels_and_cutoff_to_file(self):
        predictions1 = np.random.random(size=50)
        dataset1 = TestUtils.example_data_set(name="dataset1", data=np.random.rand(50, 75, 75, 3),
                                              labels=np.random.randint(2, size=50),
                                              dataset_dir=r"test_data/project/model1/runs/2019/4/12/15:34/")

        save_predictions_to_disk = SavePredictionsToDisk()
        self.assertFalse(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset1.npy"))
        save_predictions_to_disk.evaluate(report_path=self.report_path, dataset=dataset1, predictions=predictions1,
                                          cut_off=0.5)
        self.assertTrue(os.path.exists(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset1.npy"))
        written_data = np.load(r"test_data/project/model1/runs/2019/4/12/15:34/predictions_dataset1.npy")
        self.assertTrue(np.array_equal(dataset1.labels, written_data[:, 0]))
        self.assertTrue(np.array_equal(predictions1, written_data[:, 1]))
        self.assertTrue(np.array_equal(np.ones(50) * 0.5, written_data[:, 2]))


if __name__ == '__main__':
    unittest.main()
