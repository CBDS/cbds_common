import unittest
from cbds.deeplearning.metrics import ClassificationReportCallback
from unittests.testutils import TestUtils
from sklearn.metrics import classification_report
import numpy as np
import os


class ClassificationReportsCallbackTests(unittest.TestCase):
    def setUp(self):
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/classification_report_dataset1.txt")
        os.makedirs(r"test_data/project/model1/runs/2019/4/12/15:34/")

    def tearDown(self):
        TestUtils.rm_file_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/classification_report_dataset1.txt")
        TestUtils.rm_dir_if_exists(r"test_data/project/model1/runs/2019/4/12/15:34/")

    def test_evaluate_writes_classification_report_to_disk(self):
        true_labels = np.random.randint(0, 1, 100)
        predictions = np.random.rand(100)
        predicted_labels = predictions > 0.7
        report = classification_report(true_labels, predicted_labels)
        dataset = TestUtils.example_data_set(np.ones(100), true_labels, name="dataset1")
        ClassificationReportCallback()\
            .evaluate(r"test_data/project/model1/runs/2019/4/12/15:34",
                      dataset=dataset, predictions=predictions, cut_off=0.7)

        with open(r"test_data/project/model1/runs/2019/4/12/15:34/classification_report_dataset1.txt") as f:
            report_from_file = "".join([line for line in f])
            self.assertEqual(report, report_from_file)

    def test_evaluate_handles_uneven_dataset_sizes(self):
        true_labels = np.random.randint(0, 1, 100)
        predictions = np.random.rand(75)
        predicted_labels = predictions > 0.7
        report = classification_report(true_labels[:75], predicted_labels)
        dataset = TestUtils.example_data_set(np.ones(100), true_labels, name="dataset1")
        ClassificationReportCallback()\
            .evaluate(r"test_data/project/model1/runs/2019/4/12/15:34",
                      dataset=dataset, predictions=predictions, cut_off=0.7)

        with open(r"test_data/project/model1/runs/2019/4/12/15:34/classification_report_dataset1.txt") as f:
            report_from_file = "".join([line for line in f])
            self.assertEqual(report, report_from_file)


if __name__ == '__main__':
    unittest.main()
