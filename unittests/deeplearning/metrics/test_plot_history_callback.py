import unittest
from cbds.deeplearning.metrics import PlotHistoryCallback


class PlotHistoryCallbackTests(unittest.TestCase):
    def test_report_path_set_correctly(self):
        self.assertEqual(r"test_data/project/model1/runs/2019/4/12/15:34/plot_history.png",
                         PlotHistoryCallback(r"test_data/project/model1/runs/2019/4/12/15:34/"
                                                 r"plot_history.png").report_path)

        self.assertEqual(r"test_data/project/model2/runs/2019/4/12/15:34/plot_history.png",
                         PlotHistoryCallback(r"test_data/project/model2/runs/2019/4/12/15:34/"
                                                 r"plot_history.png").report_path)

        self.assertEqual(r"test_data/project/model3/runs/2019/4/12/15:34/plot_history.png",
                         PlotHistoryCallback(r"test_data/project/model3/runs/2019/4/12/15:34/"
                                                 r"plot_history.png").report_path)


if __name__ == '__main__':
    unittest.main()
