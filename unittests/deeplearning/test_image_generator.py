import unittest
import numpy as np
from cbds.deeplearning import ImageGenerator
from unittests.testutils import TestUtils


class ImageGeneratorTests(unittest.TestCase):
    def test_image_generator_sets_data(self):
        self.assertTrue(
            np.array_equal(np.array([1, 2, 3]), ImageGenerator(TestUtils.example_data_set(data=np.array([1, 2, 3]),
                                                                                          labels=np.array([0, 0, 0])))
                                                                                          .data))
        self.assertTrue(
            np.array_equal(np.array([4, 5, 6]), ImageGenerator(TestUtils.example_data_set(data=np.array([4, 5, 6]),
                                                                                          labels=np.array(
                                                                                              [0, 0, 0]))).data))
        self.assertTrue(
            np.array_equal(np.array([7, 8, 9]), ImageGenerator(TestUtils.example_data_set(data=np.array([7, 8, 9]),
                                                                                          labels=np.array(
                                                                                              [0, 0, 0]))).data))
        self.assertTrue(np.array_equal(np.array([10, 11, 12]),
                                       ImageGenerator(TestUtils.example_data_set(data=np.array([10, 11, 12]),
                                                                                 labels=np.array([0, 0, 0]))).data))
        self.assertTrue(np.array_equal(np.array([13, 14, 15]),
                                       ImageGenerator(TestUtils.example_data_set(data=np.array([13, 14, 15]),
                                                                                 labels=np.array([0, 0, 0]))).data))

    def test_image_generator_sets_labels(self):
        self.assertTrue(
            np.array_equal(np.array([0, 0, 0]), ImageGenerator(TestUtils.example_data_set(data=np.array([1, 2, 3]),
                                                                                          labels=np.array(
                                                                                              [0, 0, 0]))).labels))
        self.assertTrue(
            np.array_equal(np.array([0, 1, 0]), ImageGenerator(TestUtils.example_data_set(data=np.array([4, 5, 6]),
                                                                                          labels=np.array(
                                                                                              [0, 1, 0]))).labels))
        self.assertTrue(
            np.array_equal(np.array([0, 1, 2]), ImageGenerator(TestUtils.example_data_set(data=np.array([7, 8, 9]),
                                                                                          labels=np.array(
                                                                                              [0, 1, 2]))).labels))
        self.assertTrue(
            np.array_equal(np.array([0, 1, 1]), ImageGenerator(TestUtils.example_data_set(data=np.array([10, 11, 12]),
                                                                                          labels=np.array(
                                                                                              [0, 1, 1]))).labels))
        self.assertTrue(
            np.array_equal(np.array([1, 1, 1]), ImageGenerator(TestUtils.example_data_set(data=np.array([13, 14, 15]),
                                                                                          labels=np.array(
                                                                                              [1, 1, 1]))).labels))

    def test_name_returns_dataset_name(self):
        self.assertEqual("dataset1", ImageGenerator(TestUtils.example_data_set(name="dataset1", data=np.ones(3),
                                                                               labels=np.ones(3))).name)
        self.assertEqual("dataset2", ImageGenerator(TestUtils.example_data_set(name="dataset2", data=np.ones(3),
                                                                               labels=np.ones(3))).name)
        self.assertEqual("dataset3", ImageGenerator(TestUtils.example_data_set(name="dataset3", data=np.ones(3),
                                                                               labels=np.ones(3))).name)
        self.assertEqual("dataset4", ImageGenerator(TestUtils.example_data_set(name="dataset4", data=np.ones(3),
                                                                               labels=np.ones(3))).name)
        self.assertEqual("dataset5", ImageGenerator(TestUtils.example_data_set(name="dataset5", data=np.ones(3),
                                                                               labels=np.ones(3))).name)

    def test_length_equal_to_number_of_labels(self):
        self.assertEqual(1, len(ImageGenerator(TestUtils.example_data_set(data=np.array([1]), labels=np.ones(1)))))
        self.assertEqual(2, len(ImageGenerator(TestUtils.example_data_set(data=np.array([1]), labels=np.ones(2)))))
        self.assertEqual(3, len(ImageGenerator(TestUtils.example_data_set(data=np.array([1]), labels=np.ones(3)))))
        self.assertEqual(4, len(ImageGenerator(TestUtils.example_data_set(data=np.array([1]), labels=np.ones(4)))))
        self.assertEqual(5, len(ImageGenerator(TestUtils.example_data_set(data=np.array([1]), labels=np.ones(5)))))

    def test_shuffle_data_set_correctly(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       shuffle_data=True).shuffle_data)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        shuffle_data=False).shuffle_data)

    def test_with_shuffle_data_sets_shuffle_data_correctly(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                        .with_shuffle_data(True).shuffle_data)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_shuffle_data(False).shuffle_data)

    def test_seed_set_correctly(self):
        self.assertEqual(10, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            seed=10).seed)
        self.assertEqual(21, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            seed=21).seed)
        self.assertEqual(42, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            seed=42).seed)
        self.assertEqual(84, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            seed=84).seed)
        self.assertEqual(168, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             seed=168).seed)

    def test_with_seed_sets_seed_correctly(self):
        self.assertEqual(10, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_seed(
            10).seed)
        self.assertEqual(21, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_seed(
            21).seed)
        self.assertEqual(42, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_seed(
            42).seed)
        self.assertEqual(84, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_seed(
            84).seed)
        self.assertEqual(168, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_seed(
            168).seed)

    def test_feature_wise_center_sets_feature_wise_center(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       featurewise_center=True).feature_wise_center)
        self.assertFalse(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                           featurewise_center=False).feature_wise_center)

    def test_with_feature_wise_center_sets_feature_wise_center(self):
        self.assertTrue(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_feature_wise_center(
                True).feature_wise_center)
        self.assertFalse(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_feature_wise_center(
                False).feature_wise_center)

    def test_sample_wise_center_sets_sample_wise_center(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       samplewise_center=True).sample_wise_center)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        samplewise_center=False).sample_wise_center)

    def test_with_sample_wise_center_sets_sample_wise_center(self):
        self.assertTrue(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_sample_wise_center(
                True).sample_wise_center)
        self.assertFalse(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_sample_wise_center(
                False).sample_wise_center)

    def test_feature_wise_std_normalization_set_correctly(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       featurewise_std_normalization=True).feature_wise_std_normalization)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        featurewise_std_normalization=False).feature_wise_std_normalization)

    def test_with_feature_wise_std_normalization_set_correctly(self):
        self.assertTrue(ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_feature_wise_std_normalization(
            True).feature_wise_std_normalization)
        self.assertFalse(ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_feature_wise_std_normalization(
            False).feature_wise_std_normalization)

    def test_sample_wise_std_normalization_set_correctly(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       samplewise_std_normalization=True).sample_wise_std_normalization)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        samplewise_std_normalization=False).sample_wise_std_normalization)

    def test_with_sample_wise_std_normalization_set_correctly(self):
        self.assertTrue(ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_sample_wise_std_normalization(
            True).sample_wise_std_normalization)
        self.assertFalse(ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_sample_wise_std_normalization(
            False).sample_wise_std_normalization)

    def test_zca_whitening_set_correctly(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       zca_whitening=True).zca_whitening)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        zca_whitening=False).zca_whitening)

    def test_with_zca_whitening_set_correctly(self):
        self.assertTrue(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zca_whitening(
                True).zca_whitening)
        self.assertFalse(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zca_whitening(
                False).zca_whitening)

    def test_zca_epsilon_set_correctly(self):
        self.assertEqual(1e-1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              zca_epsilon=1e-1).zca_epsilon)
        self.assertEqual(1e-2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              zca_epsilon=1e-2).zca_epsilon)
        self.assertEqual(1e-3, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              zca_epsilon=1e-3).zca_epsilon)
        self.assertEqual(1e-4, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              zca_epsilon=1e-4).zca_epsilon)
        self.assertEqual(1e-5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              zca_epsilon=1e-5).zca_epsilon)

    def test_with_zca_epsilon_set_correctly(self):
        self.assertEqual(1e-1, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zca_epsilon(1e-1).zca_epsilon)
        self.assertEqual(1e-2, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zca_epsilon(1e-2).zca_epsilon)
        self.assertEqual(1e-3, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zca_epsilon(1e-3).zca_epsilon)
        self.assertEqual(1e-4, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zca_epsilon(1e-4).zca_epsilon)
        self.assertEqual(1e-5, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zca_epsilon(1e-5).zca_epsilon)

    def test_rotation_range_set_correctly(self):
        self.assertEqual(10, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            rotation_range=10).rotation_range)
        self.assertEqual(20, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            rotation_range=20).rotation_range)
        self.assertEqual(30, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            rotation_range=30).rotation_range)
        self.assertEqual(40, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            rotation_range=40).rotation_range)
        self.assertEqual(50, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            rotation_range=50).rotation_range)

    def test_with_rotation_range_set_correctly(self):
        self.assertEqual(10, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rotation_range(10).rotation_range)
        self.assertEqual(20, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rotation_range(20).rotation_range)
        self.assertEqual(30, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rotation_range(30).rotation_range)
        self.assertEqual(40, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rotation_range(40).rotation_range)
        self.assertEqual(50, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rotation_range(50).rotation_range)

    def test_width_shift_range_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             width_shift_range=0.1).width_shift_range)
        self.assertEqual(10, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            width_shift_range=10).width_shift_range)
        self.assertEqual([-1, 0, 1], ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                    width_shift_range=[-1, 0, 1]).width_shift_range)
        self.assertEqual([1.0, 2.0, 3.0, 4.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        width_shift_range=[1.0, 2.0, 3.0, 4.0]).width_shift_range)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             width_shift_range=0.5).width_shift_range)

    def test_with_width_shift_range_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_width_shift_range(0.1)
                         .width_shift_range)
        self.assertEqual(10, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_width_shift_range(10)
                         .width_shift_range)
        self.assertEqual([-1, 0, 1], ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_width_shift_range([-1, 0, 1]).width_shift_range)
        self.assertEqual([1.0, 2.0, 3.0, 4.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_width_shift_range([1.0, 2.0, 3.0, 4.0]).width_shift_range)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_width_shift_range(0.5).width_shift_range)

    def test_height_shift_range_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             height_shift_range=0.1).height_shift_range)
        self.assertEqual(10, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                            height_shift_range=10).height_shift_range)
        self.assertEqual([-1, 0, 1], ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                    height_shift_range=[-1, 0, 1]).height_shift_range)
        self.assertEqual([1.0, 2.0, 3.0, 4.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        height_shift_range=[1.0, 2.0, 3.0, 4.0]).height_shift_range)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             height_shift_range=0.5).height_shift_range)

    def test_with_height_shift_range_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_height_shift_range(0.1)
                         .height_shift_range)
        self.assertEqual(10, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_height_shift_range(10)
                         .height_shift_range)
        self.assertEqual([-1, 0, 1], ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_height_shift_range([-1, 0, 1]).height_shift_range)
        self.assertEqual([1.0, 2.0, 3.0, 4.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_height_shift_range([1.0, 2.0, 3.0, 4.0]).height_shift_range)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_height_shift_range(0.5).height_shift_range)

    def test_brightness_range_set_correctly(self):
        self.assertEqual([-1.0, 1.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        brightness_range=[-1.0, 1.0]).brightness_range)
        self.assertEqual((-1.0, 1.0),
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        brightness_range=(-1.0, 1.0)).brightness_range)
        self.assertEqual([-2.0, 2.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        brightness_range=[-2.0, 2.0]).brightness_range)
        self.assertEqual((-2.0, 2.0),
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        brightness_range=(-2.0, 2.0)).brightness_range)
        self.assertEqual([-0.5, 0.5],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        brightness_range=[-0.5, 0.5]).brightness_range)

    def test_with_brightness_range_set_correctly(self):
        self.assertEqual([-1.0, 1.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_brightness_range([-1.0, 1.0]).brightness_range)
        self.assertEqual((-1.0, 1.0),
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_brightness_range((-1.0, 1.0)).brightness_range)
        self.assertEqual([-2.0, 2.0],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_brightness_range([-2.0, 2.0]).brightness_range)
        self.assertEqual((-2.0, 2.0),
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_brightness_range((-2.0, 2.0)).brightness_range)
        self.assertEqual([-0.5, 0.5],
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_brightness_range([-0.5, 0.5]).brightness_range)

    def test_shear_range_set_correctly(self):
        self.assertEqual(10.0, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              shear_range=10.0).shear_range)
        self.assertEqual(20.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              shear_range=20.1).shear_range)
        self.assertEqual(30.2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              shear_range=30.2).shear_range)
        self.assertEqual(40.3, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              shear_range=40.3).shear_range)
        self.assertEqual(50.4, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                              shear_range=50.4).shear_range)

    def test_with_shear_range_set_correctly(self):
        self.assertEqual(10.0, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_shear_range(10.0).shear_range)
        self.assertEqual(20.1, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_shear_range(20.1).shear_range)
        self.assertEqual(30.2, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_shear_range(30.2).shear_range)
        self.assertEqual(40.3, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_shear_range(40.3).shear_range)
        self.assertEqual(50.4, ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_shear_range(50.4).shear_range)

    def test_zoom_range_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             zoom_range=0.1).zoom_range)
        self.assertEqual(0.2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             zoom_range=0.2).zoom_range)
        self.assertEqual(0.3, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             zoom_range=0.3).zoom_range)
        self.assertEqual(0.4, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             zoom_range=0.4).zoom_range)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             zoom_range=0.5).zoom_range)

    def test_with_zoom_range_set_correctly(self):
        self.assertEqual(0.1,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zoom_range(
                             0.1).zoom_range)
        self.assertEqual(0.2,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zoom_range(
                             0.2).zoom_range)
        self.assertEqual(0.3,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zoom_range(
                             0.3).zoom_range)
        self.assertEqual(0.4,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zoom_range(
                             0.4).zoom_range)
        self.assertEqual(0.5,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_zoom_range(
                             0.5).zoom_range)

    def test_channel_shift_range_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             channel_shift_range=0.1).channel_shift_range)
        self.assertEqual(0.2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             channel_shift_range=0.2).channel_shift_range)
        self.assertEqual(0.3, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             channel_shift_range=0.3).channel_shift_range)
        self.assertEqual(0.4, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             channel_shift_range=0.4).channel_shift_range)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             channel_shift_range=0.5).channel_shift_range)

    def test_with_channel_shift_range_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_channel_shift_range(0.1).channel_shift_range)
        self.assertEqual(0.2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_channel_shift_range(0.2).channel_shift_range)
        self.assertEqual(0.3, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_channel_shift_range(0.3).channel_shift_range)
        self.assertEqual(0.4, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_channel_shift_range(0.4).channel_shift_range)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_channel_shift_range(0.5).channel_shift_range)

    def test_fill_mode_set_correctly(self):
        self.assertEqual("constant", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                    fill_mode="constant").fill_mode)
        self.assertEqual("nearest", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                   fill_mode="nearest").fill_mode)
        self.assertEqual("reflect", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                   fill_mode="reflect").fill_mode)
        self.assertEqual("wrap", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                fill_mode="wrap").fill_mode)

    def test_with_fill_mode_set_correctly(self):
        self.assertEqual("constant",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_fill_mode(
                             "constant").fill_mode)
        self.assertEqual("nearest",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_fill_mode(
                             "nearest").fill_mode)
        self.assertEqual("reflect",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_fill_mode(
                             "reflect").fill_mode)
        self.assertEqual("wrap", ImageGenerator(
            TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_fill_mode("wrap").fill_mode)

    def test_cval_set_correctly(self):
        self.assertEqual(0.1,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)), cval=0.1).cval)
        self.assertEqual(1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)), cval=1).cval)
        self.assertEqual(1.5,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)), cval=1.5).cval)
        self.assertEqual(2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)), cval=2).cval)
        self.assertEqual(3.1,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)), cval=3.1).cval)

    def test_with_cval_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_cval(
            0.1).cval)
        self.assertEqual(1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_cval(
            1).cval)
        self.assertEqual(1.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_cval(
            1.5).cval)
        self.assertEqual(2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_cval(
            2).cval)
        self.assertEqual(3.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_cval(
            3.1).cval)

    def test_horizontal_flip_set_correctly(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       horizontal_flip=True).horizontal_flip)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        horizontal_flip=False).horizontal_flip)

    def test_with_horizontal_flip_set_correctly(self):
        self.assertTrue(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_horizontal_flip(
                True).horizontal_flip)
        self.assertFalse(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_horizontal_flip(
                False).horizontal_flip)

    def test_vertical_flip_set_correctly(self):
        self.assertTrue(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                       vertical_flip=True).vertical_flip)
        self.assertFalse(ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        vertical_flip=False).vertical_flip)

    def test_with_vertical_flip_set_correctly(self):
        self.assertTrue(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_vertical_flip(
                True).vertical_flip)
        self.assertFalse(
            ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_vertical_flip(
                False).vertical_flip)

    def test_rescale_set_correctly(self):
        self.assertEqual(0, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                           rescale=0).rescale)
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             rescale=0.1).rescale)
        self.assertEqual(0.2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             rescale=0.2).rescale)
        self.assertEqual(0.3, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             rescale=0.3).rescale)
        self.assertEqual(0.4, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             rescale=0.4).rescale)

    def test_with_rescale_set_correctly(self):
        self.assertEqual(0, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rescale(
            0).rescale)
        self.assertEqual(0.1,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rescale(
                             0.1).rescale)
        self.assertEqual(0.2,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rescale(
                             0.2).rescale)
        self.assertEqual(0.3,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rescale(
                             0.3).rescale)
        self.assertEqual(0.4,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_rescale(
                             0.4).rescale)

    def test_preprocessing_function_set_correctly(self):
        preprocessing_function1 = lambda image: image
        self.assertEqual(preprocessing_function1,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        preprocessing_function=preprocessing_function1).preprocessing_function)

        preprocessing_function2 = lambda image: image
        self.assertEqual(preprocessing_function2,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        preprocessing_function=preprocessing_function2).preprocessing_function)

        preprocessing_function3 = lambda image: image
        self.assertEqual(preprocessing_function3,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        preprocessing_function=preprocessing_function3).preprocessing_function)

    def test_with_preprocessing_function_set_correctly(self):
        preprocessing_function1 = lambda image: image
        self.assertEqual(preprocessing_function1,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_preprocessing_function(preprocessing_function1).preprocessing_function)

        preprocessing_function2 = lambda image: image
        self.assertEqual(preprocessing_function2,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_preprocessing_function(preprocessing_function2).preprocessing_function)

        preprocessing_function3 = lambda image: image
        self.assertEqual(preprocessing_function3,
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_preprocessing_function(preprocessing_function3).preprocessing_function)

    def test_data_format_set_correctly(self):
        self.assertEqual("channels_first",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                        data_format="channels_first").data_format)
        self.assertEqual("channels_last", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                         data_format="channels_last").data_format)

    def test_with_data_format_set_correctly(self):
        self.assertEqual("channels_first",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_data_format("channels_first").data_format)
        self.assertEqual("channels_last", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)))
                         .with_data_format("channels_last").data_format)

    def test_validation_split_set_correctly(self):
        self.assertEqual(0.1, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             validation_split=0.1).validation_split)
        self.assertEqual(0.2, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             validation_split=0.2).validation_split)
        self.assertEqual(0.3, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             validation_split=0.3).validation_split)
        self.assertEqual(0.4, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             validation_split=0.4).validation_split)
        self.assertEqual(0.5, ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                             validation_split=0.5).validation_split)

    def test_with_validation_split_set_correctly(self):
        self.assertEqual(0.1,
                         ImageGenerator(
                             TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_validation_split(
                             0.1).validation_split)
        self.assertEqual(0.2,
                         ImageGenerator(
                             TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_validation_split(
                             0.2).validation_split)
        self.assertEqual(0.3,
                         ImageGenerator(
                             TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_validation_split(
                             0.3).validation_split)
        self.assertEqual(0.4,
                         ImageGenerator(
                             TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_validation_split(
                             0.4).validation_split)
        self.assertEqual(0.5,
                         ImageGenerator(
                             TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_validation_split(
                             0.5).validation_split)

    def test_dtype_set_correctly(self):
        self.assertEqual("uint8", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                 dtype="uint8").dtype)
        self.assertEqual("uint16", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                  dtype="uint16").dtype)
        self.assertEqual("uint32", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                  dtype="uint32").dtype)
        self.assertEqual("float32", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                   dtype="float32").dtype)
        self.assertEqual("float64", ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                                   dtype="float64").dtype)

    def test_with_dtype_set_correctly(self):
        self.assertEqual("uint8",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_dtype(
                             "uint8").dtype)
        self.assertEqual("uint16",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_dtype(
                             "uint16").dtype)
        self.assertEqual("uint32",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_dtype(
                             "uint32").dtype)
        self.assertEqual("float32",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_dtype(
                             "float32").dtype)
        self.assertEqual("float64",
                         ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3))).with_dtype(
                             "float64").dtype)

    def test_inner_generator_from_image_generator_settings(self):
        preprocessing_function = lambda image: image
        image_generator = ImageGenerator(TestUtils.example_data_set(data=np.ones(3), labels=np.ones(3)),
                                         featurewise_center=True,
                                         samplewise_center=True, featurewise_std_normalization=True,
                                         samplewise_std_normalization=True, zca_whitening=False, zca_epsilon=1e-06,
                                         rotation_range=10, width_shift_range=0.1, height_shift_range=0.2,
                                         brightness_range=[-1, 1], shear_range=0.3, zoom_range=0.4,
                                         channel_shift_range=0.5, fill_mode="constant", cval=0.6, horizontal_flip=True,
                                         vertical_flip=True, rescale=0.7,
                                         preprocessing_function=preprocessing_function, data_format="channels_first",
                                         validation_split=0.8, dtype="uint8")

        self.assertEqual(image_generator.inner_generator.featurewise_center,
                         image_generator.feature_wise_center)
        self.assertEqual(image_generator.inner_generator.samplewise_center,
                         image_generator.sample_wise_center)
        self.assertEqual(image_generator.inner_generator.featurewise_std_normalization,
                         image_generator.feature_wise_std_normalization)
        self.assertEqual(image_generator.inner_generator.samplewise_std_normalization,
                         image_generator.sample_wise_std_normalization)
        self.assertEqual(image_generator.inner_generator.zca_whitening,
                         image_generator.zca_whitening)
        self.assertEqual(image_generator.inner_generator.zca_epsilon,
                         image_generator.zca_epsilon)
        self.assertEqual(image_generator.inner_generator.rotation_range,
                         image_generator.rotation_range)
        self.assertEqual(image_generator.inner_generator.width_shift_range,
                         image_generator.width_shift_range)
        self.assertEqual(image_generator.inner_generator.height_shift_range,
                         image_generator.height_shift_range)
        self.assertEqual(image_generator.inner_generator.brightness_range,
                         image_generator.brightness_range)
        self.assertEqual(image_generator.inner_generator.shear_range,
                         image_generator.shear_range)
        self.assertEqual(image_generator.inner_generator.zoom_range,
                         [0.6, 1.4])
        self.assertEqual(image_generator.inner_generator.channel_shift_range,
                         image_generator.channel_shift_range)
        self.assertEqual(image_generator.inner_generator.fill_mode,
                         image_generator.fill_mode)
        self.assertEqual(image_generator.inner_generator.cval,
                         image_generator.cval)
        self.assertEqual(image_generator.inner_generator.horizontal_flip,
                         image_generator.horizontal_flip)
        self.assertEqual(image_generator.inner_generator.vertical_flip,
                         image_generator.vertical_flip)
        self.assertEqual(image_generator.inner_generator.rescale,
                         image_generator.rescale)
        self.assertEqual(image_generator.inner_generator.preprocessing_function,
                         image_generator.preprocessing_function)
        self.assertEqual(image_generator.inner_generator.data_format,
                         image_generator.data_format)
        self.assertEqual(image_generator.inner_generator.dtype,
                         image_generator.dtype)

    def test_to_json_converts_settings_to_json(self):
        image_generator1 = ImageGenerator(TestUtils.example_data_set(name="dataset1",
                                                                     dataset_dir=r"test_data/project/datasets/dataset1",
                                                                     data=np.ones(3), labels=np.ones(3)))
        self.assertDictEqual({
            "dataset": {
                "name": "dataset1",
                "dataset_dir": r"test_data/project/datasets/dataset1",
                "dataset_filename": r"test_data/project/datasets/dataset1/dataset1.npy",
                "labels_filename": r"test_data/project/datasets/dataset1/dataset1_labels.npy",
                "SettingsProvider": {
                    "settings_dir": r"test_data/project/datasets/dataset1",
                    "settings_filename": "dataset.json"
                }
            },
            "shuffle_data": True,
            "seed": None,
            "feature_wise_center": False,
            "sample_wise_center": False,
            "feature_wise_std_normalization": False,
            "sample_wise_std_normalization": False,
            "zca_whitening": False,
            "zca_epsilon": 1e-06,
            "rotation_range": 0,
            "width_shift_range": 0.0,
            "height_shift_range": 0.0,
            "brightness_range": None,
            "shear_range": 0.0,
            "zoom_range": 0.0,
            "channel_shift_range": 0.0,
            "fill_mode": "nearest",
            "cval": 0.0,
            "horizontal_flip": False,
            "vertical_flip": False,
            "rescale": None,
            "data_format": None,
            "validation_split": 0.0,
            "dtype": None

        }, image_generator1.to_dict())

        image_generator2 = ImageGenerator(TestUtils.example_data_set(name="dataset2",
                                                                     dataset_dir=r"test_data/project/datasets/dataset2",
                                                                     data=np.ones(3), labels=np.ones(3)),
                                          featurewise_center=True,
                                          shuffle_data=False, seed=42, samplewise_center=True,
                                          featurewise_std_normalization=True,
                                          samplewise_std_normalization=True, zca_whitening=False, zca_epsilon=1e-06,
                                          rotation_range=10, width_shift_range=0.1, height_shift_range=0.2,
                                          brightness_range=[-1, 1], shear_range=0.3, zoom_range=0.4,
                                          channel_shift_range=0.5, fill_mode="constant", cval=0.6, horizontal_flip=True,
                                          vertical_flip=True, rescale=0.7, data_format="channels_first",
                                          validation_split=0.8, dtype="uint8")

        self.assertEqual({
            "dataset": {
                "name": "dataset2",
                "dataset_dir": r"test_data/project/datasets/dataset2",
                "dataset_filename": r"test_data/project/datasets/dataset2/dataset2.npy",
                "labels_filename": r"test_data/project/datasets/dataset2/dataset2_labels.npy",
                "SettingsProvider": {
                    "settings_dir": r"test_data/project/datasets/dataset2",
                    "settings_filename": "dataset.json"
                }
            },
            "shuffle_data": False,
            "seed": 42,
            "feature_wise_center": True,
            "sample_wise_center": True,
            "feature_wise_std_normalization": True,
            "sample_wise_std_normalization": True,
            "zca_whitening": False,
            "zca_epsilon": 1e-06,
            "rotation_range": 10,
            "width_shift_range": 0.1,
            "height_shift_range": 0.2,
            "brightness_range": [-1, 1],
            "shear_range": 0.3,
            "zoom_range": 0.4,
            "channel_shift_range": 0.5,
            "fill_mode": "constant",
            "cval": 0.6,
            "horizontal_flip": True,
            "vertical_flip": True,
            "rescale": 0.7,
            "data_format": "channels_first",
            "validation_split": 0.8,
            "dtype": "uint8"
        }, image_generator2.to_dict())

    def test_from_json_loads_settings_from_dict(self):
        image_generator = ImageGenerator.from_dict({
            "dataset": {
                "name": "dataset1",
                "dataset_dir": r"test_data/project/datasets/dataset1",
                "dataset_filename": r"test_data/project/datasets/dataset1.npy",
                "labels_filename": r"test_data/project/datasets/dataset1_labels.npy",
                "SettingsProvider": {
                    "settings_dir": r"test_data/project/datasets/dataset1",
                    "settings_filename": "dataset.json"
                }
            },
            "shuffle_data": False,
            "seed": 84,
            "feature_wise_center": True,
            "sample_wise_center": True,
            "feature_wise_std_normalization": True,
            "sample_wise_std_normalization": True,
            "zca_whitening": False,
            "zca_epsilon": 1e-06,
            "rotation_range": 10,
            "width_shift_range": 0.1,
            "height_shift_range": 0.2,
            "brightness_range": [-1, 1],
            "shear_range": 0.3,
            "zoom_range": 0.4,
            "channel_shift_range": 0.5,
            "fill_mode": "constant",
            "cval": 0.6,
            "horizontal_flip": True,
            "vertical_flip": True,
            "rescale": 0.7,
            "data_format": "channels_first",
            "validation_split": 0.8,
            "dtype": "uint8"
        })

        self.assertEqual("dataset1", image_generator.dataset.name)
        self.assertEqual(r"test_data/project/datasets/dataset1", image_generator.dataset.dataset_dir)
        self.assertEqual(r"test_data/project/datasets/dataset1.npy", image_generator.dataset.dataset_filename)
        self.assertEqual(r"test_data/project/datasets/dataset1_labels.npy", image_generator.dataset.labels_filename)
        self.assertEqual(False, image_generator.shuffle_data)
        self.assertEqual(84, image_generator.seed)
        self.assertEqual(True, image_generator.feature_wise_center)
        self.assertEqual(True, image_generator.sample_wise_center)
        self.assertEqual(True, image_generator.feature_wise_std_normalization)
        self.assertEqual(True, image_generator.sample_wise_std_normalization)
        self.assertEqual(False, image_generator.zca_whitening)
        self.assertEqual(1e-06, image_generator.zca_epsilon)
        self.assertEqual(10, image_generator.rotation_range)
        self.assertEqual(0.1, image_generator.width_shift_range)
        self.assertEqual(0.2, image_generator.height_shift_range)
        self.assertEqual([-1, 1], image_generator.brightness_range)
        self.assertEqual(0.3, image_generator.shear_range)
        self.assertEqual(0.4, image_generator.zoom_range)
        self.assertEqual(0.5, image_generator.channel_shift_range)
        self.assertEqual("constant", image_generator.fill_mode)
        self.assertEqual(0.6, image_generator.cval)
        self.assertEqual(True, image_generator.horizontal_flip)
        self.assertEqual(True, image_generator.vertical_flip)
        self.assertEqual(0.7, image_generator.rescale)
        self.assertEqual("channels_first", image_generator.data_format)
        self.assertEqual(0.8, image_generator.validation_split)
        self.assertEqual("uint8", image_generator.dtype)


if __name__ == '__main__':
    unittest.main()
