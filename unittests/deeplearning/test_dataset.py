import unittest
from cbds.deeplearning import Dataset
from unittests.testutils import TestUtils
import numpy as np
import os


class DatasetTests(unittest.TestCase):
    def setUp(self):
        TestUtils.rm_file_if_exists(r"../test_data/project/datasets/dataset1/dataset1.npy")
        TestUtils.rm_file_if_exists(r"../test_data/project/datasets/dataset1/dataset1_labels.npy")
        TestUtils.rm_file_if_exists(r"../test_data/project/datasets/dataset2/dataset2.npy")
        TestUtils.rm_file_if_exists(r"../test_data/project/datasets/dataset2/dataset2_labels.npy")
        TestUtils.rm_file_if_exists(r"../test_data/project/datasets/dataset3/dataset3.npy")
        TestUtils.rm_file_if_exists(r"../test_data/project/datasets/dataset3/dataset3_labels.npy")
        TestUtils.rm_dir_if_exists(r"../test_data/project/datasets/dataset1")
        TestUtils.rm_dir_if_exists(r"../test_data/project/datasets/dataset2")
        TestUtils.rm_dir_if_exists(r"../test_data/project/datasets/dataset3")
        os.makedirs(r"../test_data/project/datasets/dataset1")
        os.makedirs(r"../test_data/project/datasets/dataset2")
        os.makedirs(r"../test_data/project/datasets/dataset3")

    def tearDown(self):
        TestUtils.rm_dir_if_exists(r"../test_data/project/datasets/dataset1")
        TestUtils.rm_dir_if_exists(r"../test_data/project/datasets/dataset2")
        TestUtils.rm_dir_if_exists(r"../test_data/project/datasets/dataset3")

    def test_dataset_sets_name(self):
        self.assertEqual("dataset1", Dataset("dataset1").name)
        self.assertEqual("dataset2", Dataset("dataset2").name)
        self.assertEqual("dataset3", Dataset("dataset3").name)
        self.assertEqual("dataset4", Dataset("dataset4").name)
        self.assertEqual("dataset5", Dataset("dataset5").name)

    def test_dataset_sets_default_dataset_filename(self):
        self.assertEqual(r"../test_data/project/datasets/dataset1/dataset1.npy",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1").dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset2/dataset2.npy",
                         Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2").dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset3/dataset3.npy",
                         Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3").dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset4/dataset4.npy",
                         Dataset(name="dataset4", dataset_dir=r"../test_data/project/datasets/dataset4").dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset5/dataset5.npy",
                         Dataset(name="dataset5", dataset_dir=r"../test_data/project/datasets/dataset5").dataset_filename)

    def test_dataset_sets_default_labels_filename(self):
        self.assertEqual(r"../test_data/project/datasets/dataset1/dataset1_labels.npy",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1").labels_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset2/dataset2_labels.npy",
                         Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2").labels_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset3/dataset3_labels.npy",
                         Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3").labels_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset4/dataset4_labels.npy",
                         Dataset(name="dataset4", dataset_dir=r"../test_data/project/datasets/dataset4").labels_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset5/dataset5_labels.npy",
                         Dataset(name="dataset5", dataset_dir=r"../test_data/project/datasets/dataset5").labels_filename)

    def test_dataset_sets_dataset_dir(self):
        self.assertEqual(r"../test_data/project/datasets/dataset1",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1").dataset_dir)
        self.assertEqual(r"../test_data/project/datasets/dataset2",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset2").dataset_dir)
        self.assertEqual(r"../test_data/project/datasets/dataset3",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset3").dataset_dir)
        self.assertEqual(r"../test_data/project/datasets/dataset4",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset4").dataset_dir)
        self.assertEqual(r"../test_data/project/datasets/dataset5",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset5").dataset_dir)

    def test_import_numpy_dataset_sets_dataset_filename(self):
        self.assertEqual(r"../test_data/project/datasets/dataset1/dataset_1.npy",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1")
                         .import_numpy_dataset(r"../test_data/dataset_1.npy", "").dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset2/dataset_2.npy",
                         Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2")
                         .import_numpy_dataset(r"../test_data/dataset_2.npy", "").dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset3/dataset_3.npy",
                         Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3")
                         .import_numpy_dataset(r"../test_data/dataset_3.npy", "").dataset_filename)

    def test_import_numpy_dataset_sets_labels_filename(self):
        self.assertEqual(r"../test_data/project/datasets/dataset1/dataset_1_labels.npy",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1")
                         .import_numpy_dataset(r"../test_data/dataset_1.npy", r"../test_data/dataset_1_labels.npy")
                         .labels_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset2/dataset_2_labels.npy",
                         Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2")
                         .import_numpy_dataset(r"../test_data/dataset_2.npy", r"../test_data/dataset_2_labels.npy")
                         .labels_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset3/dataset_3_labels.npy",
                         Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3")
                         .import_numpy_dataset(r"../test_data/dataset_3.npy", r"../test_data/dataset_3_labels.npy")
                         .labels_filename)

    def test_import_numpy_dataset_copies_dataset(self):
        self.assertFalse(os.path.exists(r"../test_data/project/datasets/dataset1/dataset_1.npy"))
        Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1") \
            .import_numpy_dataset(r"../test_data/dataset_1.npy", r"../test_data/dataset_1_labels.npy")
        self.assertTrue(os.path.exists(r"../test_data/project/datasets/dataset1/dataset_1.npy"))

        self.assertFalse(os.path.exists(r"../test_data/project/datasets/dataset2/dataset_2.npy"))
        Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2") \
            .import_numpy_dataset(r"../test_data/dataset_2.npy", r"../test_data/dataset_2_labels.npy")
        self.assertTrue(os.path.exists(r"../test_data/project/datasets/dataset2/dataset_2.npy"))

        self.assertFalse(os.path.exists(r"../test_data/project/datasets/dataset3/dataset_3.npy"))
        Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3") \
            .import_numpy_dataset(r"../test_data/dataset_3.npy", r"../test_data/dataset_3_labels.npy")
        self.assertTrue(os.path.exists(r"../test_data/project/datasets/dataset3/dataset_3.npy"))

    def test_import_numpy_dataset_copies_labels(self):
        self.assertFalse(os.path.exists(r"../test_data/project/datasets/dataset1/dataset_1_labels.npy"))
        Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1") \
            .import_numpy_dataset(r"../test_data/dataset_1.npy", r"../test_data/dataset_1_labels.npy")
        self.assertTrue(os.path.exists(r"../test_data/project/datasets/dataset1/dataset_1_labels.npy"))

        self.assertFalse(os.path.exists(r"../test_data/project/datasets/dataset2/dataset_2_labels.npy"))
        Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2") \
            .import_numpy_dataset(r"../test_data/dataset_2.npy", r"../test_data/dataset_2_labels.npy")
        self.assertTrue(os.path.exists(r"../test_data/project/datasets/dataset2/dataset_2_labels.npy"))

        self.assertFalse(os.path.exists(r"../test_data/project/datasets/dataset3/dataset_3_labels.npy"))
        Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3") \
            .import_numpy_dataset(r"../test_data/dataset_3.npy", r"../test_data/dataset_3_labels.npy")
        self.assertTrue(os.path.exists(r"../test_data/project/datasets/dataset3/dataset_3_labels.npy"))

    def test_labels_loads_labels_from_numpy_file(self):
        dataset1 = Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1") \
            .import_numpy_dataset(r"../test_data/dataset_1.npy", r"../test_data/dataset_1_labels.npy")
        self.assertEqual(3, len(dataset1.labels))
        self.assertTrue(np.array_equal(np.load(r"../test_data/project/datasets/dataset1/dataset_1_labels.npy"),
                                       dataset1.labels))

        dataset2 = Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2") \
            .import_numpy_dataset(r"../test_data/dataset_2.npy", r"../test_data/dataset_2_labels.npy")
        self.assertEqual(5, len(dataset2.labels))
        self.assertTrue(np.array_equal(np.load(r"../test_data/project/datasets/dataset2/dataset_2_labels.npy"),
                                       dataset2.labels))

        dataset3 = Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3") \
            .import_numpy_dataset(r"../test_data/dataset_3.npy", r"../test_data/dataset_3_labels.npy")
        self.assertEqual(7, len(dataset3.labels))
        self.assertTrue(np.array_equal(np.load(r"../test_data/project/datasets/dataset3/dataset_3_labels.npy"),
                                       dataset3.labels))

    def test_data_loads_data_from_numpy_file(self):
        dataset1 = Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1") \
            .import_numpy_dataset(r"../test_data/dataset_1.npy", r"../test_data/dataset_1_labels.npy")
        self.assertEqual(3, len(dataset1.data))
        self.assertTrue(np.array_equal(np.load(r"../test_data/project/datasets/dataset1/dataset_1.npy"), dataset1.data))

        dataset2 = Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2") \
            .import_numpy_dataset(r"../test_data/dataset_2.npy", r"../test_data/dataset_2_labels.npy")
        self.assertEqual(5, len(dataset2.data))
        self.assertTrue(np.array_equal(np.load(r"../test_data/project/datasets/dataset2/dataset_2.npy"), dataset2.data))

        dataset3 = Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3") \
            .import_numpy_dataset(r"../test_data/dataset_3.npy", r"../test_data/dataset_3_labels.npy")
        self.assertEqual(7, len(dataset3.data))
        self.assertTrue(np.array_equal(np.load(r"../test_data/project/datasets/dataset3/dataset_3.npy"), dataset3.data))

    def test_split_splits_datasets(self):
        (train1, test1) = Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1"). \
            import_numpy_dataset(r"../test_data/dataset_1.npy", r"../test_data/dataset_1_labels.npy"). \
            split(test_size=0.33)
        self.assertEqual(2, len(train1))
        self.assertEqual(1, len(test1))

        (train2, test2) = Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2"). \
            import_numpy_dataset(r"../test_data/dataset_2.npy", r"../test_data/dataset_2_labels.npy"). \
            split(test_size=0.2)
        self.assertEqual(4, len(train2))
        self.assertEqual(1, len(test2))

        (train3, test3) = Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2"). \
            import_numpy_dataset(r"../test_data/dataset_2.npy", r"../test_data/dataset_2_labels.npy"). \
            split(test_size=0.4)
        self.assertEqual(3, len(train3))
        self.assertEqual(2, len(test3))

        (train4, test4) = Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3"). \
            import_numpy_dataset(r"../test_data/dataset_3.npy", r"../test_data/dataset_3_labels.npy"). \
            split(test_size=0.4)
        self.assertEqual(4, len(train4))
        self.assertEqual(3, len(test4))

        (train5, test5) = Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3"). \
            import_numpy_dataset(r"../test_data/dataset_3.npy", r"../test_data/dataset_3_labels.npy"). \
            split(test_size=0.6)
        self.assertEqual(2, len(train5))
        self.assertEqual(5, len(test5))

    def test_to_dict_returns_dict_with_dataset_settings(self):
        self.assertDictEqual({
            "name": "dataset1",
            "dataset_dir": r"../test_data/project/datasets/dataset1",
            "dataset_filename": r"../test_data/project/datasets/dataset1/dataset1.npy",
            "labels_filename": r"../test_data/project/datasets/dataset1/dataset1_labels.npy",
            "SettingsProvider": {
                "settings_dir": r"../test_data/project/datasets/dataset1",
                "settings_filename": "dataset.json"
            }
        }, Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1").to_dict())

    def test_from_dict_creates_dataset_from_settings_in_dict(self):
        dataset = Dataset.instance_from_dict({
            "name": "dataset2",
            "dataset_dir": r"../test_data/project/datasets/dataset2",
            "dataset_filename": r"../test_data/project/datasets/dataset2/dataset2.npy",
            "labels_filename": r"../test_data/project/datasets/dataset2/dataset2_labels.npy",
            "SettingsProvider": {
                "settings_dir": r"../test_data/project/datasets/dataset2",
                "settings_filename": "dataset.json"
            }

        })
        self.assertEqual("dataset2", dataset.name)
        self.assertEqual(r"../test_data/project/datasets/dataset2", dataset.dataset_dir)
        self.assertEqual(r"../test_data/project/datasets/dataset2/dataset2.npy", dataset.dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset2/dataset2_labels.npy", dataset.labels_filename)
        self.assertEqual(r"../test_data/project/datasets/dataset2", dataset.settings_dir)
        self.assertEqual("dataset.json", dataset.settings_filename)

    def test_flow_returns_dataset_data_in_batches(self):
        data = np.random.rand(160, 75, 75, 3)
        labels = np.random.randint(0, 1, 160)

        dataset = TestUtils.example_data_set(data=data, labels=labels)
        generator = dataset.flow(batch_size=32)

        first_batch = next(generator)
        self.assertEqual(32, len(first_batch[0]))
        self.assertTrue(np.allclose(data[:32, :, :, :], first_batch[0]))
        self.assertTrue(np.array_equal(labels[:32], first_batch[1]))

        second_batch = next(generator)
        self.assertEqual(32, len(second_batch[0]))
        self.assertTrue(np.allclose(data[32:64, :, :, :], second_batch[0]))
        self.assertTrue(np.array_equal(labels[32:64], second_batch[1]))

        third_batch = next(generator)
        self.assertEqual(32, len(third_batch[0]))
        self.assertTrue(np.allclose(data[64:96, :, :, :], third_batch[0]))
        self.assertTrue(np.array_equal(labels[64:96], third_batch[1]))

        fourth_batch = next(generator)
        self.assertEqual(32, len(fourth_batch[0]))
        self.assertTrue(np.allclose(data[96:128, :, :, :], fourth_batch[0]))
        self.assertTrue(np.array_equal(labels[96:128], fourth_batch[1]))

        fifth_batch = next(generator)
        self.assertEqual(32, len(fifth_batch[0]))
        self.assertTrue(np.allclose(data[128:, :, :, :], fifth_batch[0]))
        self.assertTrue(np.array_equal(labels[128:], fifth_batch[1]))

    def test_settings_filename_set_correctly(self):
        self.assertEqual("dataset.json",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1").settings_filename)
        self.assertEqual("dataset.json",
                         Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2").settings_filename)
        self.assertEqual("dataset.json",
                         Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3").settings_filename)

    def test_settings_dir_set_correctly(self):
        self.assertEqual(r"../test_data/project/datasets/dataset1",
                         Dataset(name="dataset1", dataset_dir=r"../test_data/project/datasets/dataset1").settings_dir)
        self.assertEqual(r"../test_data/project/datasets/dataset2",
                         Dataset(name="dataset2", dataset_dir=r"../test_data/project/datasets/dataset2").settings_dir)
        self.assertEqual(r"../test_data/project/datasets/dataset3",
                         Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3").settings_dir)

    def test_load_loads_settings_from_disk(self):
        dataset = Dataset.load(r"../test_data/dataset.json")
        self.assertEqual("Heerlen", dataset.name)
        self.assertEqual(r"../test_data/project/datasets/Heerlen", dataset.dataset_dir)
        self.assertEqual(r"../test_data/project/datasets/Heerlen/data.npy", dataset.dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/Heerlen/labels.npy", dataset.labels_filename)
        self.assertEqual(r"../test_data/project/datasets/Heerlen", dataset.settings_dir)
        self.assertEqual("dataset.json", dataset.settings_filename)

    def test_save_saves_settings_to_disk(self):
        dataset = Dataset(name="dataset3", dataset_dir=r"../test_data/project/datasets/dataset3")
        dataset.import_numpy_dataset(r"../test_data/dataset_1.npy", r"../test_data/dataset_1_labels.npy")
        dataset.save(r"../test_data/project/datasets/dataset3/dataset.json")

        saved_dataset = Dataset.load(r"../test_data/project/datasets/dataset3/dataset.json")
        self.assertEqual(dataset.name, saved_dataset.name)
        self.assertEqual(dataset.dataset_dir, saved_dataset.dataset_dir)
        self.assertEqual(dataset.dataset_filename, saved_dataset.dataset_filename)
        self.assertEqual(dataset.labels_filename, saved_dataset.labels_filename)
        self.assertEqual(dataset.settings_dir, saved_dataset.settings_dir)
        self.assertEqual(dataset.settings_filename, saved_dataset.settings_filename)

    def test_settings_loaded_automatically_if_available(self):
        dataset = Dataset(dataset_dir=r"../test_data")
        self.assertEqual("Heerlen", dataset.name)
        self.assertEqual(r"../test_data/project/datasets/Heerlen", dataset.dataset_dir)
        self.assertEqual(r"../test_data/project/datasets/Heerlen", dataset.settings_dir)
        self.assertEqual("dataset.json", dataset.settings_filename)
        self.assertEqual(r"../test_data/project/datasets/Heerlen/data.npy", dataset.dataset_filename)
        self.assertEqual(r"../test_data/project/datasets/Heerlen/labels.npy", dataset.labels_filename)

    def test_sample_returns_sample_of_data(self):
        data = np.concatenate([np.ones((30, 75, 75, 3)), np.zeros((70, 75, 75, 3))])
        labels = np.concatenate([np.ones(30), np.zeros(70)])
        dataset = TestUtils.example_data_set(data, labels)

        sampled_data1 = dataset.sample(0.1)
        self.assertEqual(3, len(np.where(sampled_data1.labels == 1)[0]))
        self.assertEqual(3, len(np.where(np.all(sampled_data1.data, axis=(1, 2, 3)) == 1)[0]))
        self.assertEqual(7, len(np.where(sampled_data1.labels == 0)[0]))
        self.assertEqual(7, len(np.where(np.all(sampled_data1.data, axis=(1, 2, 3)) == 0)[0]))

        sampled_data2 = dataset.sample(0.5)
        self.assertEqual(15, len(np.where(sampled_data2.labels == 1)[0]))
        self.assertEqual(15, len(np.where(np.all(sampled_data2.data, axis=(1, 2, 3)) == 1)[0]))
        self.assertEqual(35, len(np.where(sampled_data2.labels == 0)[0]))
        self.assertEqual(35, len(np.where(np.all(sampled_data2.data, axis=(1, 2, 3)) == 0)[0]))

        sampled_data3 = dataset.sample(0.2)
        self.assertEqual(6, len(np.where(sampled_data3.labels == 1)[0]))
        self.assertEqual(6, len(np.where(np.all(sampled_data3.data, axis=(1, 2, 3)) == 1)[0]))
        self.assertEqual(14, len(np.where(sampled_data3.labels == 0)[0]))
        self.assertEqual(14, len(np.where(np.all(sampled_data3.data, axis=(1, 2, 3)) == 0)[0]))

    def test_class_weights_returns_fraction_for_each_class(self):
        data1 = np.random.rand(100, 75, 75, 3)
        labels1 = np.concatenate((np.ones(50), np.zeros(50)))

        dataset1 = TestUtils.example_data_set(data=data1, labels=labels1, name="dataset1")
        self.assertDictEqual({0: 1, 1: 1}, dataset1.class_weights)

        labels2 = np.concatenate((np.zeros(50), np.ones(30),  np.ones(20) * 2))
        dataset2 = TestUtils.example_data_set(data=data1, labels=labels2, name="dataset1")
        self.assertDictEqual({0: 100.0 / (50 * 3), 1: 100.0 / (30 * 3), 2: 100.0 / (20 * 3)}, dataset2.class_weights)

        labels3 = np.concatenate((np.zeros(20), np.ones(10), np.ones(50) * 2, np.ones(20) * 3))
        dataset3 = TestUtils.example_data_set(data=data1, labels=labels3, name="dataset1")
        self.assertDictEqual({0: 100.0 / (20 * 4), 1: 100.0 / (10 * 4), 2: 100.0 / (50 * 4), 3: 100.0 / (20 * 4)},
                             dataset3.class_weights)


if __name__ == '__main__':
    unittest.main()
