import unittest
from unittest.mock import MagicMock
import os
from cbds.deeplearning import Project, ImageGenerator
from cbds.deeplearning import Model
from cbds.deeplearning import Dataset
from unittests.testutils import TestUtils


class ProjectTests(unittest.TestCase):
    def setUp(self):
        self.project_path = r"test_data/project"
        TestUtils.rm_dir_if_exists(self.project_path)
        TestUtils.rm_dir_if_exists(r"test_data/project1")
        TestUtils.rm_dir_if_exists(r"test_data/project2")
        TestUtils.rm_dir_if_exists(r"test_data/project3")
        os.makedirs(self.project_path)
        os.makedirs(r"test_data/project1")
        os.makedirs(r"test_data/project2")
        os.makedirs(r"test_data/project3")
        self.project = Project(project_path=self.project_path)

    def tearDown(self):
        TestUtils.rm_file_if_exists(r"test_data/project1/project.json")
        TestUtils.rm_file_if_exists(r"test_data/project2/project.json")
        TestUtils.rm_file_if_exists(r"test_data/project3/project.json")
        TestUtils.rm_dir_if_exists(r"test_data/project1")
        TestUtils.rm_dir_if_exists(r"test_data/project2")
        TestUtils.rm_dir_if_exists(r"test_data/project3")
        TestUtils.rm_file_if_exists(r"test_data/test.json")

    def test_project_path_set_correctly(self):
        self.assertEqual(r"test_data/project", self.project.project_path)

    def test_models_path_set_correctly(self):
        self.assertEqual(r"test_data/project1/models", Project(project_path="test_data/project1").models_path)
        self.assertEqual(r"test_data/project2/models", Project(project_path="test_data/project2").models_path)
        self.assertEqual(r"test_data/project3/models", Project(project_path="test_data/project3").models_path)
        self.assertEqual(r"test_data/project4/models", Project(project_path="test_data/project4").models_path)
        self.assertEqual(r"test_data/project5/models", Project(project_path="test_data/project5").models_path)

    def test_datasets_path_set_correctly(self):
        self.assertEqual(r"test_data/project1/datasets", Project(project_path="test_data/project1").datasets_path)
        self.assertEqual(r"test_data/project2/datasets", Project(project_path="test_data/project2").datasets_path)
        self.assertEqual(r"test_data/project3/datasets", Project(project_path="test_data/project3").datasets_path)
        self.assertEqual(r"test_data/project4/datasets", Project(project_path="test_data/project4").datasets_path)
        self.assertEqual(r"test_data/project5/datasets", Project(project_path="test_data/project5").datasets_path)

    def test_project_creates_sub_directories(self):
        self.assertFalse(os.path.exists(os.path.join(self.project_path, "models")))
        self.assertFalse(os.path.exists(os.path.join(self.project_path, "datasets")))
        self.project.create_dirs()
        self.assertTrue(os.path.exists(os.path.join(self.project_path, "models")))
        self.assertTrue(os.path.exists(os.path.join(self.project_path, "datasets")))

    def test_project_does_not_create_sub_dir_if_already_exists(self):
        os.makedirs(os.path.join(self.project_path, "models"))
        os.makedirs(os.path.join(self.project_path, "datasets"))
        self.assertTrue(os.path.exists(os.path.join(self.project_path, "models")))
        self.assertTrue(os.path.exists(os.path.join(self.project_path, "datasets")))
        self.project.create_dirs()
        self.assertTrue(os.path.exists(os.path.join(self.project_path, "models")))
        self.assertTrue(os.path.exists(os.path.join(self.project_path, "datasets")))

    def test_project_model_creates_sub_dir_in_models(self):
        model_dir = os.path.join(self.project_path, "models")
        self.assertFalse(os.path.exists(os.path.join(model_dir, "vgg16")))
        self.project.model("vgg16")
        self.assertTrue(os.path.exists(os.path.join(model_dir, "vgg16")))

        self.assertFalse(os.path.exists(os.path.join(model_dir, "InceptionV3")))
        self.project.model("InceptionV3")
        self.assertTrue(os.path.exists(os.path.join(model_dir, "InceptionV3")))

        self.assertFalse(os.path.exists(os.path.join(model_dir, "Xception")))
        self.project.model("Xception")
        self.assertTrue(os.path.exists(os.path.join(model_dir, "Xception")))

    def test_project_model_does_not_create_sub_dir_if_already_exists(self):
        model_dir = os.path.join(self.project_path, "models")

        os.makedirs(os.path.join(model_dir, "vgg16"))
        self.assertTrue(os.path.exists(os.path.join(model_dir, "vgg16")))
        self.project.model("vgg16")
        self.assertTrue(os.path.exists(os.path.join(model_dir, "vgg16")))

        os.makedirs(os.path.join(model_dir, "InceptionV3"))
        self.assertTrue(os.path.exists(os.path.join(model_dir, "InceptionV3")))
        self.project.model("InceptionV3")
        self.assertTrue(os.path.exists(os.path.join(model_dir, "InceptionV3")))

        os.makedirs(os.path.join(model_dir, "Xception"))
        self.assertTrue(os.path.exists(os.path.join(model_dir, "Xception")))
        self.project.model("Xception")
        self.assertTrue(os.path.exists(os.path.join(model_dir, "Xception")))

    def test_project_model_returns_model_class(self):
        self.assertIsInstance(self.project.model("vgg16"), Model)
        self.assertIsInstance(self.project.model("InceptionV3"), Model)
        self.assertIsInstance(self.project.model("Xception"), Model)

    def test_project_model_sets_model_name(self):
        self.assertEqual("model1", self.project.model("model1").name)
        self.assertEqual("model2", self.project.model("model2").name)
        self.assertEqual("model3", self.project.model("model3").name)
        self.assertEqual("model4", self.project.model("model4").name)
        self.assertEqual("model5", self.project.model("model5").name)

    def test_project_model_sets_model_dir(self):
        self.assertEqual("test_data/project/models/model1", self.project.model("model1").model_dir)
        self.assertEqual("test_data/project/models/model2", self.project.model("model2").model_dir)
        self.assertEqual("test_data/project/models/model3", self.project.model("model3").model_dir)
        self.assertEqual("test_data/project/models/model4", self.project.model("model4").model_dir)
        self.assertEqual("test_data/project/models/model5", self.project.model("model5").model_dir)

    def test_project_dataset_creates_sub_dir_in_datasets(self):
        dataset_dir = os.path.join(self.project_path, "datasets")
        self.assertFalse(os.path.exists(os.path.join(dataset_dir, "Bradbury")))
        self.project.dataset("Bradbury")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Bradbury")))

        self.assertFalse(os.path.exists(os.path.join(dataset_dir, "Fresno")))
        self.project.dataset("Fresno")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Fresno")))

        self.assertFalse(os.path.exists(os.path.join(dataset_dir, "NRW")))
        self.project.dataset("NRW")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "NRW")))

        self.assertFalse(os.path.exists(os.path.join(dataset_dir, "Heerlen")))
        self.project.dataset("Heerlen")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Heerlen")))

    def test_project_dataset_does_not_create_sub_dir_if_exists(self):
        dataset_dir = os.path.join(self.project_path, "datasets")

        os.makedirs(os.path.join(dataset_dir, "Bradbury"))
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Bradbury")))
        self.project.dataset("Bradbury")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Bradbury")))

        os.makedirs(os.path.join(dataset_dir, "Fresno"))
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Fresno")))
        self.project.dataset("Fresno")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Fresno")))

        os.makedirs(os.path.join(dataset_dir, "NRW"))
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "NRW")))
        self.project.dataset("NRW")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "NRW")))

        os.makedirs(os.path.join(dataset_dir, "Heerlen"))
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Heerlen")))
        self.project.dataset("Heerlen")
        self.assertTrue(os.path.exists(os.path.join(dataset_dir, "Heerlen")))

    def test_project_dataset_returns_dataset_object(self):
        self.assertIsInstance(self.project.dataset("Bradbury"), Dataset)
        self.assertIsInstance(self.project.dataset("Fresno"), Dataset)
        self.assertIsInstance(self.project.dataset("NRW"), Dataset)
        self.assertIsInstance(self.project.dataset("Heerlen"), Dataset)

    def test_project_dataset_set_dataset_name(self):
        self.assertEqual("Bradbury", self.project.dataset("Bradbury").name)
        self.assertEqual("Fresno", self.project.dataset("Fresno").name)
        self.assertEqual("NRW", self.project.dataset("NRW").name)
        self.assertEqual("Heerlen", self.project.dataset("Heerlen").name)

    def test_project_model_sets_model_dir(self):
        self.assertEqual("test_data/project/datasets/Bradbury", self.project.dataset("Bradbury").dataset_dir)
        self.assertEqual("test_data/project/datasets/Fresno", self.project.dataset("Fresno").dataset_dir)
        self.assertEqual("test_data/project/datasets/NRW", self.project.dataset("NRW").dataset_dir)
        self.assertEqual("test_data/project/datasets/Heerlen", self.project.dataset("Heerlen").dataset_dir)

    def test_image_generator_for_dataset_returns_image_generator(self):
        image_generator1 = self.project.image_generator_for_dataset("Bradbury")
        self.assertIsInstance(image_generator1, ImageGenerator)
        self.assertEqual("test_data/project/datasets/Bradbury", image_generator1.dataset.dataset_dir)

        image_generator2 = self.project.image_generator_for_dataset("Fresno")
        self.assertIsInstance(image_generator2, ImageGenerator)
        self.assertEqual("test_data/project/datasets/Fresno", image_generator2.dataset.dataset_dir)

        image_generator3 = self.project.image_generator_for_dataset("NRW")
        self.assertIsInstance(image_generator3, ImageGenerator)
        self.assertEqual("test_data/project/datasets/NRW", image_generator3.dataset.dataset_dir)

        image_generator4 = self.project.image_generator_for_dataset("Heerlen")
        self.assertIsInstance(image_generator4, ImageGenerator)
        self.assertEqual("test_data/project/datasets/Heerlen", image_generator4.dataset.dataset_dir)

    def test_to_dict_returns_dictionary_for_project(self):
        self.assertDictEqual({"Project": {
            "project_path": r"test_data/project1",
            "SettingsProvider": {
                "settings_dir": r"test_data/project1",
                "settings_filename": "project.json"
            }
        }}, Project(project_path=r"test_data/project1").to_dict())

        self.assertDictEqual({"Project": {
            "project_path": r"test_data/project2",
            "SettingsProvider": {
                "settings_dir": r"test_data/project2",
                "settings_filename": "project.json"
            }
        }}, Project(project_path=r"test_data/project2").to_dict())

        self.assertDictEqual({"Project": {
            "project_path": r"test_data/project3",
            "SettingsProvider": {
                "settings_dir": r"test_data/project3",
                "settings_filename": "project.json"
            }
        }}, Project(project_path=r"test_data/project3").to_dict())

    def test_from_dict_returns_project_object_for_settings(self):
        project1 = Project.instance_from_dict({"Project":
                                               {
                                                   "project_path": r"test_data/project1",
                                                   "SettingsProvider":
                                                   {
                                                        "settings_dir": r"test_data/project1",
                                                        "settings_filename": "project.json"
                                                   }
                                               }
                                               })
        self.assertEqual(r"test_data/project1", project1.project_path)
        self.assertEqual(r"test_data/project1", project1.settings_dir)
        self.assertEqual("project.json", project1.settings_filename)

        project2 = Project.instance_from_dict({"Project":
                                               {
                                                    "project_path": r"test_data/project2",
                                                    "SettingsProvider":
                                                    {
                                                        "settings_dir": r"test_data/project2",
                                                        "settings_filename": "project.json"
                                                    }
                                                }
                                               })
        self.assertEqual(r"test_data/project2", project2.project_path)
        self.assertEqual(r"test_data/project2", project2.settings_dir)
        self.assertEqual("project.json", project2.settings_filename)

        project3 = Project.instance_from_dict({"Project":
                                               {
                                                    "project_path": r"test_data/project3",
                                                    "SettingsProvider":
                                                    {
                                                        "settings_dir": r"test_data/project3",
                                                        "settings_filename": "project.json"
                                                    }
                                               }
                                               })
        self.assertEqual(r"test_data/project3", project3.project_path)
        self.assertEqual(r"test_data/project3", project3.settings_dir)
        self.assertEqual("project.json", project3.settings_filename)

    def test_project_loads_settings_from_disk_correctly(self):
        project = Project.load(r"test_data/project.json")
        self.assertEqual(r"test_data/project1", project.project_path)

    def test_save_saves_settings_to_disk_correctly(self):
        project1 = Project(project_path=r"test_data/project1")
        project1.save(r"test_data/project1/project.json")
        saved_project1 = Project.load(r"test_data/project1/project.json")
        self.assertEqual(project1.project_path, saved_project1.project_path)

        project2 = Project(project_path=r"test_data/project2")
        project2.save(r"test_data/project2/project.json")
        saved_project2 = Project.load(r"test_data/project2/project.json")
        self.assertEqual(project2.project_path, saved_project2.project_path)

        project1 = Project(project_path=r"test_data/project3")
        project1.save(r"test_data/project3/project.json")
        saved_project1 = Project.load(r"test_data/project3/project.json")
        self.assertEqual(project1.project_path, saved_project1.project_path)

    def test_settings_loaded_automatically_if_available(self):
        project1 = Project(project_path=r"test_data")
        self.assertEqual(r"test_data/project1", project1.project_path)
        self.assertEqual(r"test_data/project1", project1.settings_dir)

    def test_project_model_added_to_opened_model_list(self):
        project = Project(project_path=r"test_data")
        model1 = project.model("model1")
        self.assertIs(project.opened_models["model1"], model1)

        model2 = project.model("model2")
        self.assertIs(project.opened_models["model2"], model2)

        model3 = project.model("model3")
        self.assertIs(project.opened_models["model3"], model3)

    def test_project_model_returns_same_model_object_for_model_name(self):
        project = Project(project_path=r"test_data")
        model1 = project.model("model1")
        model2 = project.model("model2")
        model3 = project.model("model3")
        self.assertIs(model1, project.model("model1"))
        self.assertIs(model2, project.model("model2"))
        self.assertIs(model3, project.model("model3"))

    def test_project_dataset_added_to_opened_dataset_list(self):
        project = Project(project_path=r"test_data")
        dataset1 = project.dataset("dataset1")
        self.assertIs(project.opened_datasets["dataset1"], dataset1)

        dataset2 = project.dataset("dataset2")
        self.assertIs(project.opened_datasets["dataset2"], dataset2)

        dataset3 = project.dataset("dataset3")
        self.assertIs(project.opened_datasets["dataset3"], dataset3)

    def test_project_dataset_returns_same_dataset_object_for_dataset_name(self):
        project = Project(project_path=r"test_data")
        dataset1 = project.dataset("dataset1")
        dataset2 = project.dataset("dataset2")
        dataset3 = project.dataset("dataset3")
        self.assertIs(dataset1, project.dataset("dataset1"))
        self.assertIs(dataset2, project.dataset("dataset2"))
        self.assertIs(dataset3, project.dataset("dataset3"))

    def test_project_save_method_saves_everything_to_disk(self):
        model1 = MagicMock()
        model2 = MagicMock()
        model3 = MagicMock()
        dataset1 = MagicMock()
        dataset2 = MagicMock()

        project = Project(project_path=r"test_data")
        project.opened_models = {
            "model1": model1,
            "model2": model2,
            "model3": model3
        }
        project.opened_datasets = {
            "dataset1": dataset1,
            "dataset2": dataset2
        }

        project.save(r"test_data/test.json")
        self.assertTrue(model1.save_settings.called)
        self.assertTrue(model2.save_settings.called)
        self.assertTrue(model3.save_settings.called)
        self.assertTrue(dataset1.save_settings.called)
        self.assertTrue(dataset2.save_settings.called)

    def test_project_exit_method_saves_project_to_disk(self):
        model1 = MagicMock()
        model2 = MagicMock()
        model3 = MagicMock()
        dataset1 = MagicMock()
        dataset2 = MagicMock()

        with Project(project_path=r"test_data") as project:
            project.opened_models = {
                "model1": model1,
                "model2": model2,
                "model3": model3
            }
            project.opened_datasets = {
                "dataset1": dataset1,
                "dataset2": dataset2
            }

        self.assertTrue(model1.save_settings.called)
        self.assertTrue(model2.save_settings.called)
        self.assertTrue(model3.save_settings.called)
        self.assertTrue(dataset1.save_settings.called)
        self.assertTrue(dataset2.save_settings.called)

    def test_project_models_returns_all_models_for_project(self):
        project = Project(project_path=r"test_data/project1")
        project.model("model1")
        project.model("model2")
        project.model("model3")

        self.assertEqual(["model1", "model2", "model3"], project.models)

    def test_project_datasets_returns_all_datasets_for_project(self):
        project = Project(project_path=r"test_data/project1")
        project.dataset("dataset1")
        project.dataset("dataset2")
        project.dataset("dataset3")

        self.assertEqual(["dataset1", "dataset2", "dataset3"], project.datasets)


if __name__ == '__main__':
    unittest.main()
