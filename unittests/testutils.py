import os
import shutil

from keras import Sequential
from keras.layers import Dense
from cbds.deeplearning import Dataset


class TestUtils:
    @staticmethod
    def rm_file_if_exists(path):
        if os.path.exists(path):
            os.remove(path)

    @staticmethod
    def rm_dir_if_exists(path):
        if os.path.exists(path):
            shutil.rmtree(path)

    @staticmethod
    def create_example_model():
        model = Sequential()
        model.add(Dense(64, input_shape=(100,), activation="relu"))
        model.add(Dense(1, activation="softmax"))
        return model

    @staticmethod
    def compare_keras_models(left_model, right_model):
        equal = type(left_model) == type(right_model)
        for layer1, layer2 in zip(left_model.layers, right_model.layers):
            equal = equal and (layer1.get_config() == layer2.get_config())
        return equal

    @staticmethod
    def example_training_set(dataset_dir, name="training_set"):
        return Dataset(name=name, dataset_dir=dataset_dir).\
            import_numpy_dataset(r"test_data/dataset_1.npy", r"test_data/dataset_1_labels.npy")

    @staticmethod
    def example_test_set(dataset_dir, name="test_set"):
        return Dataset(name=name, dataset_dir=dataset_dir).\
            import_numpy_dataset(r"test_data/dataset_2.npy", r"test_data/dataset_2_labels.npy")

    @staticmethod
    def example_data_set(data, labels, name="example_data", dataset_dir=""):
        dataset = Dataset(name=name, dataset_dir=dataset_dir)
        dataset.data = data
        dataset.labels = labels
        return dataset

